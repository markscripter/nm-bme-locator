﻿;
(function ($, nmcom, undefined) {
    'use strict';

    nmcom.errorHandler = {

        endpointIsAvailable: true,

        init: function () {
            window.onerror = this.windowErrorHandler;
        },

        testAjaxError: function () {
            nmcom.Services.Http.POST('/nmcomMvc/NonController/NonAction/ThisShouldError', { header: 'test header' }, { data: 'test value' });
        },

        testUndefinedFunctionError: function () {
            nmcom.NoneSuch.DoSomethingThatIsNotAFunction();
        },

        /**
        * global error handler to capture errors and send them to a logging service
        * @param {string} msg - automatically passed in by window
        * @param {string} url - automatically passed in by window
        * @param {string} lineNumber - automatically passed in by window
        */

        windowErrorHandler: function (msg, url, lineNumber) {
           if (msg) { // only log an error if we have a message
              var error = {
                 scriptName: url,
                 message: msg,
                 originUrl: window.location.href,
                 browser: window.navigator.userAgent,
                 lineNumber: lineNumber,
                 level: 3
              };

              nmcom.errorHandler.logError(error);
           }

           //this will tell the native error handler to run
            return false;
        },

        logError: function (error) {
            var message;
            var logUrl = '/nmcomMvc/JavaScriptLogging/Log?scIsFallThrough=True';

            if (error.message === undefined) {
                var logMessage = {
                    scriptName: '',
                    message: error,
                    originUrl: window.location.href,
                    browser: window.navigator.userAgent,
                    lineNumber: 0,
                    level: 0
                };
                message = JSON.stringify(logMessage);
            } else {
                message = JSON.stringify(error);
            }

            if (nmcom.errorHandler.endpointIsAvailable) {
                try {
                    var request = nmcom.Services.Http.POST(
                        logUrl,
                        {},
                        message
                    );

                    request.done(nmcom.errorHandler.handleResponse);
                    request.error(nmcom.errorHandler.handleError);

                    return request;

                } catch (ex) {
                    nmcom.errorHandler.endpointIsAvailable = false;
                    console.log('Cannot post to the logging endpoint at ' + logUrl + '\r\nError Details:');
                    console.log(ex);
                    console.log('The following error just occurred\r\n');
                    console.log(message);
                    return false;
                }
            } else {
                console.log('The logging endpoint is not currently available. The following error just occurred\r\n');
                console.log(message);
                return false;
            }
        },

        handleResponse: function (data, status, jqXhr) {
            nmcom.errorHandler.endpointIsAvailable = true;
            if (status === 'error') {
                var logMessage = {
                    data: data,
                    status: status,
                    originUrl: window.location.href,
                    browser: window.navigator.userAgent,
                    jqXhr: jqXhr
                };

                console.log('An error occurred while logging a javascript error. Details:\r\n');
                console.log(logMessage);
            }
        },

        handleError: function (data, status, jqXhr) {
            if (data.status === 503 || data.status === 404 || jqXhr === 'Service Unavailable') {
                nmcom.errorHandler.endpointIsAvailable = false;
            }

            var logMessage = {
                data: data,
                status: status,
                originUrl: window.location.href,
                browser: window.navigator.userAgent,
                jqXhr: jqXhr
            };

            console.log('An error occurred while logging a javascript error. Details:\r\n');
            console.log(logMessage);
        }
    };

})(window.jQuery, window.nmcom || (window.nmcom = {}));

jQuery(function() {
    nmcom.errorHandler.init();
});