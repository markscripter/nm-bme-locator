﻿;
(function ($, nmcom, undefined) {
    'use strict';
    nmcom.rate = nmcom.rate || {};

    nmcom.rate.RateItem = (function () {
        var rateItemModel = function(config) {
            var vm = {};
            var me = {}; // for private members

            me.defaults = {
                ItemId: '',
                Ratings: [],
                SessionRating: 0.0,
                rateApi: '/nmcomApi/rateitem',
                Containers: {
                    RateButtonContainer: $() // container to store the result of the Underscore template rendering
                },
                Selectors: {
                    rateTemplate: '', // selector for the element that contains the Underscore template definition
                    rateButton: '', // selectors for the individual rate buttons.
                    ratingLabel: '',
                    rateValueDataAttribute: 'value', // each rate button needs a rating value in a data attribute with the Selectors.rateValueDataAttribute suffix
                    rateLabelDataAttribute: 'label'
                }
            };

            me.adobeTracker = nmcom.AdobeTracker != undefined ? nmcom.AdobeTracker : undefined;

            var opts = $.extend(true, {}, me.defaults, config);

            //#region settings
            vm.currentRating = 0.0;
            vm.rateButtons = $();
            vm.ratingLabel = $();
            vm.currentActive = $();
            //#endregion

            //#region methods
            vm.setup = function () {
                vm.currentRating = opts.SessionRating;

                vm.render();
                vm.selectElements();
                vm.setupDom();
            };

            vm.prepareTemplates = function () {
                if (opts.Selectors.rateTemplate.length) {
                    me.rateTemplate = _.template($(opts.Selectors.rateTemplate).html());
                }
            };

            vm.render = function() {
                vm.prepareTemplates();

                if (opts.Containers.RateButtonContainer.length && me.rateTemplate.length) {
                    opts.Containers.RateButtonContainer.html(me.rateTemplate({
                        currentRating: vm.currentRating,
                        ratings: opts.Ratings
                    }));
                }
            };

            vm.clickOnce = function(e, elem, func) {
                elem.off('click');
                func(elem);
                e.preventDefault();
            }

            vm.selectElements = function() {
                vm.rateButtons = $(opts.Selectors.rateButton);
                vm.ratingLabel = $(opts.Selectors.ratingLabel);
                vm.currentActive = opts.Containers.RateButtonContainer.find('.star.active');
            }

            vm.setupDom = function() {
                vm.rateButtons.hover(function () { vm.ratingButtonHoverIn($(this)) }, function () { vm.ratingButtonHoverOut() });
                vm.rateButtons.off('click').one('click', function (e) { vm.clickOnce(e, $(this), vm.ratingButtonClick); });
            }

            vm.ratingButtonClick = function(elem) {
                var rating = elem.data(opts.Selectors.rateValueDataAttribute);
                vm.currentRating = parseFloat(rating);
                vm.currentActive = opts.Containers.RateButtonContainer.find('.star.active');
                return vm.rateItem();
            }

            vm.ratingButtonHoverIn = function(elem) {
                vm.currentActive.removeClass('active');
                elem.prevAll().addClass('active');
                elem.addClass('active');
                vm.ratingLabel.text(elem.data(opts.Selectors.rateLabelDataAttribute));
            }

            vm.ratingButtonHoverOut = function() {
                opts.Containers.RateButtonContainer.find('.star').removeClass('active');
                vm.currentActive.addClass('active');
                vm.ratingLabel.text('');
            }

            vm.rateItem = function () {
                return vm.doRating().then(vm.setupDom); //.then(vm.render).then(vm.setupDom); //.then(vm.recordAnalytics);
            };

            vm.onDoRatingDone = function () {
                //vm.averageRating = data.Average;
            };

            vm.onDoRatingrror = function (jqXhr, status, error) {
                window.console && console.log('error rating item. error:');
                window.console && console.log(JSON.stringify(error, null, "\t"));

                var pageUrl = window.location.href;
                var errorMsg = {
                    scriptName: 'rate.js',
                    message: 'error rating item. ItemId=' + vm.ItemId + ' URL=' + pageUrl + ' STATUS=' + status + ' ERROR=' + error,
                    originUrl: pageUrl,
                    browser: window.navigator.userAgent,
                    lineNumber: 0,
                    level: 5
                };

                nmcom.errorHandler.logError(errorMsg);
            };

            vm.getRateParams = function() {
                var params = {
                    ItemIdNum: opts.ItemId,
                    RatingNum: vm.currentRating
                };

                return params;
            }

            vm.doRating = function () {
                // this function exists only as an external dependency that can be replaced in unit tests.
                var params = vm.getRateParams();

                var request = nmcom.Services.Http.POST(
                        opts.rateApi,
                        {},
                        JSON.stringify(params)
                    );

                request.done(vm.onDoRatingDone);
                request.error(vm.onDoRatingrror);

                return request;
            };

            vm.recordAnalytics = function () {

                if (me.adobeTracker !== undefined && me.adobeTracker.setVar !== undefined) {
                    try {
                        if (opts.Labels.ResultsTotalLabel.length) {
                            //adobe dtm call to record the rating of an item
                            me.adobeTracker.setVar('RateItem', opts.Labels.ResultsTotalLabel.text());
                        }
                        nmcom.adobeDtmDirectCall("nmcomRateItem");
                    } catch (e) {
                        var errorMsg = {
                            scriptName: 'rate.js',
                            message: '_satellite: Failed to call .track() method with this rule: nmcomRateItem',
                            originUrl: window.location.href,
                            browser: window.navigator.userAgent,
                            lineNumber: 71,
                            level: 2
                        };

                        nmcom.errorHandler.logError(errorMsg);
                    }
                }
            };

            //#endregion

            return vm;
        };

        var createRateItemModel = function (config) {
            var model = new rateItemModel(config);
            model.setup();
            return model;
        };

        return {
            Init: createRateItemModel
        };
    })();

})(window.jQuery, window.nmcom || (window.nmcom = {}));
