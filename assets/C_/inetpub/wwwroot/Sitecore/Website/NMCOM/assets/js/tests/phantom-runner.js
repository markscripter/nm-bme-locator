﻿'use strict';

console.log('About to load teamCitySpecRunner.html ... ');

var page = new WebPage();

// LOCAL DEV PATH
//var url = 'file://localhost/C:/nmldev/NMCOM/Trunk/NMCOM/NMCOM.Web/NMCOM/assets/js/tests/teamCitySpecRunner.html';

// PATH ON TEAM CITY
var url = 'file://localhost/C:/TC/NMCOM/NMCOM.Web/NMCOM/assets/js/tests/teamCitySpecRunner.html';

console.log('URL for teamCitySpecRunner.html: ' + url);

phantom.viewportSize = { width: 1024, height: 768 };

// PhantomJS sandboxes the website and it doesn't display console messages by default
page.onConsoleMessage = function (msg) {

    console.log(msg);

    // exit as soon as the tests complete
    if (msg && msg.indexOf('##teamcity[progressFinish') !== -1) {
        phantom.exit();
    }

};

// open the website/page
page.open(url, function (status) {

    if (status !== 'success') {
        console.log('Unable to load the address!');
        phantom.exit();
    } else {
        // using a delay to make sure the JavaScript has time to manipulate the DOM in the tests
        window.setTimeout(function() {
            // page.render("output.png");
            console.log('Exiting after 60 seconds of execution time.');
            phantom.exit();
        }, 60000);
    }

});