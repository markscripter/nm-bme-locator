﻿; (function ($, window, document, undefined) {
    'use strict';

    if (typeof $ === 'undefined') { throw new Error("scroll-toggle.js requires jQuery") };

    var ScrollToggle = function ($element, options) {
        this.init($element, options);
    };

    var namespace = ScrollToggle.NAMESPACE = 'scrollToggle';

    var defaults = ScrollToggle.DEFAULTS = {};

    ScrollToggle.prototype.init = function ($element, options) {
        this.$element = $element;
        this.settings = options;
        this.target = [];

        var _self = this;

        $.each(this.settings.target.split(','), function (indx, val) {
            _self.target.push($(val));
        });

        $(document).on('scroll.scrolltoggle', $.proxy(this.toggle, this))
    };

    ScrollToggle.prototype.hasOverlap = function (target) {
        if (target.length === 0) {
            return false;
        }
        var target = {
            top: Math.ceil(target.offset().top),
            bottom: Math.ceil(target.offset().top + target.outerHeight())
        },
          toggle = {
              top: Math.ceil(this.$element.offset().top),
              bottom: Math.ceil(this.$element.offset().top + this.$element.outerHeight())
          };

        return target.top < toggle.bottom == toggle.bottom < target.bottom || target.top < toggle.top == toggle.top < target.bottom;
    };

    ScrollToggle.prototype.toggle = function () {
        var _self = this,
          toggleDisplay = false;

        $.each(this.target, function (idx, val) {
            if (_self.hasOverlap(val)) toggleDisplay = true;
        });

        if (toggleDisplay) {
            $('.free-guide-form-popout.open').removeClass('open');
            $('button.scroll-toggle.open').removeClass('open');
            $('body').removeClass('pop-out-open');
        }

        this.$element.toggleClass('scroll-hide', toggleDisplay);
        this.$element.prop("disabled", toggleDisplay);
    };

    // Plugin Definition 

    $.fn[namespace] = function (options) {
        return this.each(function () {
            var $this = $(this),
              data = $this.data(namespace),
              settings = $.extend(true, {}, defaults, options, $this.data());

            !data && $this.data(namespace, (data = new ScrollToggle($this, settings)));
        });
    };

    $.fn[namespace].Constructor = ScrollToggle;

    $.expr[':'][namespace] = function (elem) {
        return !!$.data(elem, namespace);
    };

    $(function () {
        $('.scroll-toggle').scrollToggle();
    });
})(window.jQuery, this, this.document);

$(function () {
    $('.form-close').on("click", function (e) {
        $('.free-guide-form-popout').toggleClass('open');
        $('.scroll-toggle').toggleClass('open');
        $('body').toggleClass('pop-out-open');
    });
    $('.form-close-mobile').on("click", function (e) {
        $('.free-guide-form-popout').toggleClass('open');
        $('.scroll-toggle').toggleClass('open');
        $('body').toggleClass('pop-out-open');
    });
});

$(function () {
    $(".free-guide").click(function () {
        $(".free-guide-form-popout").toggleClass("open", 1000);
        $('.scroll-toggle').toggleClass('open');
        $('body').toggleClass('pop-out-open');
    });
});
