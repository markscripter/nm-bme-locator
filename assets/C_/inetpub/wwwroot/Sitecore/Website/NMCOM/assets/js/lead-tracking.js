﻿;
(function ($, nmcom, undefined) {
    'use strict';
    nmcom.leadTracking = nmcom.leadTracking || {};

    $.extend(nmcom.leadTracking, {
        settings: {
            leadTrackingCookieName: "leadTracking",
            leadTrackingCookieDuration: "", //Immediately die
            campaignTrackingCookieName: "campaignTracking",
            campaignTrackingCookieDuration: "" //Immediately die
        },

        configCampaignTrackingCookie: function() {

            var campName = nmcom.getQueryStringParam("campName");
            var campaignTrackingDetails = {
                campaignName: ""
            };

            if (campName.length) {
                campaignTrackingDetails.campaignName = campName;
                nmcom.setCookie(this.settings.campaignTrackingCookieName, JSON.stringify(campaignTrackingDetails), this.settings.campaignTrackingCookieDuration);
            }
        },

        configLeadTrackingCookie: function() {

            var agentNumber = nmcom.getQueryStringParam("agentNumber");
            var agentEmail = nmcom.getQueryStringParam("email");
            var fwsType = nmcom.getQueryStringParam("FWSType");
            var isFws = nmcom.getQueryStringParam("fws");
            var isNewsLetter = nmcom.getQueryStringParam("nl");

            var leadTrackingDetails = {
                agentNum: "",
                email: "",
                fwsType: "",
                isFWS: false,
                isNewsLetter: false
            };

            if (agentNumber.length) {
                leadTrackingDetails.agentNum = agentNumber;
            }

            if (agentEmail.length) {
                leadTrackingDetails.email = agentEmail + "@nm.com";
            }

            if (fwsType.length) {
                leadTrackingDetails.fwsType = fwsType;
            }

            if (isFws.length && isFws === "1") {
                leadTrackingDetails.isFWS = true;
            }

            if (isNewsLetter.length && isNewsLetter === "1") {
                leadTrackingDetails.isNewsLetter = true;
            }

            if (nmcom.isCookieValid(leadTrackingDetails)) {
                nmcom.setCookie(this.settings.leadTrackingCookieName, JSON.stringify(leadTrackingDetails), this.settings.leadTrackingCookieDuration);
            }

        },

        removeLeadTrackingCookie: function() {

            var cookieDate = new Date(); // current date & time
            cookieDate.setTime(cookieDate.getTime() - 1);
            nmcom.setCookie(this.settings.leadTrackingCookieName, "", cookieDate);
        },
        
        isLeadTrackingCookieValid: function (trackingCookie) {
            var isValid = true;

            if (trackingCookie === null || trackingCookie === undefined || trackingCookie === "") {
                return false;
            }

            for (var prop in trackingCookie) {
                if (trackingCookie.hasOwnProperty(prop)) {
                    if (trackingCookie[prop] === "") {
                        isValid = false;
                    }
                }
            }

            return isValid;
        }


    }); //extend

})(window.jQuery, window.nmcom || (window.nmcom = {}));

/*
jQuery(function () {
    nmcom.leadTracking.init();
});*/