﻿;
(function ($, nmcom, undefined) {
   'use strict';
   var nml = nmcom.recruiting = nmcom.recruiting || {};

   nml.GoogleMapsLoaded = function () { //Google Maps has loaded, go forth and do stuff
      window.recruitingModels = window.recruitingModels || [];
      for (var i = 0; i < window.recruitingModels.length; i++) {
         var model = window.recruitingModels[i];
         if (model.GoogleApiIsReady !== undefined) {
            model.GoogleApiIsReady();
         }
      }
   };

   nml.RecruitingModel = (function () {
      var recruitingModel = function(settings) {
         var model = {};
         var me = {}; // for private members
         me.form = $();

         me.google = nmcom.recruiting.GoogleMaps.Create();
         me.thankYouPage = settings.thankYouPage;

         me.selectors = {
            formID: "#recruiting-form"
         };

         me.mapObjects = {
            geocoderInstance: null,
         };

         me.rules = settings.rules != undefined ? settings.rules : {
            'fName': {
               required: true
            },
            'lName': {
               required: true
            },
            'primAddr': {
               required: true
            },
            'cityNam': {
               required: true
            },
            'stateAbbr': {
               required: true
            },
            'zipCDE': {
               required: true,
               minlength: 5,
               digits: true
            },
            'email': {
               required: true,
               validEmail: true
            },
            'phoneNum': {
               required: true,
               phoneWithArea: true
            },
            'resume': {
               extension: "doc|docx|docm|pdf|gdoc|mcw|txt|wps|pps|ppt|xls",
               fileSize: 10485760 //10 MB
            }
         };

         me.messages = settings.messages != undefined ? settings.messages : {
            'fName': {
               required: 'Please enter your first name.'
            },
            'lName': {
               required: 'Please enter your last name.'
            },
            'primAddr': {
               required: "Please enter your street address."
            },
            'cityNam': {
               required: "Please enter your city."
            },
            'stateAbbr': {
               required: "Please select your state."
            },
            'zipCDE': {
               required: "Please enter a zip code.",
               minlength: 'Zip code must be at least 5 digits long.',
               digits: 'Zip code can only be digits.'
            },
            'email': {
               required: "Please enter your email address."
            },
            'phoneNum': {
               required: "Please enter your phone number."
            },
            'resume': {
               extension: "We accept the following file types: doc, docx, docm, pdf, gdoc, mcw, txt, wps, pps, ppt, xls",
               fileSize: "Please choose a smaller file, max size is 10 MB."
            }
         };

         model.InitializeRecruitingModel = function () {
            me.form = $(me.selectors.formID);
            me.google.Initialize();
            me.setupPageEvents();
            me.setupValidation();
         };

         model.GoogleApiIsReady = function () {
            me.mapObjects.geocoderInstance = me.google.CreateGeocoder();
         };

         me.setupPageEvents = function() {
            $(document).ajaxStart(function() {
               $('#recruiting-form input[type="submit"]').prop('disabled', true);
            }).ajaxStop(function() {
               $('#loading').hide();
               $('#recruiting-form input[type="submit"]').prop('disabled', false);
            });

            me.form.submit(me.handleFormSubmission);

         };

         me.setupValidation = function() {
            nmcom.validation.setupValidate(me.form,
            {
               rules: me.rules,
               messages: me.messages,
               invalidHandler: function() {
                  $('#loading').hide();
                  $('input[type="submit"]').removeClass('loading');
               }
            });
         };

         me.handleFormSubmission = function(e) {
            e.preventDefault();

            var $form = $(e.currentTarget);

            if (!$form.valid()) {
               return;
            }

            $('#loading').css('display', 'inline-block');
            $('input[type="submit"]').addClass('loading');

            me.resetFormVisibility();

            me.geocodeAddress().then(function(results) {
               me.addressGeocoded(results);
               me.submitForm($form);
            }).fail(function (err)
            {
               var pageUrl = window.location.href;
               var errorMsg = {
                  scriptName: 'recruiting-form-view.js',
                  message: 'Recruiting form failed to geocode address.  URL=' + pageUrl + ' STATUS=' + status + ' ERROR=' + error,
                  originUrl: pageUrl,
                  browser: window.navigator.userAgent,
                  lineNumber: 0,
                  level: 3
               };
               nmcom.errorHandler.logError(errorMsg);
            });
            
         };

         me.geocodeAddress = function () {
            var deferred = $.Deferred();
            var address1 = $('input[name="primAddr"]').val();
            var address2 = $('input[name="secnAddr"]').val();
            var city = $('input[name="cityNam"]').val();
            var state = $('select[name="stateAbbr"]').val();
            var zip = $('input[name="zipCDE"]').val();

            var address = address1;
            if (address2.length > 0) {
               address += " " + address2;
            }
            if (city.length > 0) {
               address += ", " + city + ", ";
            }
            if (state.length > 0) {
               address += state + " ";
            }
            if (zip.length > 0) {
               address += zip;
            }

            var geocodeRequest = {
               region: 'US',
               address: address
            };

            me.mapObjects.geocoderInstance.geocode(geocodeRequest, function(geocodeResults, status) {
               if (status === google.maps.GeocoderStatus.OK) {
                  deferred.resolve(geocodeResults[0]);
               } else {
                  deferred.reject(status);
               }
            });

            return deferred.promise();
         };

         me.addressGeocoded = function(geocodeResult, geocodeStatus) {
            var lat;
            var lng;
            //geocoded request successfully, put lat/lng into the hidden inputs
            var location = geocodeResult["geometry"]["location"];
            lat = location.lat();
            lng = location.lng();

            var $form = $('#recruiting-form');
            $form.find('input[name="recLong"]').val(lng);
            $form.find('input[name="recLat"]').val(lat);    
         };

         me.submitForm = function(formToSubmit) {
            var $form = formToSubmit;

            var xhr2 = !!(window.FormData && ("upload" in ($.ajaxSettings.xhr())));
            if (xhr2) { //this browser supports AJAX file uploading, do it!
               var actionUrl = $form.attr('action');
               var data = new FormData($form[0]);

               $.ajax({
                  url: actionUrl,
                  data: data,
                  cache: false,
                  contentType: false,
                  processData: false,
                  type: 'POST',
                  dataType: 'json',
                  success: me.formSubmitSuccess,
                  error: me.formSubmitError
               });
            } else { //this is an old browser that doesn't support ajax file uploading, so do the post to hidden iframe trick
               $('iframe').attr({
                  "name": "postResultsFrame"
               }).appendTo($form).hide();

               /*
                 * Tell the form to post to the hidden iframe
                 * remove the submit event handler so that a traditional form POST may occur
                 * submit the form
                 */
               $form.attr("target", "postResultsFrame").unbind('submit').submit();
               model.showThankYou();
            }

            //track analytics
            me.TrackAnalytics();

         };

         me.TrackAnalytics = function() {
            if ($('#fulltime-internship').is(':checked')) {
               nmcom.adobeDtmDirectCall("FRInternshipRecruitingFormComplete");
            } else {
               nmcom.adobeDtmDirectCall("FRFullTimeRecruitingFormCompletete");
            }
         };

         me.formSubmitSuccess = function() {
            me.showThankYou();
         };

         me.formSubmitError = function(jqXhr, status, error) {
            var pageUrl = window.location.href;
            var errorMsg = {
               scriptName: 'recruiting-form-view.js',
               message: 'Recruiting form failed to submit.  URL=' + pageUrl + ' STATUS=' + status + ' ERROR=' + error,
               originUrl: pageUrl,
               browser: window.navigator.userAgent,
               lineNumber: 0,
               level: 3
            };
            nmcom.errorHandler.logError(errorMsg);
            $('#recruiting-form-error').show();
            $('#recruiting-form input[type="submit"]').prop('disabled', false);
         };


         me.resetFormVisibility = function() {
            $('#recruiting-form-error').hide();
         };

         me.showThankYou = function() {
            window.location = me.thankYouPage;
         };

         return model;
      };

      var createRecruitingModel = function (settings) {
         var model = new recruitingModel(settings);
         window.recruitingModels = window.recruitingModels || [];
         window.recruitingModels.push(model);

         return model;
      };

      return {
         Create: createRecruitingModel
      };
   })();

   nml.GoogleMaps = (function () {
      var googleApiModel = function () {
         var model = {};
         var me = {}; // for private members

         me.mapsUrl = "https://maps.google.com/maps/api/js?v=3&client=gme-northwesternmutual&sensor=false&callback=nmcom.recruiting.GoogleMapsLoaded";

         model.Initialize = function () {
            return nmcom.Services.Http.SCRIPT(me.mapsUrl);
         };

         //#region  Google Map API Wrapper

         model.CreateGeocoder = function () {
            return new google.maps.Geocoder();
         };

         //#endregion

         return model;
      };

      var createGoogleApiModel = function () {
         var model = new googleApiModel();
         return model;
      };
      return {
         Create: createGoogleApiModel
      };
   })();

   })(window.jQuery, window.nmcom || (window.nmcom = {}));
