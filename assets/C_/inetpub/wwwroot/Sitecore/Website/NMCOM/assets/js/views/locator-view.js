﻿;
(function ($, window, nmcom, undefined) {
    'use strict';
    var nml = nmcom.locator = nmcom.locator || {};

    // global hook for the google map api callback
    nml.GoogleMapsLoaded = function() {
        window.locatorModels = window.locatorModels || [];
        for (var i = 0; i < window.locatorModels.length; i++) {
            var model = window.locatorModels[i];
            if (model.GoogleApiIsReady !== undefined) {
                model.GoogleApiIsReady();
            }
        }
    }

    nml.markerTypes = {
        people: 'people',
        alpha: 'alpha'
    };
    nml.defaults = {
        searchApi: '/nmcomApi/entities',
        entityAddressServiceUrl: '/nmcomApi/EntityAddress?',
        types: ['FR', 'MD'],	
        numPerPage: 10,
        maxCountToShowInList: 150,
        animationSpeed: 500,
        userIpLocation: undefined,
        delimiter: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''),
        markerType: nml.markerTypes.people, 
        Input: {
            geoLocationLink: $('#geolocation-search'),
            nameInput: $('input#last-name'), 
            addressInput: $('input#location'),
            inputWithPlaceholder: $('#location, #last-name'),
            searchButton: $('.map-options input[type="submit"]')
        },
        Containers: {
            mapContainer: $(),
            resultsContainer: $(),
            noResultsContainer: $(),
            locationContainer: $(),
            messageContainer: $(),
            paginationContainer: $(),
            leadForm: $()
        },
        Selectors: {
            resultsTemplate: '#agent-template',
            pagingTemplate: '',
            pageLink: '',
            pageLinkPrev: '',
            pageLinkNext: '',
            mapOptions: '.map-options',
            detailList: '#advisorLocatorList',
            detailListItems: '#advisorLocatorList > li',
            detailContactLink: '.advisor-list .contactMe',
            agentNumberInput: 'input[name="AgentNumber"]',
            formCampName: 'input[name="campName"]',
            leadFormThankYouAndError: '#lead-form-thankyou, #lead-form-error'
        },
        Constants: {
            // Messaging
            yourLocationLabel: 'Your Location:',
            resultLabel: 'Result',
            forLabel: 'for:',
            nearLabel: 'near',
            // Mapper
            pinLetterToken: '{{letter}}',
            //Searcher
            geolocateFailureMessage: 'Unable to determine your exact location. <br> <br> Please use the search fields.',
            // Formatter
            managingPartner: 'Managing Partner',
            managingDirector: 'Managing Director',
            aoeHeadline: '<strong>Areas of Expertise: </strong>',
            spokenLanguagesHeadline: '<strong>Additional Languages Spoken: </strong>',
            bioHeadline: '<strong>About my Practice: </strong>',
            // Form
            leadCampName: 'NM.com FR Locator'
        }
    };

    nml.LocatorModel = (function () {
        var locatorModel = function (config) {
            var model = {};
            var me = {}; // for private members

            model.settings = $.extend(true, {}, nml.defaults, config); 

            me.initialized = false;

            model.leadForm = config.leadForm;
            model.google = nmcom.locator.GoogleMaps.Create();

            model.mapDragTimeout = null;
            model.entityAddresses = {
                //key is Address.Street + Address.zip, value is array of entities at that address
            };
            model.agentCards = $(); //store a reference to the jQuery collection of agent cards for progressive scrolling

            model.InitializeLocator = function () {
                model.google.Initialize();
            };
            model.Setup = function () {
                if ('geolocation' in navigator) {
                    model.settings.Input.geoLocationLink.show();
                } else {
                    model.settings.Input.geoLocationLink.hide();
                }

                // Events - ajax progress indicator, click handlers, key handlers
                $(document).ajaxStart(function () { 
                    $('#loading').show(); 
                }).ajaxStop(function () {
                    $('#loading').hide();
                });
                model.settings.Input.searchButton.click(function (e) {
                    e.preventDefault();
                    if (model.settings.Containers.leadForm.is(":visible")) { //if form already visible, close it
                        model.leadForm.hideLeadForm(e);
                    }
                    model.searcher.search();
                });
                model.settings.Input.geoLocationLink.click(function (e) {
                    e.preventDefault();
                    if (model.settings.Containers.leadForm.is(":visible")) { //if form already visible, close it
                        model.leadForm.hideLeadForm(e);
                    }
                    if (model.searcher !== undefined && !model.searcher.isSearching) {
                        model.searcher.gpsSearch();
                        var mapContainer = $(model.settings.Containers.mapContainer);
                        if (mapContainer.offset().top < $(window).height()) {
                            $('html, body').animate({
                                scrollTop: mapContainer.offset().top - 20
                            }, model.settings.animationSpeed);
                        }
                    }
                });
                $(document).on('click', model.settings.Selectors.detailContactLink, model.leadForm.toggleLeadForm);
                $(model.settings.Selectors.mapOptions).keydown(function (e) {
                    if (e.keyCode === 13) { // enter key
                        e.preventDefault();
                        if (model.settings.Containers.leadForm.is(":visible")) { //if form already visible, close it
                            model.leadForm.hideLeadForm(e);
                        }
                        model.searcher.search();
                    }
                });

                $(document).on('input', '.clearable', function () {
                    $(this)[model.toggleClass(this.value)]('x');
                }).on('mousemove', '.x', function (e) {
                    $(this)[model.toggleClass(this.offsetWidth - 18 < e.clientX - this.getBoundingClientRect().left)]('onX');
                }).on('click', '.onX', function () {
                    $(this).removeClass('x onX').val('');
                });

                //setup progressive scrolling
                $(window).scroll(function () {
                    if (!model.leadForm.IsVisible) { //don't do progressive loading if the lead form is visible

                        var advisorList = $(model.settings.Selectors.detailList);
                        var top = advisorList.offset() ? advisorList.offset().top : 0;

                        var topOfWindow = $(window).scrollTop();
                        var windowHeight = $(window).height();
                        var topOfAgentCards = top;
                        var agentCardHeight = advisorList.height();

                        if ((topOfWindow + windowHeight) > (topOfAgentCards + agentCardHeight - 200)) {
                            model.showNextSetOfAgentCards();
                        }
                    }

                    // Disable map dragging and zooming when the window is scrolling
                    if (model.mapDragTimeout == null) {
                        model.mapper.disableUI();
                        model.mapDragTimeout = setTimeout(function () {
                            model.mapper.enableUI();
                            model.mapDragTimeout = null;
                        }, 2000);
                    }
                });

                // add placeholders for fields without labels in older browsers (IE9)
                if (!Modernizr.input.placeholder) {
                    nmcom.setupLegacyPlaceholders(model.settings.Input.inputWithPlaceholder);
                }
            }
            model.StartSearch = function () {
                model.searcher.querystringSearch();
            };
            model.GoogleApiIsReady = function () {
                if (!me.initialized) {
                    me.initialized = true;
                    model.google.SetApiProperties();
                    model.mapper = nmcom.locator.Mapper.Create(model.settings, model.google, model.centerClick, model.markerClick, model.viewChanged);
                    model.searcher = nmcom.locator.Searcher.Create(model.settings, model.mapper, model.searchInitClearMessage, model.processAddressResults, model.processDetailedResults, model.noResults, model.showYourLocationMessage, model.showError);
                    model.formatter = nmcom.locator.Formatter.Create(model.settings);
                    model.leadForm = nmcom.locator.LeadForm.Create(model.settings, model.clearMessage, model.leadForm);
                    model.Setup();
                    model.StartSearch();
                }
            };

            //#region  Map events 
            model.focusMap = function () {
                var marker = this;
                model.mapper.focusMapOnAddress(marker);
            };
            model.selectLocation = function () {
                var marker = this;
                model.searcher.initSearch();
                model.mapper.setMarkers();
                model.mapper.highlightMarker(marker);
                model.clearAgentCards();
                model.mapper.focusMapOnAddress(marker);

                if(marker.entities){
                    model.searcher.searchParameters.lastName = model.searcher.LastNameInputValue();
                    model.searcher.doSearchForFRDetails(marker.entities[0].Address.Latitude, marker.entities[0].Address.Longitude);

                    model.scrollToAgentCards();
                }
            };
            model.scrollToAgentCards = function () {
                model.settings.Containers.leadForm.find(model.settings.Selectors.agentNumberInput).val('');
                model.settings.Containers.leadForm.hide();
                $(model.settings.Selectors.leadFormThankYouAndError).hide();
                var list = $(model.settings.Selectors.detailList);
                var top = list.offset() ? list.offset().top : 0;
                $('html, body').animate({
                        scrollTop: top
                    }, model.settings.animationSpeed,
                    function () {
                        model.searcher.endSearch();
                    });
            };
            model.mapViewChanged = function () {
                if (!model.searcher.isSearching) {
                    model.searcher.viewportSearch();
                }
            };

            model.toggleClass = function (v) {
                return v ? 'addClass' : 'removeClass';
            }

            model.centerClick = function (marker) {
                return model.google.AddEventListener(marker, 'click', model.focusMap);
            };
            model.markerClick = function (marker) {
                return model.google.AddEventListener(marker, 'click', model.selectLocation);
            };
            model.viewChanged = function (map) {
                return model.google.AddEventListenerOnce(map, 'idle', model.mapViewChanged);
            }
            //#endregion
            //#region  Messaging 
            model.showMessage = function (msg) {
                model.settings.Containers.messageContainer.html(msg);
            };
            model.showError = function (msg) {
                model.settings.Containers.messageContainer.html(msg).addClass('error');
            };
            model.clearMessage = function () {
                model.settings.Containers.messageContainer.html('').removeClass('error');
            };
            model.showSearchResultsMessage = function (numResults, lastName, address, showHelpMessage) {
                var msg;
                var result = model.settings.Constants.resultLabel + ((numResults > 1) ? 's' : '');
                if (lastName.length && address.length) {
                    msg = numResults + ' ' + result + ' ' + model.settings.Constants.forLabel + ' <span>' + lastName + '</span> ' + model.settings.Constants.nearLabel + ' <span>' + address + '</span>';
                } else if (lastName.length) {
                    msg = numResults + ' ' + result + ' ' + model.settings.Constants.forLabel + ' <span>' + lastName + '</span>';
                } else if (address.length && model.searcher.searchParameters.searchType !== model.searcher.searchTypes.viewport) {
                    msg = numResults + ' ' + result + ' ' + model.settings.Constants.forLabel + ' <span>' + address + '</span>';
                } else {
                    msg = numResults + ' ' + result;
                }

                if(showHelpMessage){
                    msg += "<br><br>Please zoom in or select a pin to see results";
                }
                model.showMessage(msg);
            };
            model.showYourLocationMessage = function (address) {
                var msg = '';
                var addressArray = address.split(' ');
                if (addressArray.length) {
                    msg = '<span>' + model.settings.Constants.yourLocationLabel + ' </span><span class="location">' + addressArray[0] + ' ' + addressArray[1] + '</span>';
                }
                model.showYourLocation(msg);
            };
            model.showYourLocation = function (msg) {
                model.settings.Containers.locationContainer.html(msg);
            };
            model.clearYourLocation = function () {
                model.settings.Containers.locationContainer.html('');
            };
            model.searchInitClearMessage = function () {
                model.clearMessage();
                model.clearYourLocation();
            };
            model.noResults = function () {
                model.clearAgentCards();
                model.showError('No results found');
            };
            //#endregion
            //#region  Search Result / Agent Cards 
            model.processAddressResults = function (response, lastName, address) {
                var ret = new $.Deferred(function () {
                    model.clearAgentCards();
                    model.populateAddresses(response.Results);
                    model.mapper.mapEntities(model.entityAddresses, model.searcher.searchParameters.fitResults);

                    //only show the list of results when below the zoom threshold or when there is only one page of results
                    if (model.mapper.ShowReps() || response.Results.length <= model.settings.maxCountToShowInList) {

                        //make another call to get FRs' details
                        model.searcher.doSearchForFRDetails(model.searcher.searchParameters.lat, model.searcher.searchParameters.lng );
                    }else{
                        model.showSearchResultsMessage(response.Results.length, lastName, address, true);
                    }
                });
                ret.resolve();
                return ret;
            };

            model.processDetailedResults = function (response, lastName, address) {
                var ret = new $.Deferred(function () {
                    model.clearAgentCards();
                    model.populateAddresses(response.Results);
                    model.mapper.updateMarkersWithDetailedEntities(model.entityAddresses);

                    //only show the list of results when below the zoom threshold or when there is only one page of results
                    if (model.mapper.ShowReps() || response.Results.length <= model.settings.maxCountToShowInList) {
                        model.buildAgentCardsForAllAddresses();
                        model.showNextSetOfAgentCards();
                    }
                    model.showSearchResultsMessage(response.Results.length, lastName, address);
                });
                ret.resolve();
                return ret;
            };            
            model.buildAgentCardsForAllAddresses = function () {
                var addresses = $.map(model.entityAddresses, function(entities) {
                    return { people: entities };
                });

                $.each(addresses, function (i, peopleAtAddress) {
                    var pinLetter = (i > 25) ? model.settings.delimiter[25] : model.settings.delimiter[i];
                    model.buildAgentCardsForEntitiesAtAddress(peopleAtAddress.people, pinLetter);
                });
            };
            model.clearAgentCards = function () {
                model.agentCards = $();
                $(model.settings.Selectors.detailList).children().remove();
            };
            model.buildAgentCardsForEntitiesAtAddress = function (entitiesAtAddress, pinLetter) {
                var formattedAgents = model.formatter.formatAgents(entitiesAtAddress, pinLetter);
                model.bindAgentCards(formattedAgents);
            };
            model.getAgentCardsFromTemplate = function (agents) {
                var agentTemplate = _.template($(model.settings.Selectors.resultsTemplate).html());
                var agentHtml = agentTemplate({
                    agents: agents
                });

                //TODO why is underscore inserting text nodes? It's because of whitespace inside the template. This happens because of the .html() bit above.
                var agentCardsWrapped = $(agentHtml).filter(function () {
                    return this.nodeType === Node.ELEMENT_NODE; //1
                });
                return agentCardsWrapped;
            };
            model.bindAgentCards = function (agents) {
                model.agentCards.push.apply(model.agentCards, model.getAgentCardsFromTemplate(agents));
            };
            model.showNextSetOfAgentCards = function() {
                var $list = $(model.settings.Selectors.detailList);
                var numAlreadyVisible = $list.children().length;
                var agentCardCount = model.agentCards !== undefined ? model.agentCards.length : 0;
                for (var i = numAlreadyVisible; (i < numAlreadyVisible + model.settings.numPerPage) && (i < agentCardCount); i++) {
                    var $card = model.agentCards.eq(i);
                    var $img = $card.find('.image img');
                    $img.attr('src', $img.data('src')); //causes images to load only when they are shown, without this Chrome was loading all images as the cards were created in memory
                    $list.append($card);
                }
            };
            model.populateAddresses = function (entityArray) {
                var addresses = {};

                $.each(entityArray, function (i, entity) {
                    //create a collection of addresses with an array of related entities
                    var addrKey = entity.Address.Latitude + '' + entity.Address.Longitude;

                    if (_.has(addresses, addrKey)) {
                        //address is in collection
                        addresses[addrKey].push(entity);
                    } else {
                        //address isn't in collection yet
                        addresses[addrKey] = [entity];
                    }
                });
                model.entityAddresses = addresses;
            };       

            //#endregion
            return model;
        };

        var createLocatorModel = function (config) {
            config.leadForm = (window.leadFormModels !== undefined && window.leadFormModels.length) ? window.leadFormModels[0] : null;
            var model = new locatorModel(config);
            window.locatorModels = window.locatorModels || [];
            window.locatorModels.push(model);
            return model;
        };

        return {
            Create: createLocatorModel
        };
    })();

    nml.GoogleMaps = (function () {
        var googleApiModel = function () {
            var gm = {};
            var me = {}; // for private members

            me.mapsUrl = "https://maps.google.com/maps/api/js?v=3&client=gme-northwesternmutual&sensor=false&callback=nmcom.locator.GoogleMapsLoaded";
            gm.MarkerClusterer = window.MarkerClusterer || {};

            gm.mapTypeId = null;
            gm.controlPosition = null;
            gm.geocodeOK = null;

            gm.Initialize = function () {
                return nmcom.Services.Http.SCRIPT(me.mapsUrl);
            };
            gm.SetApiProperties = function () {
                gm.mapTypeId = google.maps.MapTypeId.ROADMAP;
                gm.controlPosition = google.maps.ControlPosition.TOP_RIGHT;
                gm.geocodeOK = google.maps.GeocoderStatus.OK;
            };

            //#region  Google Map API Wrapper 
            gm.GoogleApiIsReady = function () {
                return typeof google === 'object' && typeof google.maps === 'object';
            };
            gm.CreateMap = function (element) {
                return new google.maps.Map(element);
            };
            gm.SetMapOptions = function (map, options) {
                return (map === undefined || map === null) ? {} : map.setOptions(options);
            };
            gm.CreateGeocoder = function () {
                return new google.maps.Geocoder();
            };
            gm.CreateLatLng = function (lat, lng) {
                return new google.maps.LatLng(lat, lng);
            };
            gm.CreateLatLngBounds = function (southWestLatLng, northEastLatLng) {
                return new google.maps.LatLngBounds(southWestLatLng, northEastLatLng);
            };
            gm.CreateMarker = function (options) {
                return new google.maps.Marker(options);
            };
            gm.CreateSize = function (width, height) {
                return new google.maps.Size(width, height);
            };
            gm.CreatePoint = function (x, y) {
                return new google.maps.Point(x, y);
            };
            gm.CreateMarkerClusterer = function (map, markers, options) {
                return new gm.MarkerClusterer(map, markers, options);
            };
            gm.AddMarkers = function (clusterer, markers) {
                clusterer.addMarkers(markers);
            };
            gm.ClearMarkers = function (clusterer) {
                clusterer.clearMarkers();
            };
            gm.FitMapToMarkers = function (clusterer) {
                clusterer.fitMapToMarkers();
            };
            gm.AddEventListener = function (map, eventName, callback) {
                return google.maps.event.addListener(map, eventName, callback);
            };
            gm.AddEventListenerOnce = function (map, eventName, callback) {
                return google.maps.event.addListenerOnce(map, eventName, callback);
            };
            gm.RemoveEventListener = function (listener) {
                google.maps.event.removeListener(listener);
            };
            gm.GetPosition = function (marker) {
                return marker.getPosition();
            }
            gm.GetBounds = function (map) {
                return map.getBounds();
            };
            gm.FitBounds = function (map, bounds) {
                return map.fitBounds(bounds);
            };
            gm.GetCenter = function (map) {
                return gm.GetBounds(map).getCenter();
            };
            gm.GetZoom = function (map) {
                return map.getZoom();
            };
            gm.SetZoom = function (map, zoomLevel) {
                return map.setZoom(zoomLevel);
            };
            gm.PanTo = function (map, latlng) {
                return map.panTo(latlng);
            };
            gm.PanToBounds = function (map, bounds) {
                return map.panToBounds(bounds);
            };
            gm.SetMarkerMap = function (marker, map) {
                return marker.setMap(map);
            };
            gm.DrawCircle = function (options) {
                return new google.maps.Circle(options);
            };
            //#endregion
            return gm;
        };

        var createGoogleApiModel = function () {
            var model = new googleApiModel();
            return model;
        };
        return {
            Create: createGoogleApiModel
        };
    })();

    nml.Mapper = (function () {
        var mapperModel = function (settings, googleMapApi, centerClick, markerClick, viewChanged) {
            var model = {};
            var me = {}; // for private members

            model.api = googleMapApi;
           
            me.minZoomLevel = 3; //USA
            me.initialZoomLevel = 9;
            me.maxZoomLevel = 12; //stop map bounding from zooming in too far for only one result
            me.showRepListZoomThreshold = 11;
            me.smallMarkerThreshold = 5;
            me.showAccuracyCircle = false;
            me.minimumAccuracy = 16090; // 10 miles - for accuracy circle
            me.markers = {
                pinCenter: {
                    url: '/NMCOM/assets/img/icons/pincenter.png',
                    size: model.api.CreateSize(25, 25),
                    origin: model.api.CreatePoint(0, 0),
                    anchor: model.api.CreatePoint(13, 13)
                },
                pinSmall: {
                    url: '/NMCOM/assets/img/icons/pinsmall.png',
                    size: model.api.CreateSize(8, 13),
                    origin: model.api.CreatePoint(0, 0),
                    anchor: model.api.CreatePoint(3, 13)
                },
                pinMedium: {
                    url: '/NMCOM/assets/img/icons/pinmedium.png',
                    size: model.api.CreateSize(15, 24),
                    origin: model.api.CreatePoint(0, 0),
                    anchor: model.api.CreatePoint(6, 24)
                },
                pinIconSingleRep: {
                    url: '/NMCOM/assets/img/icons/pinrep.png',
                    size: model.api.CreateSize(32, 40),
                    origin: model.api.CreatePoint(0, 0),
                    anchor: model.api.CreatePoint(17, 40)
                },
                pinIconSingleRepHighlight: {
                    url: '/NMCOM/assets/img/icons/pinreph.png',
                    size: model.api.CreateSize(32, 40),
                    origin: model.api.CreatePoint(0, 0),
                    anchor: model.api.CreatePoint(17, 40)
                },
                pinIconMultiRep: {
                    url: '/NMCOM/assets/img/icons/pinreps.png',
                    size: model.api.CreateSize(32, 40),
                    origin: model.api.CreatePoint(0, 0),
                    anchor: model.api.CreatePoint(17, 40)
                },
                pinIconMultiRepHighlight: {
                    url: '/NMCOM/assets/img/icons/pinrepsh.png',
                    size: model.api.CreateSize(32, 40),
                    origin: model.api.CreatePoint(0, 0),
                    anchor: model.api.CreatePoint(17, 40)
                },
                pinIconLetter: {
                    url: '/NMCOM/assets/img/views/office-locator/pindrop-' + settings.Constants.pinLetterToken + '.png',
                    size: model.api.CreateSize(38, 52),
                    origin: model.api.CreatePoint(0, 0),
                    anchor: model.api.CreatePoint(18, 52)
                }
            };
            me.accuracyCircleColor = '#5c778b';
            me.accuracyFillColor = '#5c778b';
            me.minLat = 90; //north pole
            me.maxLat = 0; //equator
            me.minLng = 0; //prime meridian
            me.maxLng = -180; //international date line
            me.idleListener = null;
            //#region  Map Settings 
            me.clusterSettings = { 
                maxZoom: 1,
                gridSize: 60,
                styles: [
                    {
                        url: '/NMCOM/assets/img/icons/cluster1.png',
                        height: 40,
                        width: 71,
                        anchorText: [-1, 13],
                        anchorIcon: [20, 20],
                        textColor: '#002b49',
                        textSize: 18
                    }
                ],
                averageCenter: true,
                minimumClusterSize: 3
            };
            me.mapSettings = {
                zoom: me.minZoomLevel,
                minZoom: me.minZoomLevel,
                center: model.api.CreateLatLng(38, -98),
                mapTypeId: model.api.mapTypeId,
                scaleControl: true,
                scaleControlOptions: {
                    position: model.api.controlPosition
                },
                streetViewControl: false,
                panControl: false
            };
            me.enableUIOptions = {
                disableDefaultUI: false,
                draggable: true,
                zoomControl: true,
                scrollwheel: true,
                disableDoubleClickZoom: false
            };
            me.disableUIOptions = {
                disableDefaultUI: true,
                draggable: false,
                zoomControl: false,
                scrollwheel: false,
                disableDoubleClickZoom: true
            };
            //#endregion

            model.mapObjects = {
                centerOfUsa: null,
                mapInstance: null,
                markerClusterer: null,
                geocoderInstance: null,
                markers: [],
                centerPin: null,
                accuracyCircle: null
            };
            model.Setup = function () {
                model.mapObjects.geocoderInstance = model.api.CreateGeocoder();
                var mapDiv = settings.Containers.mapContainer;
                model.mapObjects.mapInstance = model.api.CreateMap(mapDiv);
                model.mapObjects.markerClusterer = model.api.CreateMarkerClusterer(model.mapObjects.mapInstance, model.mapObjects.markers, me.clusterSettings);
                model.resetMap();
            }

            //#region  Map Functions 
            model.mapEntities = function (entityAddresses, fitResults) {

                model.removeMarkers();
                var zIndex = 1;
                var addresses = $.map(entityAddresses, function (entities) {
                    return { people: entities };
                });

                $.each(addresses, function (i, office) {

                    var peopleAtAddress = office.people;
                    var address = peopleAtAddress[0].Address; //get the Address of the first entity, they'll all be the same
                    var lat = parseFloat(address.Latitude);
                    var lng = parseFloat(address.Longitude);

                    

                    var pinLatLng = model.api.CreateLatLng(lat, lng);
                    var markerOptions = {
                        icon: null,
                        position: pinLatLng,
                        cursor: 'pointer',
                        entities: peopleAtAddress,
                        entityCount: peopleAtAddress.length,
                        zIndex: zIndex,
                        pinLetter: (i > 25) ? settings.delimiter[25] : settings.delimiter[i]
                    };
               
                    zIndex++;

                    var entityMarker = model.api.CreateMarker(markerOptions);

                    markerClick(entityMarker);

                    model.mapObjects.markers.push(entityMarker);

                });

                model.api.AddMarkers(model.mapObjects.markerClusterer, model.mapObjects.markers);

                if (fitResults) {
                    model.setMapBounds();
                }
                model.setMarkers();
            };
            model.setMarkers = function () {
                var zoomLevel = model.api.GetZoom(model.mapObjects.mapInstance);
                for (var i = 0; i < model.mapObjects.markers.length; i++) {
                    model.setMarkerIcon(model.mapObjects.markers[i], zoomLevel);
                }
            };

            model.updateMarkersWithDetailedEntities = function (detailedEntities) {
                $.each(detailedEntities, function (i, entity) {
                    var entityAddress = entity[0].Address;
                    for (var i = 0; i < model.mapObjects.markers.length; i++) {

                        var marker = model.mapObjects.markers[i];
                        var markerAddress = marker.entities[0].Address;

                        if(entityAddress.Street == markerAddress.Street 
                            && entityAddress.City == markerAddress.City 
                            && entityAddress.State == markerAddress.State 
                            && entityAddress.Zip == markerAddress.Zip){

                            marker.entities = entity;

                            model.api.SetMarkerMap(marker, model.mapObjects.mapInstance);
                            break;
                        }
                    }
                });

            };

            model.getIcon = function (marker) {
                var newmarker = null;
                if (settings.markerType === nml.markerTypes.people) {
                    newmarker = marker.entityCount > 1 ? me.markers.pinIconMultiRep : me.markers.pinIconSingleRep;
                }
                if (settings.markerType === nml.markerTypes.alpha) {
                    newmarker = $.extend({}, me.markers.pinIconLetter);
                    var url = nmcom.replaceString(settings.Constants.pinLetterToken, marker.pinLetter, newmarker.url);
                    newmarker.url = url;
                }
                return newmarker;
            }
            model.getPin = function (zoomLevel) {
                return zoomLevel > me.smallMarkerThreshold ? me.markers.pinMedium : me.markers.pinSmall;
            }
            model.getHighlightIcon = function (marker) {
                var newmarker = null;
                if (settings.markerType === nml.markerTypes.people) {
                    newmarker = marker.entityCount > 1 ? me.markers.pinIconMultiRepHighlight : me.markers.pinIconSingleRepHighlight;
                }
                if (settings.markerType === nml.markerTypes.alpha) {
                    newmarker = model.getIcon(marker);
                }
                return newmarker;
            }
            model.createHighlightMarker = function (marker) {
                marker.icon = model.getHighlightIcon(marker);
                marker.zIndex = 9999;
                return marker;
            }
            model.highlightMarker = function (marker) {
                var hmarker = model.createHighlightMarker(marker);
                model.api.SetMarkerMap(marker, model.mapObjects.mapInstance);
            }
            model.setMarkerIcon = function (marker, zoomLevel) {
                if (zoomLevel === undefined) {
                    zoomLevel = model.api.GetZoom(model.mapObjects.mapInstance);
                }
                var newIcon;
                if (zoomLevel >= me.showRepListZoomThreshold) {
                    newIcon = model.getIcon(marker);
                } else {
                    newIcon = model.getPin(zoomLevel);
                }
                if (marker.icon === null || newIcon.url !== marker.icon.url) {
                    marker.icon = newIcon;
                    model.api.SetMarkerMap(marker, model.mapObjects.mapInstance);
                }
            };
            model.resetMap = function () {
                model.api.SetMapOptions(model.mapObjects.mapInstance, me.mapSettings);
                model.removeMarkers();
            };
            model.setMapBounds = function () {
                // if one result, focus on that location.
                if (model.mapObjects.markers.length === 1) {
                    model.focusMapOnAddress(model.mapObjects.markers[0]);
                } else {
                    model.api.FitMapToMarkers(model.mapObjects.markerClusterer);
                }
            };
            model.focusMapOnAddress = function (marker) {
                var latLngPoint = model.api.GetPosition(marker);
                if (model.api.GetZoom(model.mapObjects.mapInstance) < me.maxZoomLevel) {
                    model.api.SetZoom(model.mapObjects.mapInstance, me.maxZoomLevel);
                }
                model.api.PanTo(model.mapObjects.mapInstance, latLngPoint);
            };
            model.removeCenterPin = function () {
                if (model.mapObjects.centerPin !== null && model.mapObjects.centerPin !== undefined) {
                    model.api.SetMarkerMap(model.mapObjects.centerPin, null);
                    model.mapObjects.centerPin = null;
                }
                if (model.mapObjects.accuracyCircle !== null && model.mapObjects.accuracyCircle !== undefined) {
                    model.api.SetMarkerMap(model.mapObjects.accuracyCircle, null);
                    model.mapObjects.accuracyCircle = null;
                }
            };
            model.removeMarkers = function () {
                if (model.mapObjects.markerClusterer) {
                    model.api.ClearMarkers(model.mapObjects.markerClusterer);
                }
                $.each(model.mapObjects.markers, function (i, marker) {
                    marker.icon = null;
                    model.api.SetMarkerMap(marker, null);
                });
                model.mapObjects.markers = [];
            };
            model.setCenterPin = function (lat, lng, accuracy) {
                model.removeCenterPin();
                var pinLatLng = model.api.CreateLatLng(lat, lng);

                var markerOptions = {
                    position: pinLatLng,
                    icon: me.markers.pinCenter,
                    cursor: 'pointer',
                    zIndex: -1
                };

                model.mapObjects.centerPin = model.api.CreateMarker(markerOptions);
                centerClick(model.mapObjects.centerPin);
                model.api.SetMarkerMap(model.mapObjects.centerPin, model.mapObjects.mapInstance);

                if (me.showAccuracyCircle) {
                    model.mapObjects.accuracyCircle = model.api.DrawCircle({
                        center: pinLatLng,
                        radius: (accuracy < me.minimumAccuracy ? me.minimumAccuracy : accuracy),
                        map: model.mapObjects.mapInstance,
                        fillColor: me.accuracyFillColor,
                        fillOpacity: 0.3,
                        strokeColor: me.accuracyCircleColor,
                        strokeOpacity: 0.8,
                        strokeWeight: 1,
                        cursor: 'none',
                        clickable: false,
                        zIndex: -1
                    });
                }
            };
            model.enableUI = function () {
                model.api.SetMapOptions(model.mapObjects.mapInstance, me.enableUIOptions);
            };
            model.disableUI = function () {
                model.api.SetMapOptions(model.mapObjects.mapInstance, me.disableUIOptions);
            };
            model.clearViewChangedEvents = function () {
                if (model.idleListener !== null) {
                    model.api.RemoveEventListener(model.idleListener);
                    model.idleListener = null;
                }
            };
            model.bindViewChangedEvents = function () {
                model.clearViewChangedEvents();
                viewChanged(model.mapObjects.mapInstance);
            };
            model.PanTo = function (lat, lng) {
                model.api.PanTo(model.mapObjects.mapInstance, model.api.CreateLatLng(lat, lng));
            };
            model.PanToBounds = function (bounds) {
                model.api.PanToBounds(model.mapObjects.mapInstance, bounds);
            };
            model.GetZoom = function () {
                return model.api.GetZoom(model.mapObjects.mapInstance);
            };
            model.ZoomIn = function (levels) {
                model.SetZoom(model.GetZoom() + levels);
            };
            model.ZoomOut = function (levels) {
                model.SetZoom(model.GetZoom() - levels);
            };
            model.SetDefaultZoom = function () {
                model.SetZoom(me.showRepListZoomThreshold);
            };
            model.SetZoom = function (zoomLevel) {
                model.api.SetZoom(model.mapObjects.mapInstance, zoomLevel);
            };
            model.CanZoomOut = function () {
                return model.GetZoom() > me.minZoomLevel;
            }
            model.GetBoundsOfMap = function () {
                return model.api.GetBounds(model.mapObjects.mapInstance);
            };
            model.FitBoundsOfMap = function (bounds) {
                return model.api.FitBounds(model.mapObjects.mapInstance, bounds);
            };
            model.GetCenterOfMap = function () {
                return model.api.GetCenter(model.mapObjects.mapInstance);
            };
            model.ShowReps = function () {
                return model.GetZoom() >= me.showRepListZoomThreshold;
            }
            //#endregion
            //#region  Geo Functions 
            model.codeLatLng = function (lat, lng, successCallback, failureCallback) {
                var latlng = model.api.CreateLatLng(lat, lng);
                model.mapObjects.geocoderInstance.geocode({ 'latLng': latlng }, function (results, status) {

                    if (status === model.api.geocodeOK) {
                        if (successCallback !== undefined) {
                            successCallback(results);
                        }
                    } else {
                        if (failureCallback !== undefined) {
                            failureCallback();
                        }
                    }
                });
            };
            model.codeAddress = function (address, successCallback, failureCallback) {
                model.mapObjects.geocoderInstance.geocode({ 'address': address }, function (results, status) {

                    if (status === model.api.geocodeOK) {
                        if (successCallback !== undefined) {
                            successCallback(results);
                        }
                    } else {
                        if (failureCallback !== undefined) {
                            failureCallback();
                        }
                    }
                });
            };
            //#endregion
            return model;
        };
        var createMapper = function (settings, googleMapApi, centerClick, markerClick, viewChanged) {
            var model = new mapperModel(settings, googleMapApi, centerClick, markerClick, viewChanged);
            model.Setup();
            return model;
        };
        return {
            Create: createMapper
        };
    })();

    nml.Searcher = (function () {
        var searcherModel = function (settings, mapper, clearMessages, processAddressResults, processDetailedResults, noResults, locationMessage, errorMessage) {
            var model = {};
            var me = {}; // for private members

            model.settings = $.extend(true, {}, nml.defaults, settings);

            me.userIpLocation = settings.userIpLocation;
            me.entityServiceUrl = settings.searchApi;
            me.entityAddressServiceUrl = settings.entityAddressServiceUrl;
            me.geoLocationAccuracyThreshold = 80467; // 50 miles
            me.geoLocationErrorCodes = {
                PERMISSION_DENIED: 1,
                POSITION_UNAVAILABLE: 2,
                TIMEOUT: 3
            };
            model.mapper = mapper;
            me.locationTypes = {
                streetAddress: 'street_address',                    // Zoom: 16-18 | Street Address
                neighborhood: 'neighborhood',                       // Zoom: 15    | "Downtown"
                adminAreaLevelThree: 'administrative_area_level_3', // Zoom: 15    | Township
                postalCode: 'postal_code',                          // Zoom: 12-14 | City, State, Postal Code
                locality: 'locality',                               // Zoom: 10-11 | City, State
                adminAreaLevelTwo: 'administrative_area_level_2',   // Zoom: 8-9   | County
                adminAreaLevelOne: 'administrative_area_level_1',   // Zoom: 6-7   | State
                political: 'political',                             // Zoom: 1-5   | City/County, State (only type)
                country: 'country'                                  // Zoom: 1-5   | Country
            };
            model.zoomOverride = undefined;

            model.isSearching = false;
            model.isPromptingGeolocation = false;
            model.searchTypes = {
                geolocate: 'geolocate',
                input: 'input',
                viewport: 'viewport'
            };
            model.searchParameters = {
                lat: 0,
                lng: 0,
                bounds: null,
                types: settings.types,
                comboname: '',
                agentNum: '',
                lastName: '',
                address: '',
                fitResults: false,
                searchType: null
            };

            //#region  Inputs 
            model.LastNameInputValue = function (value) {
                var input = settings.Input.nameInput;
                if (!input.length) {
                    return '';
                }

                if (value !== undefined) {
                    return input.val(value);
                }

                value = input.val();
                return (value !== undefined) ? value : '';
            }
            model.AddressInputValue = function (value) {
                var input = settings.Input.addressInput;
                if (!input.length) {
                    return '';
                }

                if (value !== undefined) {
                    return input.val(value);
                }

                value = input.val();
                return (value !== undefined) ? value : '';
            }
            //#endregion

            //#region  Utility 
            model.initSearch = function () {
                model.isSearching = true;
                model.preventMouseScroll();
                model.mapper.disableUI();
                model.mapper.clearViewChangedEvents();
                clearMessages();
                model.clearSearchParameters();
            };
            model.endSearch = function () {
                model.isSearching = false;
                model.allowMouseScroll();
                model.mapper.bindViewChangedEvents();
                model.mapper.enableUI();
            };
            model.clearSearchParameters = function () {
                model.searchParameters.lat = false;
                model.searchParameters.lng = false;
                model.searchParameters.bounds = null;
                model.searchParameters.lastName = '';
                model.searchParameters.address = '';
                model.searchParameters.fitResults = false;
            };
            model.zoomIn = function (levels) {
                model.mapper.ZoomIn(levels);
            };
            model.zoomOut = function (levels) {
                model.mapper.ZoomOut(levels);
            };
            model.stopWheel = function (e) {
                if (!e) { /* IE7, IE8, Chrome, Safari */
                    e = window.event;
                }
                if (e.preventDefault) { /* Chrome, Safari, Firefox */
                    e.preventDefault();
                }
                e.returnValue = false; /* IE7, IE8 */
            };
            model.preventMouseScroll = function () {
                $(document).bind('mousewheel DOMMouseScroll', function () {
                    model.stopWheel();
                });
            };
            model.allowMouseScroll = function () {
                $(document).unbind('mousewheel DOMMouseScroll');
            };
            //#endregion

            //#region  Search 

            // querystring search: replaces initial search if querystring params are present
            model.querystringSearch = function () {
                var params = model.searchParameters;
                var urlParams = window.location.search;
                var addressPattern = /address=([^&]+)/i;
                var latlngPattern = /latlng=([0-9-.]+),([0-9-.]+)/i;
                var lastNamePattern = /lastname=([^&]+)/i;
                var zoomPattern = /zoom=([^&]+)/i;
                var isInputSearch = false;

                if (zoomPattern.test(urlParams)) {
                    model.zoomOverride = parseInt(decodeURIComponent(zoomPattern.exec(urlParams)[1]));
                    model.mapper.SetZoom(model.zoomOverride);
                } else {
                    model.mapper.SetDefaultZoom();
                }

                // address or name provided, do an input search
                if (addressPattern.test(urlParams)) {
                    params.address = decodeURIComponent(addressPattern.exec(urlParams)[1]).replace(/\+/g, " ");
                    model.AddressInputValue(params.address);
                    isInputSearch = true;
                }
                if (lastNamePattern.test(urlParams)) {
                    params.lastName = decodeURIComponent(lastNamePattern.exec(urlParams)[1]);
                    model.LastNameInputValue(params.lastName);
                    isInputSearch = true;
                }
                if (isInputSearch) {
                    params.searchType = model.searchTypes.input;
                    model.search();
                } else if (latlngPattern.test(urlParams)) {
                    params.lat = latlngPattern.exec(urlParams)[1];
                    params.lng = latlngPattern.exec(urlParams)[2];
                    params.searchType = model.searchTypes.viewport;
                    model.mapper.PanTo(params.lat, params.lng);
                    model.doSearchForAddresses(params.lat, params.lng);
                } else {
                    model.gpsSearch();
                }
                model.zoomOverride = undefined;
            };

            // input search: if address is provided, locate the address. if successful, set the map bounds and search the visible area, zooming out when no results.
            //               if last name is provided, the results will be filtered. If no address is provided, the entire list will be used.
            model.search = function () {
                if (model.isSearching && !model.isPromptingGeolocation) {
                    return;
                }
                model.searchParameters.searchType = model.searchTypes.input;
                var name = model.LastNameInputValue();
                var address = model.AddressInputValue();
                if (name.length || address.length) {
                    model.initSearch();
                    model.mapper.removeMarkers();

                    // address search:set default zoom level, geolocate the string, then focus on the location and search the bounds.
                    if (name.length) {
                        model.searchParameters.lastName = name;
                    }
                    if (address.length) {
                        model.searchParameters.address = address;

                        model.mapper.codeAddress(model.searchParameters.address, function (results) {
                            var result = results[0];
                            model.searchParameters.address = result.formatted_address.replace(', USA', '');

                            if (model.searchParameters.address !== '') {
                                model.searchParameters.lat = result.geometry.location.lat();
                                model.searchParameters.lng = result.geometry.location.lng();

                                //if (model.zoomOverride !== undefined) {
                                //    model.mapper.SetZoom(model.zoomOverride);
                                //} else {
                                //    model.mapper.SetDefaultZoom();
                                //}

                                // pan the viewport to include the bounds of the located area
                                //model.mapper.PanTo(model.searchParameters.lat, model.searchParameters.lng);

                                // set the viewport to include the bounds of the located area
                                model.mapper.FitBoundsOfMap(result.geometry.viewport);

                                // setting the map bounds always seems to include extra space around the viewport
                                if (model.zoomOverride === undefined) {
                                    model.zoomIn(1);
                                }

                                // set the search bounds to the located area (will probably leave unsearched space in the viewport)                                
                                //model.searchParameters.bounds = result.geometry.viewport;

                                // tell the mapper to set the viewport to the area covered by results of the search.
                                // This only happens after the search, so it's likely that part of the viewport will be unsearched
                                //model.searchParameters.fitResults = true;

                                model.viewportSearch();
                            } else {
                                errorMessage("This address cannot be located: " + model.searchParameters.address);
                            }
                        }, function () {
                            errorMessage("This address cannot be located: " + model.searchParameters.address);
                            model.endSearch();
                        });
                    } else {
                        model.searchParameters.fitResults = true;
                        model.doSearchForAddresses(); // name search only
                    }



                }
            };
            // default search: GPS will set the map bounds, or the user can pan and zoom to get more results
            model.viewportSearch = function (getBounds) {
                if (!model.isSearching) {
                    model.initSearch();
                }
                model.mapper.setMarkers();
                // preserve the last name search if there is one.
                model.searchParameters.lastName = model.LastNameInputValue();
                if (getBounds || getBounds === undefined) {
                    model.searchParameters.bounds = model.mapper.GetBoundsOfMap();
                    var center = model.mapper.GetCenterOfMap();
                    model.searchParameters.lat = center.lat();
                    model.searchParameters.lng = center.lng();
                }
                model.doSearchForAddresses();
            };

            // Geo locate: navigator.geolocation - when successful, set the map bounds and search the visible area
            model.gpsSearch = function () {
                if (model.isSearching) {
                    return;
                }
                model.initSearch();
                model.searchParameters.searchType = model.searchTypes.geolocate;
                if (navigator.geolocation) {
                    var geoOptions = {
                        timeout: 10 * 1000,
                        enableHighAccuracy: true
                    }
                    model.isPromptingGeolocation = true;
                    navigator.geolocation.getCurrentPosition(model.gpsLocateSuccess, model.gpsLocateError, geoOptions);
                } else { // Browser doesn't support Geolocation
                    model.maxMindSearch(false);
                }
            };

            // Geo locate: IP based location, provided by the server. when successful, set the map bounds and search the visible area
            model.maxMindSearch = function (geoLocateFailed) {
                var address = '';
                var userIpLoc = settings.userIpLocation;
                if (userIpLoc.CityName.length) {
                    address = userIpLoc.CityName + ', ';
                }
                if (userIpLoc.StateAbbr.length) {
                    address += userIpLoc.StateAbbr + ' ';
                }
                if (userIpLoc.PostalCode.length) {
                    address += userIpLoc.PostalCode;
                }

                // if we're falling back to maxmind and an address is not provided, tell the user that we cannot determine their location
                if (geoLocateFailed && address.length === 0) {
                    errorMessage(settings.Constants.geolocateFailureMessage);
                } else {
                    model.searchParameters.address = address;
                    model.searchParameters.lat = userIpLoc.Latitude;
                    model.searchParameters.lng = userIpLoc.Longitude;
                    model.mapper.setCenterPin(userIpLoc.Latitude, userIpLoc.Longitude, me.geoLocationAccuracyThreshold); // assume accuracy of 50 miles
                    model.mapper.SetDefaultZoom();
                    model.mapper.PanTo(model.searchParameters.lat, model.searchParameters.lng);
                    locationMessage(model.searchParameters.address);
                    model.viewportSearch();
                }
            };

            model.getSearchQuery = function() {
                var params = model.searchParameters;

                var query = {
                    Types: params.types,
                    PageIndex: 0,
                    PageSize: 0
                };

                if (params.lastName.length) {
                    query.LastName = params.lastName;
                }
                if (params.comboname.length) {
                    query.ComboName = params.comboname;
                }
                if (params.agentNum.length) {
                    query.AgentNumber = params.agentNum;
                }

                if (params.lat !== null && params.lat !== undefined && params.lng !== null && params.lng !== undefined) {
                    query.Center = { Lat: params.lat, Lng: params.lng };
                }

                if (params.bounds !== null && params.bounds !== undefined) {
                    var ne = params.bounds.getNorthEast();
                    var maxLat = ne.lat();
                    var maxLong = ne.lng();

                    var sw = params.bounds.getSouthWest();
                    var minLat = sw.lat();
                    var minLong = sw.lng();

                    query.Bounds = { NE: { Lat: maxLat, Lng: maxLong }, SW: { Lat: minLat, Lng: minLong } };
                }

                return query;
            };	    

            // perform the search using the current search parameters
            model.doSearchForAddresses = function (lat, lng) {
                var query = model.getSearchQuery();
                 if(lat && lng){
                    query.Center = { Lat: lat, Lng: lng };
                    query.Distance = 0.01; //limit search radius to be very small so that we get results for lat lng
                }                 
              
                // TODO: Construct an object with the search parameters and pass that to the service as data (instead of constructing a querystring)
                nmcom.Services.Http.POST(me.entityAddressServiceUrl, null, JSON.stringify(query)).then(model.entityResultsReturned).then(model.deferEndSearch);
            };

                        // perform the search using the current search parameters
            model.doSearchForFRDetails = function (lat, lng) {
                 var query = model.getSearchQuery();
                 if(lat && lng){
                    query.Center = { Lat: lat, Lng: lng };
                    query.Distance = 0.01; //limit search radius to be very small so that we get results for lat lng
                } 

              nmcom.Services.Http.POST(me.entityServiceUrl, null, JSON.stringify(query)).then(model.entityDetailResultsReturned).then(model.deferEndSearch);

            };
            //#endregion

            model.gpsLocateSuccess = function (position) {
                model.isPromptingGeolocation = false;
                // check the accuracy before using thie data
                if (position.coords.accuracy < me.geoLocationAccuracyThreshold) {
                    model.searchParameters.lat = position.coords.latitude;
                    model.searchParameters.lng = position.coords.longitude;

                    model.mapper.setCenterPin(position.coords.latitude, position.coords.longitude, position.coords.accuracy);

                    model.mapper.codeLatLng(model.searchParameters.lat, model.searchParameters.lng, function (results) {
                        var result = results[0];
                        var city = "";
                        var state = "";
                        var zip = "";
                        for (var i = 0, len = result.address_components.length; i < len; i++) {
                            var ac = result.address_components[i];
                            if (ac.types.indexOf(me.locationTypes.locality) >= 0) {
                                city = ac.long_name;
                            }
                            if (ac.types.indexOf(me.locationTypes.adminAreaLevelOne) >= 0) {
                                state = ac.short_name;
                            }
                            if (ac.types.indexOf(me.locationTypes.postalCode) >= 0) {
                                zip = ac.short_name;
                            }
                        }
                        if (city !== '' && state !== '' && zip !== '') {
                            model.searchParameters.address = city + ", " + state + " " + zip;
                            model.searchParameters.lat = result.geometry.location.lat();
                            model.searchParameters.lng = result.geometry.location.lng();
                            model.mapper.SetDefaultZoom();
                            model.mapper.PanTo(model.searchParameters.lat, model.searchParameters.lng);
                            locationMessage(model.searchParameters.address);
                            model.viewportSearch();
                        } else {
                            model.maxMindSearch(true);
                        }
                    }, function () {
                        model.maxMindSearch(true);
                    });
                } else {
                    model.maxMindSearch(true);
                }
            };

            model.gpsLocateError = function (error) {
               model.isPromptingGeolocation = false;
               model.allowMouseScroll();
                switch (error.code) {
                case me.geoLocationErrorCodes.PERMISSION_DENIED:
                    model.settings.Input.geoLocationLink.hide();
                    model.mapper.resetMap();
                    model.isSearching = false;
                    break;
                case me.geoLocationErrorCodes.POSITION_UNAVAILABLE:
                    model.maxMindSearch(true);
                    break;
                case me.geoLocationErrorCodes.TIMEOUT:
                    model.maxMindSearch(true);
                    break;
                }
            };

            model.entityResultsReturned = function (response) {
                if (response.Results.length) {
                    processAddressResults(response, model.searchParameters.lastName, model.searchParameters.address);
                } else {
                    var geoSearch = (model.searchParameters.searchType === model.searchTypes.geolocate) || 
                                    (model.searchParameters.searchType === model.searchTypes.input && model.searchParameters.address.length > 0);

                    if (geoSearch && model.mapper.CanZoomOut()) {
                        model.zoomOut(1);
                        model.viewportSearch();
                    } else {
                        noResults();
                        model.endSearch();
                    }
                }
            };

            model.entityDetailResultsReturned = function (response) {
                if (response.Results.length) {
                    processDetailedResults(response, model.searchParameters.lastName, model.searchParameters.address);
                } 
            };            

            model.deferEndSearch = function() {
                setTimeout(function () {
                    model.endSearch();
                }, 1000);
            }

            return model;
        };
        var createSearcher = function (settings, mapper, clearMessages, processAddressResults, processDetailedResults, noResults, locationMessage, errorMessage) {
            var model = new searcherModel(settings, mapper, clearMessages, processAddressResults, processDetailedResults, noResults, locationMessage, errorMessage);
            return model;
        };
        return {
            Create: createSearcher
        };
    })();

    nml.Formatter = (function () {
        var formatModel = function (settings) {
            var model = {};

            model.agentTypes = {
                detached: 'Detached',
                managingPartner: 'MP',
                managingDirecotor: 'MD'
            };

            model.formatAgents = function (sortedAgents, pinLetter) {
                var formattedAgents = [];
                $.each(sortedAgents, function (i, entity) {
                        var agent = {
                            displayName: model.formatAgentName(entity),
                            displayTitle: model.formatAgentTitle(entity),
                            displayAddress: model.formatAgentAddress(entity),
                            displayPhone: model.formatAgentPhoneNumbers(entity),
                            designations: model.formatAgentDesignations(entity),
                            areasOfExpertise: model.formatAgentAreasOfExpertise(entity),
                            socialNetworks: model.formatAgentSocialNetworks(entity),
                            spokenLanguages: model.formatSpokenLanguages(entity),
                            displayBio: model.formatAgentBio(entity),
                            displayUrl: model.formatAgentWebsite(entity),
                            displayPhoto: model.formatAgentPhoto(entity),
                            email: entity.Email,
                            agentNum: entity.AgentNum,
                            directionsLink: model.formatDirectionsLink(entity),
                            dnoNum: entity.DnoNum != null ? entity.DnoNum : '',
                            noNum: entity.NoNum != null ? entity.NoNum : '',
                            pinLetter: pinLetter
                        };

                        formattedAgents.push(agent);
                });
                return formattedAgents;
            };
            model.formatAgentName = function (entity) {
                //if is detached office, use the Managing Partner Name
                var name = (entity.Type === model.agentTypes.detached) ? entity.ManagingPartnerName : entity.AgentName;

                var displayName = name.FirstName;
                if (name.MiddleName !== null && name.MiddleName.length) {
                    displayName += ' ' + name.MiddleName;
                }
                displayName += ' ' + name.LastName;
                if (name.Lineage !== null && name.Lineage.length > 1) {
                    displayName += ' ' + name.Lineage;
                }
                return displayName;
            };
            model.formatAgentAddress = function (entity) {
                var addr = entity.Address;
                var displayAddress = addr.Street + ' ' + addr.Building + '<br>' + addr.City + ', ' + addr.State + ' ' + addr.Zip;
                if (addr.ZipLastFour !== null && addr.ZipLastFour.length > 0) {
                    displayAddress += '-' + addr.ZipLastFour;
                }
                return displayAddress;
            };
            model.formatAgentPhoneNumbers = function (entity) {
                var phone = (entity.Type === model.agentTypes.detached) ? entity.ManagingPartnerPhone : entity.Phone;
                var displayPhone = '';
                if ($.isArray(phone)) { //multiple phone numbers in an array
                    $.each(phone, function (i, p) {
                        displayPhone += model.formatPhoneNumber(p);
                    });
                } else { //a single phone number as an object
                    if (phone !== null) {
                        displayPhone = model.formatPhoneNumber(phone);
                    }
                }
                return displayPhone;
            };
            model.formatPhoneNumber = function (phone) {
                var basePhone = phone.AreaCode + '-' + phone.ControlNum + '-' + phone.UniqueNum;
                var fullPhone = basePhone;
                if (phone.Extension !== - null && phone.Extension.length > 0) fullPhone += ' x' + phone.Extension;
                if (phone.Type.slice(-1).toLowerCase() === 'f') fullPhone = 'FAX ' + fullPhone;
                return '<li><a href="tel:' + basePhone + '">' + fullPhone + '</a></li>';
            };
            model.formatAgentWebsite = function (entity) {
                var url;
                if (entity.Type === model.agentTypes.detached) {
                    url = entity.RecruitingOfficeURL != null ? entity.RecruitingOfficeURL : "";
                } else {
                    url = entity.URL != null ? entity.URL : "";
                }
                var displayUrl = url;
                if (/http:/i.test(displayUrl) === false) { //URL doesn't already have protocol prefix
                    displayUrl = 'http://' + url;
                }
                return displayUrl;
            };
            model.formatAgentTitle = function (entity) {
                var displayTitle = entity.Title;
                if (entity.Type === model.agentTypes.managingPartner) {
                    displayTitle = settings.Constants.managingPartner;
                } else if (entity.Type === model.agentTypes.managingDirecotor) {
                    displayTitle = settings.Constants.managingDirector;
                }
                return displayTitle;
            };
            model.formatAgentDesignations = function (entity) {
                if (entity.Type === model.agentTypes.detached) {
                    return '';
                }
                var designationArray = entity.AgentDesignations.Designation;
                var displayDesignation = '';
                $.each(designationArray, function (i, d) {
                    displayDesignation += d;
                    if (designationArray[i + 1] !== undefined) {
                        displayDesignation += ', ';
                    }
                });
                return displayDesignation;
            };
            model.formatAgentBio = function (entity) {
                var displayBio = '';
                if (entity.BriefBio != null && entity.BriefBio.length) {
                    displayBio = settings.Constants.bioHeadline +  entity.BriefBio ;
                }
                return displayBio;
            };
            model.formatAgentAreasOfExpertise = function (entity) {
                if (entity.Type === model.agentTypes.detached) {
                    return '';
                }
                var aoeArray = entity.AreasOfExpertise.Expertise;
                var displayAoe = '';
                var hasEllipsis = false;
                $.each(aoeArray, function (i, aoe) {
                    if (displayAoe.length < 250) {
                        displayAoe += aoe;
                        if (aoeArray[i + 1] !== undefined) {
                            displayAoe += ', ';
                        }
                    } else if (!hasEllipsis) {
                        displayAoe += ' ...';
                        hasEllipsis = true;
                    }
                });
                if (displayAoe.length) {
                    displayAoe = settings.Constants.aoeHeadline + displayAoe + '.';
                }
                return displayAoe;
            };

            model.formatSpokenLanguages = function (entity) {
                if (entity.Type === model.agentTypes.detached) {
                    return '';
                }

                var languagesArray = entity.SpokenLanguages.Languages;
                var displayLanguages = '';

                $.each(languagesArray, function (i, lang) {

                    if(lang.toLowerCase() != 'english' ) {
                        displayLanguages += lang;

                        if (languagesArray[i + 1] !== undefined) {
                            displayLanguages += ', ';
                        }
                    }

                });

                if (displayLanguages.length) {
                    displayLanguages = settings.Constants.spokenLanguagesHeadline + displayLanguages + '.';
                }
                return displayLanguages;                


                
            };

            model.formatAgentSocialNetworks = function (entity) {
                if (entity.SocialNetwork.length && entity.SocialNetwork[0] !== undefined && entity.SocialNetwork[0].Sites !== undefined) {
                    return entity.SocialNetwork[0].Sites;
                }
                return [];
            };
            model.formatAgentPhoto = function(entity) {
                var photoUrl;
                if (entity.Type === model.agentTypes.detached) {
                    photoUrl = entity.ManagingPartnerPhotoUrl != null ? entity.ManagingPartnerPhotoUrl : '';
                } else {
                    photoUrl = entity.Photo != null ? entity.Photo : '';
                }
                return photoUrl;
            };
            model.formatDirectionsLink = function(entity) {
                var myLat = parseFloat(entity.Address.Latitude);
                var myLng = parseFloat(entity.Address.Longitude);
                var street = entity.Address.Street;
                var building = entity.Address.Building;
                var city = entity.Address.City;
                var state = entity.Address.State;
                var zip = entity.Address.Zip;

                var googleAddress = '';
                if (street != null && street.length > 0) googleAddress += street;
                if (building != null && building.length > 0) googleAddress += ' ' + building;
                if (city != null && city.length > 0) googleAddress += ' ' + city;
                if (state != null && state.length > 0) googleAddress += ' ' + state;
                if (zip != null && zip.length > 0) googleAddress += ' ' + zip;

                //build link to Google Maps  http://mapki.com/wiki/Google_Map_Parameters
                var mapsHref = 'http://maps.google.com?ll=';
                //check if device type is iPhone or iPad 6.0 or higher --> then display native apple maps otherwise display google diretions
                if (/iphone|ipod|ipad[^)]*OS (\d)/i.test(navigator.userAgent) && /(iphone|ipod|ipad)[^)]*OS (\d)/i.exec(navigator.userAgent)[2] >= 6) {
                    mapsHref = 'http://maps.apple.com?ll=';
                }

                mapsHref = mapsHref + myLat + "," + myLng + '&daddr=' + encodeURIComponent(googleAddress);

                return mapsHref;
            };

            return model;
        };
        var createFormatter = function (settings) {
            var model = new formatModel(settings);
            return model;
        };
        return {
            Create: createFormatter
        };
    })();

    nml.LeadForm = (function () {
        var leadFormModel = function (settings, clearMessage, formModel) {
            var model = {};
            var me = {}; // for private members
            me.animationSpeed = 500;

            model.IsVisible = false;
            model.toggleLeadForm = function (e) {
                e.preventDefault();
                if (settings.Containers.leadForm.is(":visible")) { //form already visible, close it
                    model.hideLeadForm();
                } else { //form not yet visible, show it
                    model.showLeadForm(e.target);
                }
            };
            model.formatLeadFormForlocator = function () {
                var campaignTrackingCookie = nmcom.getCookie(nmcom.leadTracking.settings.campaignTrackingCookieName);
                if (campaignTrackingCookie != null && campaignTrackingCookie.campaignName !== "") {
                    $(settings.Selectors.formCampName).val($.parseJSON(campaignTrackingCookie).campaignName);
                } else {
                    $(settings.Selectors.formCampName).val(settings.Constants.leadCampName);
                }

                formModel.hideAddressCityState();
                formModel.hideAgentNameAndCurrCustomterFields();
            };
            model.showLeadForm = function (anchor) {
                model.IsVisible = true;
                clearMessage();

                model.formatLeadFormForlocator();

                var $anchor = $(anchor);
                var $agentCard = $anchor.parents('li');
                var marginTop = parseInt($agentCard.css('marginTop'));
                var listItems = $(settings.Selectors.detailListItems);

                var topOfFirstCard = listItems.first().offset().top - marginTop;
                listItems.not($agentCard).hide();

                settings.Containers.leadForm.fadeIn(me.animationSpeed);

                //on desktop scroll to top of agent card.  On mobile scroll to top of contact form.
                //detect mobile by the agent info being below the photo     
                var topOfContactForm = settings.Containers.leadForm.offset().top;
                var topOfAgentPhoto = $agentCard.find('.photo').offset().top;
                var topOfAgentInfo = $agentCard.find('.advisor-info').offset().top;
                var scrollPos = (topOfAgentInfo > topOfAgentPhoto) ? topOfContactForm : topOfFirstCard;
                $(document).scrollTop(scrollPos);

                var agentNum = $anchor.siblings(settings.Selectors.agentNumberInput).val();
                settings.Containers.leadForm.find(settings.Selectors.agentNumberInput).val(agentNum);

                $anchor.text(settings.Constants.contactMeCloseLabel); // pulled in from advisorlocator.cshtml

                //remove lead tracking cookie because user picked a differnt FR
                nmcom.leadTracking.removeLeadTrackingCookie();
                formModel.resetLeadTrackingParameters();
            };
            model.hideLeadForm = function () {
                formModel.resetFormVisibility();

                var $anchor = $("a.contactMe[href]:contains('" + settings.Constants.contactMeCloseLabel + "')"); // pulled in from advisorlocator.cshtml
                var $agentCard = $anchor.parents('li');
                var marginTop = parseInt($agentCard.css('marginTop'));

                $anchor.text(settings.Constants.contactMeOpenLabel); // pulled in from advisorlocator.cshtml

                settings.Containers.leadForm.find(settings.Selectors.agentNumberInput).val('');
                settings.Containers.leadForm.hide();

                $(settings.Selectors.detailListItems).show();
                model.IsVisible = false;

                var topOfAgentCard = $agentCard.offset().top - marginTop;
                $(document).scrollTop(topOfAgentCard);
            };
            return model;
        };
        var createLeadForm = function (settings, clearMessage, formModel) {
            var model = new leadFormModel(settings, clearMessage, formModel);
            return model;
        };
        return {
            Create: createLeadForm
        };
    })();

})(jQuery, window, window.nmcom || (window.nmcom = {}));