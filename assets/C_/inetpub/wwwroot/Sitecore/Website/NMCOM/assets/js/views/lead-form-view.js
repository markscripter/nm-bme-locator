﻿;
(function ($, nmcom, undefined) {
    'use strict';
    nmcom.leadForm = nmcom.leadForm || {};

    nmcom.leadForm = (function () {
        var leadFormModel = function (settings) {
            var model = {};
            var me = {}; // for private members
            me.formId = settings.formId;
            me.View = $();
            me.whitepaperURL = settings.whitepaperURL;
            me.policyOwnerDisplay = settings.policyOwnerDisplay != undefined ? settings.policyOwnerDisplay : 'inline-block';
            me.leadDistribuionSettings = {
                leadUrlInternet: settings.leadUrlInternet,
                leadUrlFwsURL: settings.leadUrlFwsURL,
                leadUrlNewsLetterURL: settings.leadUrlNewsLetterURL,
                leadUrlEmail: settings.leadEmailURL,
                leadEmailRecipient: settings.leadEmailRecipient
            };

            me.settings = {
                LeadStatusURL: "/nmcomApi/LeadStatus",
                firstNameInput: "First Name",
                lastNameInput: "Last Name",
                addressInput: "address_1",
                cityInput: "city",
                stateInput: "state",
                zipInput: "zip",
                phoneInput: "Phone Number",
                emailInput: "EmailAddress",
                policyOwnerInput: "policyowner",
                agentNameInput: "AgentName",
                commentsInput: "text",
                distributionListInput: "fldDistLstNo",
                campSourceCodeInput: "campSrcCde",
                campNameInput: "campName",
                newsLetterCampSrcCde: "7",
                fwsCampSrcCde: "2",
                agentNumberInput: "AgentNumber",
                agentEmailParameter: "",
                fwsTypeParameter: "",
                isFwsParameter: "",
                isNewsLetterParameter: "",
                campNameParameter: "",
            };

            me.rules = settings.rules != undefined ? settings.rules : {
                'First Name': {
                    required: true
                },
                'Last Name': {
                    required: true
                },
                'address_1': {
                    required: true
                },
                'city': {
                    required: true
                },
                'state': {
                    required: true
                },
                'zip': {
                    required: true,
                    minlength: 5,
                    digits: true
                },
                'EmailAddress': {
                    email: true
                },
                'Phone Number': {
                    required: true,
                    phoneWithArea: true
                },
                'policyowner': {
                    required: true
                },
                'AgentName': {
                    required: {
                        depends: function () {
                            return me.View.find('input[name="policyowner"]:checked').val() === 'Yes';
                        }
                    }
                }
            };

            me.messages = settings.messages != undefined ? settings.messages : {
                'First Name': {
                    required: "Please enter your first name."
                },
                'Last Name': {
                    required: "Please enter your last name."
                },
                'address_1': {
                    required: "Please enter your street address."
                },
                'city': {
                    required: "Please enter your city."
                },
                'state': {
                    required: "Please select your state."
                },
                'zip': {
                    required: "Please enter your zip.",
                    minlength: "Zip code must be 5 digits.",
                    digits: "Zip code can only contain digits."
                },
                'EmailAddress': {
                    email: "Please enter a valid email address."
                },
                'Phone Number': {
                   required: "Please enter your phone number."
                },
                'policyowner': {
                    required: "Please indicate whether or not you are a current customer."
                },
                'AgentName': {
                    required: "Please enter your financial professional's name."
                }
            };

            model.initModel = function () {
                me.View = $('#' + me.formId);
                model.asw = new nmcom.asw.Init(me.View);
                model.setupPageEvents();
                model.setupValidation();
                model.processLeadTrackingCookie();
                model.detectTermCCampName();
                model.setLeadStatus();
            };

            model.processLeadTrackingCookie = function () {

                //prcoess lead and campaign cookies 
                nmcom.leadTracking.configLeadTrackingCookie();
                nmcom.leadTracking.configCampaignTrackingCookie();

                var leadTrackingCookie = nmcom.getCookie(nmcom.leadTracking.settings.leadTrackingCookieName);
                var campaignTrackingCookie = nmcom.getCookie(nmcom.leadTracking.settings.campaignTrackingCookieName);
                if (nmcom.leadTracking.isLeadTrackingCookieValid(leadTrackingCookie)) {
                    //hide 'Are you a current customer' and 'Agent Name' fields because we will use the cookie values
                    model.hideAgentNameAndCurrCustomterFields();
                    leadTrackingCookie = $.parseJSON(leadTrackingCookie);
                    me.settings.agentEmailParameter = leadTrackingCookie.email;
                    me.settings.fwsTypeParameter = leadTrackingCookie.fwsType;
                    me.settings.isFwsParameter = leadTrackingCookie.isFWS;
                    me.settings.isNewsLetterParameter = leadTrackingCookie.isNewsLetter;
                    me.View.find('form').find('input[name="' + me.settings.agentNumberInput + '"]').val(leadTrackingCookie.agentNum);
                }

                if (campaignTrackingCookie != null && $.parseJSON(campaignTrackingCookie).campaignName !== "") {
                    me.settings.campNameParameter = $.parseJSON(campaignTrackingCookie).campaignName;
                    //overwrite the campaign name in the form
                    me.View.find('form').find('input[name="' + me.settings.campNameInput + '"]').val(me.settings.campNameParameter);
                }
            };

            model.resetLeadTrackingParameters = function () {
                me.settings.agentEmailParameter = "";
                me.settings.fwsTypeParameter = "";
                me.settings.isFwsParameter = "";
                me.settings.isNewsLetterParameter = "";
                me.settings.campNameParameter = "";
            };

            /*REVISIT - remove this camp name hack*/
            model.detectTermCCampName = function () {
                var pageURL = window.location.href;
                if (pageURL.indexOf('term-conversion') > -1) {
                    me.View.find('input[name="campName"]').val("NM.COM Convert Page");
                }
            };

            model.setupPageEvents = function () {
                $(document).ajaxStart(function () {
                    me.View.find('#lead-form input[type="submit"]').prop('disabled', true);
                    me.View.closest('section.connect').find('.cta-btn').prop('disabled', true);
                }).ajaxStop(function () {
                    me.View.find('#lead-form input[type="submit"]').prop('disabled', false);
                    me.View.closest('section.connect').find('.cta-btn').prop('disabled', false);
                });
                me.View.find('input[name="' + me.settings.policyOwnerInput + '"]').click(model.toggleAgentName);
                me.View.find('#lead-form').submit(model.handleFormSubmission);
            };

            model.toggleAgentName = function () {
                if (me.View.find('input[name="' + me.settings.policyOwnerInput + '"]:checked').val() === "No") {
                    me.View.find('input[name="AgentName"]').parents('.form-item').hide();
                } else if (me.View.find('input[name="' + me.settings.policyOwnerInput + '"]:checked').val() === "Yes") {
                    me.View.find('input[name="AgentName"]').parents('.form-item').css('display', me.policyOwnerDisplay);
                }
            };

            model.hideAgentNameAndCurrCustomterFields = function () {
                me.View.find('input[name="' + me.settings.policyOwnerInput + '"]').parents('.form-group').hide();
            };

            model.hideAddressCityState = function () {
                me.View.find('input[name="' + me.settings.cityInput + '"]').parent().hide();
                me.View.find('select[name="' + me.settings.stateInput + '"]').parent().hide();
                me.View.find('input[name="' + me.settings.addressInput + '"]').parent().hide();
            },

            model.setupValidation = function () {
                nmcom.validation.setupValidate(me.View.find('#lead-form'),
                    {
                        rules: me.rules,
                        messages: me.messages,
                        invalidHandler: function () {
                            me.View.find('#loading').hide();
                            me.View.closest('section.connect').find('.cta-btn').removeClass('loading');
                            me.View.find('input[type="submit"]').removeClass('loading');
                        }
                    });
            },

            model.handleFormSubmission = function (e) {
                me.View.find('#loading').css('display', 'inline-block');
                me.View.closest('section.connect').find('.cta-btn').addClass('loading');
                me.View.find('input[type="submit"]').addClass('loading');

                model.resetFormVisibility();

                var $form = me.View.find(e.currentTarget);
                e.preventDefault();
                if (!$form.valid()) {
                    return;
                }

                var isModalForm = $form.parents('#modal-window').length > 0;
                var isLifeInsuranceSlideForm = $('.life-insurance .free-guide-form-popout.open').length > 0;
                var isLifeInsuranceInlineForm = $form.parents('.life-insurance #connect').length > 0;
                var actionUrl = me.leadDistribuionSettings.leadUrlInternet;
                var $dynamicForm = $form;
                var formData = $form.serialize();
                //remove value from Redirect_URL
                $('input[name="Redirect_URL"]').val("");

                if (model.isFWSLead()) {
                    formData = model.createFWSLeadObject($form);
                    actionUrl = me.leadDistribuionSettings.leadUrlFwsURL;
                    //FUIE - IE9 Ajax form submission
                    if (!nmcom.settings.isCorsSupported) {
                        $dynamicForm = me.View.find('#shadowFwsLeadForm');
                    }
                } else if (model.isNewsLetterLead()) {
                    formData = model.createNewsLetterLeadObject($form);
                    actionUrl = me.leadDistribuionSettings.leadUrlNewsLetterURL;
                    //FUIE - IE9 Ajax form submission
                    if (!nmcom.settings.isCorsSupported) {
                        $dynamicForm = me.View.find('#shadowNewsLetterLeadForm');
                    }
                }

                //if lead status is false, lead is down, send lead as an email 
                if ($('input[name="leadStatus"]').val() == 'false') {
                    actionUrl = me.leadDistribuionSettings.leadUrlEmail;
                    //set Redirect_URL
                    $('input[name="Redirect_URL"]').val("needs a value to submit successfully");

                    formData = $form.serialize();

                }

                $dynamicForm.attr('action', actionUrl);
                // detect browser support for CORS
                if (nmcom.settings.isCorsSupported) {
                    $.ajax(actionUrl, {
                        data: formData,
                        type: "POST",
                        success: model.formSubmitSuccess,
                        error: model.formSubmitError
                    });
                } else { // this is an old browser that doesn't support CORS, so do the post to hidden iframe trick
                    $dynamicForm.append('<iframe name="postResultsFrame" style="display: none;" />')

                    // Tell the form to post to the hidden iframe and remove the submit event handler so that a traditional form POST may occur submit the form
                    $dynamicForm.attr('target', 'postResultsFrame').unbind('submit').submit();

                    model.formSubmitSuccess();
                    me.View.find('#loading').hide();
                    me.View.closest('section.connect').find('.cta-btn').hide();
                    me.View.find('input[type="submit"]').hide();
                    // remove hidden form
                    //$dynamicForm.remove();
                }
                //track analytics
                if (isModalForm) {
                    nmcom.adobeDtmDirectCall("FrCardContactMeComplete");
                } else if (isLifeInsuranceInlineForm) {
                    nmcom.adobeDtmDirectCall("InLineFormLifeInsuranceComplete");
                } else if (isLifeInsuranceSlideForm) {
                    nmcom.adobeDtmDirectCall("SlideFormLifeInsuranceComplete");
                } else if (window.location.pathname.indexOf("financial-professional-locator") > -1) {
                    nmcom.adobeDtmDirectCall("FindAFinancialProfessionalContactMeFormCompete");
                } else if (window.location.pathname.indexOf("life-insurance/term-conversion") > -1) {
                    nmcom.adobeDtmDirectCall("TermConversionContactMeComplete");
                } else {
                    nmcom.adobeDtmDirectCall("ContactMeGeneralFormComplete");
                }

            };

            model.formSubmitSuccess = function () {
                me.View.find('#loading').hide();
                me.View.closest('section.connect').find('.cta-btn').hide();
                me.View.find('#lead-form-thankyou').show();
                me.View.find('#lead-form').hide();
                if (me.whitepaperURL != undefined && me.whitepaperURL != '') {
                    window.location.href = me.whitepaperURL; //start download automatically if variable is set
                }
            };

            model.formSubmitError = function (jqXhr, status, error) {
                me.View.find('#loading').hide();
                me.View.closest('section.connect').find('.cta-btn').hide();
                var pageUrl = window.location.href;
                var errorMsg = {
                    scriptName: 'lead-form-view.js',
                    message: 'Lead form failed to submit.  URL=' + pageUrl + ' STATUS=' + status + ' ERROR=' + error,
                    originUrl: pageUrl,
                    browser: window.navigator.userAgent,
                    lineNumber: 0,
                    level: 3
                };

                nmcom.errorHandler.logError(errorMsg);
                me.View.find('#lead-form-error').show();
                me.View.find('#lead-form input[type="submit"]').prop('disabled', false);
                me.View.closest('section.connect').find('.cta-btn').prop('disabled', false);
            };

            model.setLeadStatus = function () {
                var leadStatus = '';
                //$.getJSON(me.settings.LeadStatusURL, function (data) {
                //    leadStatus = data.Status;
                //    $('input[name="leadStatus"]').val(leadStatus);
                //});

            };
            model.resetFormVisibility = function () {
                me.View.find('#lead-form-thankyou, #lead-form-error').hide();
                me.View.find('#lead-form').show();
            };

            model.isNewsLetterLead = function () {
                if (me.settings.isNewsLetterParameter) {
                    return true;
                } else {
                    return false;
                }
            };

            model.isFWSLead = function () {
                if (me.settings.isFwsParameter) {
                    return true;
                } else {
                    return false;
                }
            };

            model.createNewsLetterLeadObject = function (form) {

                var lead = new Object();
                var leadPhoneNum = model.splitPhone($(form).find('input[name="' + me.settings.phoneInput + '"]').val());

                lead.fr_number = $(form).find('input[name="' + me.settings.agentNumberInput + '"]').val();
                lead.campaign_name = $(form).find('input[name="' + me.settings.campNameInput + '"]').val();
                lead.company = "";
                lead.source = me.settings.newsLetterCampSrcCde;
                lead.fr_name = "";
                lead.fr_email = me.settings.agentEmailParameter;
                lead.userName = "";
                lead.fr_designation = "";
                lead.phone_type = "";
                lead.first_name = $(form).find('input[name="' + me.settings.firstNameInput + '"]').val();
                lead.last_name = $(form).find('input[name="' + me.settings.lastNameInput + '"]').val();
                lead.address_1 = $(form).find('input[name="' + me.settings.addressInput + '"]').val();
                lead.city = $(form).find('input[name="' + me.settings.cityInput + '"]').val();
                lead.state = $(form).find('select[name="' + me.settings.stateInput + '"]').val();
                lead.zip = $(form).find('input[name="' + me.settings.zipInput + '"]').val();
                lead.phone_number = leadPhoneNum[0];
                lead.txtPhone1 = leadPhoneNum[1];
                lead.txtPhone2 = leadPhoneNum[2];
                lead.txtPhone3 = leadPhoneNum[3];
                lead.emailaddress = $(form).find('input[name="' + me.settings.emailInput + '"]').val();
                lead.comments = $(form).find('textarea[name="' + me.settings.commentsInput + '"]').val();
                lead.moreinfoabout = "";

                //FUIE - IE9 Ajax form submission
                if (!nmcom.settings.isCorsSupported) {
                    model.createShadowForm('shadowNewsLetterLeadForm', lead);
                }

                return lead;
            };

            model.createFWSLeadObject = function (form) {
                var lead = new Object();
                var leadPhoneNum = model.splitPhone($(form).find('input[name="' + me.settings.phoneInput + '"]').val());


                lead.a1_ReqFirstName = $(form).find('input[name="' + me.settings.firstNameInput + '"]').val();
                lead.a3_ReqLastName = $(form).find('input[name="' + me.settings.lastNameInput + '"]').val();
                lead.addressLine1 = $(form).find('input[name="' + me.settings.addressInput + '"]').val();
                lead.AgentName = "";
                lead.AgentNumber = $(form).find('input[name="' + me.settings.agentNumberInput + '"]').val();
                lead.b2_ReqAreaCde = leadPhoneNum[1];
                lead.b3_ReqPhone1 = leadPhoneNum[2];
                lead.b4_ReqPhone2 = leadPhoneNum[3];
                lead.b8_EmailAddress = $(form).find('input[name="' + me.settings.emailInput + '"]').val();
                lead.BCCRcpntEmailNam = "";
                lead.campName = $(form).find('input[name="' + me.settings.campNameInput + '"]').val();
                lead.campSrcCde = me.settings.fwsCampSrcCde;
                lead.city = $(form).find('input[name="' + me.settings.cityInput + '"]').val();
                lead.comments = $(form).find('textarea[name="' + me.settings.commentsInput + '"]').val();
                lead.contactReason = "";
                lead.EmailSubjectTxt = "Contact request from your internet site";
                lead.fldDistLstNo = "";
                lead.fwsType = me.settings.fwsTypeParameter.replace(/#.*$/, '');
                lead.Interestinfo = "";
                lead.InterestinfoText = "";
                lead.LegalEntityId = "";
                lead.Page = "FormLoggerHandler";
                lead.policyowner = $(form).find('input[name="' + me.settings.policyOwnerInput + '"]').val();
                lead.product_interest = "NMFN Lead";
                lead.RecipientEmailNam = me.settings.agentEmailParameter;
                lead.Redirect_URL = "";
                lead.src = "";
                lead.state = $(form).find('select[name="' + me.settings.stateInput + '"]').val();
                lead.temp_text = "";
                lead.text = "";
                lead.zip = $(form).find('input[name="' + me.settings.zipInput + '"]').val();
                lead.zipPlus = "";

                //FUIE - IE9 Ajax form submission
                if (!nmcom.settings.isCorsSupported) {
                    model.createShadowForm('shadowFwsLeadForm', lead);
                }

                return lead;

            };

            model.splitPhone = function (phoneNumberString) {
                var phonePattern = /^[^\d]*([\d]{3}).*([\d]{3}).*([\d]{4})/;

                return phonePattern.exec(phoneNumberString);
            };

            model.createShadowForm = function (formName, obj) {

                // to submit forms via CORS Ajax in IE9, create hidden form to submit via iframe
                var $shadowForm = me.View.find('<form>', {
                    id: formName,
                    name: formName,
                    method: 'post',
                    'class': 'is-hidden'
                });

                $.each(obj, function (key, val) {
                    var $ele = $('<input>', {
                        type: 'hidden',
                        name: key,
                        value: val
                    });
                    $shadowForm.append($ele);
                });

                $shadowForm.appendTo('body');

            };
            return model;
        };
        var init = function (settings) {
            var model = new leadFormModel(settings);
            model.initModel();
            return model;
        };
        return {
            Init: init
        };
    })();

})(window.jQuery, window.nmcom || (window.nmcom = {}));
