﻿;
(function ($, nmcom) {
    'use strict';
    nmcom.learningcenter = nmcom.learningcenter || {};

    $.extend(nmcom.learningcenter, {
        settings: {
            cookieName: "lc-gate-completed",
            cookieDurationDays: 60,

            firstNameInput: "First Name",
            lastNameInput: "Last Name",
            addressInput: "address_1",
            cityInput: "city",
            stateInput: "state",
            zipInput: "zip",
            phoneInput: "Phone Number",
            emailInput: "EmailAddress",
            policyOwnerInput: "policyowner",
            agentNameInput: "AgentName",
            commentsInput: "text",
            distributionListInput: "fldDistLstNo",
            campSourceCodeInput: "campSrcCde",
            campNameInput: "campName",
            newsLetterCampSrcCde: "7",
            fwsCampSrcCde: "2",
            agentNumberInput: "AgentNumber",

            agentEmailParameter: "",
            fwsTypeParameter: "",
            isFwsParameter: "",
            isNewsLetterParameter: "",
            campNameParameter: "",

            defaultCampaignName: "NM.COM Whitepaper"
        },

        init: function() {
            this.setupPageEvents();
            this.setupValidation();
            this.checkForCookie();
            this.checkForBypassGateParameter();
            this.processLeadTrackingCookie();
            this.setLeadCampaignName();
            var model = new nmcom.asw.Init($('#learning-center-gate-form'));
        },

        checkForBypassGateParameter: function() {
            var self = this;
            var bypassGateParam = nmcom.getQueryStringParam("noGate");

            if (bypassGateParam === "true") {
                self.hideGateAndShowDetails();
            }
        },

        checkForCookie: function() {
            var self = this;
            var cookieValue = nmcom.getCookie(self.settings.cookieName);
            if (cookieValue === "true") { // visitor has previously filled out a lead gate in the learning center
                self.hideGateAndShowDetails();
            }
        },

        setupPageEvents: function() {
            var self = this;

            $(document).ajaxStart(function() {
                $('#loading').css('display', 'inline-block');
                $('#learning-center-gate-form input[type="submit"]').prop('disabled', true);
            }).ajaxStop(function() {
                $('#loading').hide();
                $('#learning-center-gate-form input[type="submit"]').prop('disabled', false);
            });

            $('#learning-center-gate-form').submit(nmcom.learningcenter.handleLeadGateForm);
            $('input[name="policyowner"]').click(function() {
                self.toggleAgentNameField();

            });

        },

        toggleAgentNameField: function() {
            if ($('input[name="policyowner"]:checked').val() === "No") {
                $('input[name="AgentName"]').parents('.form-item').hide();
            } else if ($('input[name="policyowner"]:checked').val() === "Yes") {
                $('input[name="AgentName"]').parents('.form-item').css('display', 'inline-block');
            }

        },

        setupValidation: function() {
            nmcom.validation.setupValidate('#learning-center-gate-form', {
                rules: {
                    'First Name': {
                        required: true
                    },
                    'Last Name': {
                        required: true
                    },
                    'zip': {
                        required: true,
                        minlength: 5,
                        digits: true
                    },
                    'EmailAddress': {
                        email: true
                    },
                    'Phone Number': {
                        required: true,
                        phoneWithArea: true
                    },
                    'policyowner': {
                        required: true
                    },
                    'AgentName': {
                        required: {
                            depends: function() {
                                return $('input[name="policyowner"]:checked').val() === 'Yes';
                            }
                        }
                    }
                },
                messages: {
                    'First Name': {
                        required: "Please enter your first name."
                    },
                    'Last Name': {
                        required: "Please enter your last name."
                    },
                    'zip': {
                        required: "Please enter your zip.",
                        minlength: "Zip code must be 5 digits.",
                        digits: "Zip code can only contain digits."
                    },
                    'EmailAddress': {
                        email: "Please enter a valid email address."
                    },
                    'Phone Number': {
                        required: "Please enter your phone number."
                    },
                    'policyowner': {
                        required: "Please indicate whether or not you are a current customer."
                    },
                    'AgentName': {
                        required: "Please enter your financial professional's name."
                    }
                }
            });
        },

        handleLeadGateForm: function(e) {
            var self = nmcom.learningcenter;
            var $form = $(e.currentTarget);

            e.preventDefault();

            if (!$form.valid()) {
                return;
            }

            var actionUrl = nmcom.learningcenter.leadDistribuionSettings.leadUrlInternet;

            var $dynamicForm = $form;
            var formData = $form.serialize();

            if (self.isFWSLead()) {
                formData = self.createFWSLeadObject($form);
                actionUrl = nmcom.learningcenter.leadDistribuionSettings.leadUrlFwsURL;
                //FUIE - IE9 Ajax form submission
                if (!nmcom.settings.isCorsSupported) {
                    $dynamicForm = $('#shadowFwsLeadForm');
                }
            } else if (self.isNewsLetterLead()) {
                formData = self.createNewsLetterLeadObject($form);
                actionUrl = nmcom.learningcenter.leadDistribuionSettings.leadUrlNewsLetterURL;
                //FUIE - IE9 Ajax form submission
                if (!nmcom.settings.isCorsSupported) {
                    $dynamicForm = $('#shadowNewsLetterLeadForm');
                }

            }

            $dynamicForm.attr('action', actionUrl);

            // detect browser support for CORS
            if (nmcom.settings.isCorsSupported) {

                $.ajax(actionUrl, {
                    data: formData,
                    type: 'POST',
                    success: self.leadGateFormSubmitSuccess,
                    error: self.leadGateFormSubmitError
                });

            } else { // this is an old browser that doesn't support CORS, so do the post to hidden iframe trick

                $('iframe').attr({
                    'name': 'postResultsFrame'
                }).appendTo($dynamicForm).hide();

                // Tell the form to post to the hidden iframe and remove the submit event handler so that a traditional form POST may occur submit the form
                $dynamicForm.attr('target', 'postResultsFrame').unbind('submit').submit();

                self.leadGateFormSubmitSuccess();

                // remove hidden form
                //$dynamicForm.remove();

            }

            //track analytics
            if (window.location.pathname.indexOf("learning-center/whitepapers") > -1) {
                nmcom.adobeDtmDirectCall("LearningCenterWhitepaperFormComplete");
            }

        },

        processLeadTrackingCookie: function() {

            var leadTrackingCookie = nmcom.getCookie(nmcom.leadTracking.settings.leadTrackingCookieName);

            if (nmcom.leadTracking.isLeadTrackingCookieValid(leadTrackingCookie)) {

                //hide 'Are you a current customer' and 'Agent Name' fields because we will use the cookie values
                this.hideAgentNameAndCurrCustomterFields();

                leadTrackingCookie = $.parseJSON(leadTrackingCookie);
                this.settings.agentEmailParameter = leadTrackingCookie.email;
                this.settings.fwsTypeParameter = leadTrackingCookie.fwsType;
                this.settings.isFwsParameter = leadTrackingCookie.isFWS;
                this.settings.isNewsLetterParameter = leadTrackingCookie.isNewsLetter;

                $('form').find('input[name="' + this.settings.agentNumberInput + '"]').val(leadTrackingCookie.agentNum);

            }

        },

        setLeadCampaignName: function() {

            var campName = $('input[name="leadCampName"]').val();

            if (campName == null || campName === "") {
                campName = this.settings.defaultCampaignName;
            }

            $('form').find('input[name="' + nmcom.learningcenter.settings.campNameInput + '"]').val(campName);

        },

        hideAgentNameAndCurrCustomterFields: function() {

            $('input[name="' + this.settings.policyOwnerInput + '"]').parents('.form-group').hide();
        },

        leadGateFormSubmitSuccess: function() {
            var self = nmcom.learningcenter;
            nmcom.setCookie(self.settings.cookieName, "true", self.settings.cookieDurationDays);
            self.triggerGoal(); //Trigger the whitepaper goal
            self.hideGateAndShowDetails();
            if (nmcom.learningcenter.whitepaperURL !== null && nmcom.learningcenter.whitepaperURL !== "") {
                window.location.href = nmcom.learningcenter.whitepaperURL; //start download automatically if variable is set
            }
        },

        leadGateFormSubmitError: function(jqXhr, status, error) {
            var pageUrl = window.location.href;
            var errorMsg = {
                scriptName: 'learning-center-detail-view.js',
                message: 'Learning Center Lead Gate Form failed to submit.  URL=' + pageUrl + ' STATUS=' + status + ' ERROR=' + error,
                originUrl: pageUrl,
                browser: window.navigator.userAgent,
                lineNumber: 0,
                level: 3
            };

            nmcom.errorHandler.logError(errorMsg);
            console.log(errorMsg);
            $('#lead-form-gate-error').show();
            $('#learning-center-gate-form input[type="submit"]').prop('disabled', false);
        },

        hideGateAndShowDetails: function() {
            $('#learning-center-detail').show();
            $('#learning-center-gate').hide();
            $('html, body').scrollTop($('#learning-center-detail').offset().top);
        },

        isNewsLetterLead: function() {
            if (this.settings.isNewsLetterParameter) {
                return true;
            } else {
                return false;
            }

        },

        isFWSLead: function() {
            if (this.settings.isFwsParameter) {
                return true;
            } else {
                return false;
            }
        },

        createNewsLetterLeadObject: function(form) {

            var lead = new Object();
            var leadPhoneNum = this.splitPhone($(form).find('input[name="' + this.settings.phoneInput + '"]').val());

            lead.fr_number = $(form).find('input[name="' + this.settings.agentNumberInput + '"]').val();
            lead.campaign_name = $(form).find('input[name="' + this.settings.campNameInput + '"]').val();
            lead.company = "";
            lead.source = this.settings.newsLetterCampSrcCde;
            lead.fr_name = "";
            lead.fr_email = this.settings.agentEmailParameter;
            lead.userName = "";
            lead.fr_designation = "";
            lead.phone_type = "";
            lead.first_name = $(form).find('input[name="' + this.settings.firstNameInput + '"]').val();
            lead.last_name = $(form).find('input[name="' + this.settings.lastNameInput + '"]').val();
            lead.address_1 = $(form).find('input[name="' + this.settings.addressInput + '"]').val();
            lead.city = $(form).find('input[name="' + this.settings.cityInput + '"]').val();
            lead.state = $(form).find('select[name="' + this.settings.stateInput + '"]').val();
            lead.zip = $(form).find('input[name="' + this.settings.zipInput + '"]').val();
            lead.phone_number = leadPhoneNum[0];
            lead.txtPhone1 = leadPhoneNum[1];
            lead.txtPhone2 = leadPhoneNum[2];
            lead.txtPhone3 = leadPhoneNum[3];
            lead.emailaddress = $(form).find('input[name="' + this.settings.emailInput + '"]').val();
            lead.comments = $(form).find('textarea[name="' + this.settings.commentsInput + '"]').val();
            lead.moreinfoabout = "";

            //FUIE - IE9 Ajax form submission
            if (!nmcom.settings.isCorsSupported) {
                this.createShadowForm('shadowNewsLetterLeadForm', lead);
            }

            return lead;
        },

        createFWSLeadObject: function(form) {
            var lead = new Object();
            var leadPhoneNum = this.splitPhone($(form).find('input[name="' + this.settings.phoneInput + '"]').val());


            lead.a1_ReqFirstName = $(form).find('input[name="' + this.settings.firstNameInput + '"]').val();
            lead.a3_ReqLastName = $(form).find('input[name="' + this.settings.lastNameInput + '"]').val();
            lead.addressLine1 = $(form).find('input[name="' + this.settings.addressInput + '"]').val();
            lead.AgentName = "";
            lead.AgentNumber = $(form).find('input[name="' + this.settings.agentNumberInput + '"]').val();
            lead.b2_ReqAreaCde = leadPhoneNum[1];
            lead.b3_ReqPhone1 = leadPhoneNum[2];
            lead.b4_ReqPhone2 = leadPhoneNum[3];
            lead.b8_EmailAddress = $(form).find('input[name="' + this.settings.emailInput + '"]').val();
            lead.BCCRcpntEmailNam = "";
            lead.campName = $(form).find('input[name="' + this.settings.campNameInput + '"]').val();
            lead.campSrcCde = this.settings.fwsCampSrcCde;
            lead.city = $(form).find('input[name="' + this.settings.cityInput + '"]').val();
            lead.comments = $(form).find('textarea[name="' + this.settings.commentsInput + '"]').val();
            lead.contactReason = "";
            lead.EmailSubjectTxt = "Contact request from your internet site";
            lead.fldDistLstNo = "";
            lead.fwsType = this.settings.fwsTypeParameter.replace(/#.*$/, '');
            lead.Interestinfo = "";
            lead.InterestinfoText = "";
            lead.LegalEntityId = "";
            lead.Page = "FormLoggerHandler";
            lead.policyowner = $(form).find('input[name="' + this.settings.policyOwnerInput + '"]').val();
            lead.product_interest = "NMFN Lead";
            lead.RecipientEmailNam = this.settings.agentEmailParameter;
            lead.Redirect_URL = "";
            lead.src = "";
            lead.state = $(form).find('select[name="' + this.settings.stateInput + '"]').val();
            lead.temp_text = "";
            lead.text = "";
            lead.zip = $(form).find('input[name="' + this.settings.zipInput + '"]').val();
            lead.zipPlus = "";

            //FUIE - IE9 Ajax form submission
            if (!nmcom.settings.isCorsSupported) {
                this.createShadowForm('shadowFwsLeadForm', lead);
            }

            return lead;

        },

        splitPhone: function(phoneNumberString) {
            var phonePattern = /^[^\d]*([\d]{3}).*([\d]{3}).*([\d]{4})/;

            return phonePattern.exec(phoneNumberString);
        },

        createShadowForm: function(formName, obj) {

            // to submit forms via CORS Ajax in IE9, create hidden form to submit via iframe
            var $shadowForm = $('<form>', {
                id: formName,
                name: formName,
                method: 'post',
                'class': 'is-hidden'
            });

            $.each(obj, function(key, val) {
                var $ele = $('<input>', {
                    type: 'hidden',
                    name: key,
                    value: val
                });
                $shadowForm.append($ele);
            });

            $shadowForm.appendTo('body');

        },

        triggerGoal: function () {
            if (nmcom.learningcenter.goalIds !== undefined && nmcom.learningcenter.goalIds !== null && nmcom.learningcenter.goalIds.length) {
                nmcom.Services.Http.POST(
                    '/nmcomMvc/Components/SitecoreGoalTrigger',
                    {},
                    JSON.stringify({ goalArray: nmcom.learningcenter.goalIds }));
            }
        }
    });

})(window.jQuery, window.nmcom || (window.nmcom = {}));

jQuery(function() {
    nmcom.learningcenter.init();
});