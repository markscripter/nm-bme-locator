﻿;
(function ($, nmcom) {
    'use strict';
    nmcom.frdetail = nmcom.frdetail || {};

    $.extend(nmcom.frdetail, {
        init: function () {
            var agentNum = nmcom.getCookie("frDetailCard");
            if (agentNum != null && agentNum !== "") {
                this.getFRDetails(agentNum, this.setFRDetails);
            }
        },

        getFRDetails: function (agentNum, successCallback) {
            //Get Results
            $.ajax({
                type: "GET",
                dataType: "json",
                data: { "agentNum": agentNum, "type": "FR" },
                url: "/nmcomApi/Entity",
                success: function(results) {
                    successCallback(results);
                    },
                timeout: 8000 // Set timeout to 8 seconds
            });
        },

        setFRDetails: function (frDetails) {
            if (frDetails != null && frDetails.length === 1) {
                $('#fr-detail-image').attr({ "src": frDetails[0].Photo, "alt": "Financial Professional Photo"});
                $('#fr-detail-name').html(frDetails[0].AgentName.FirstName + " " + frDetails[0].AgentName.LastName);
                $('#fr-detail-phone').html("(" + frDetails[0].Phone[0].AreaCode + ")" + "-" + frDetails[0].Phone[0].ControlNum + "-" + frDetails[0].Phone[0].UniqueNum);
                $("#fr-detail-website").html(frDetails[0].URL);
            }
        }

    });
})(window.jQuery, window.nmcom || (window.nmcom = {}));

jQuery(function () {
    nmcom.frdetail.init();
});
