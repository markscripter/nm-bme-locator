﻿;
(function ($, nmcom) {
    'use strict';
    nmcom.learningcenter = nmcom.learningcenter || {};
    nmcom.learningcenter.category = nmcom.learningcenter.category || {};

    $.extend(nmcom.learningcenter.category, {
        init: function() {

            var self = this;

            this.returnToPage();

            $('.learning-center-category .filter-list .filter-item').on('click', function(e) {
                self.filterItems.call(self, e);
            });

            $('#clear-filters').on('click', function(e) {
                self.clearFilters.call(self, e);
            });
        },

        returnToPage: function() {
            // do an initial filter, to preserve existing checks when coming back to this page
            $('.filter-list .filter-item input:checked').each(function() {
                $(this).parent().addClass('alt');
            });
            this.toggleItems();
        },

        clearFilters: function(e) {
            e.preventDefault();
            $('.filter-list .filter-item').removeClass('alt');
            $('input[type="checkbox"]').prop('checked', false);
            this.toggleItems();
        },

        toggleItems: function() {

            $('.learning-center-item-wrapper').removeClass('learning-center-item-show');

            if ($('input:checked').length > 0) {
                $('input:checked').each(function() {
                    //replace label text white space with "-" 
                    var topicClassName = $(this).attr('id');

                    //find all learning center items with matching topic class
                    var learningCenterItems = $("." + topicClassName);
                    learningCenterItems.addClass("learning-center-item-show");
                });
            } else {
                $('.learning-center-item-wrapper').addClass("learning-center-item-show");
            }

            $('.learning-center-item-show').show();
            $('.learning-center-item-wrapper:not(".learning-center-item-show")').hide();

            this.applyRowStriping();

        },

        filterItems: function(e) {

            e.preventDefault();

            $(e.currentTarget).toggleClass('alt');

            if (!$(e.currentTarget).is('input')) {
                var checkbox = $(e.currentTarget).find('input');

                if (checkbox.is(':checked')) {
                    checkbox.prop('checked', false);
                } else {
                    checkbox.prop('checked', true);
                }
            }

            this.toggleItems();

        },

        applyRowStriping: function() {
            $('.section-list li').removeClass('even-row').removeClass('odd-row');
            $('.section-list li:visible:even').addClass('odd-row'); //:even is zero based so it matches all odd rows
            $('.section-list li:visible:odd').addClass('even-row'); //:odd is zero based so it matches all even rows
        }
    });

})(window.jQuery, window.nmcom || (window.nmcom = {}));

jQuery(function() {
    nmcom.learningcenter.category.init();
});