/// <reference path="../lib/jasmine-2.0.0/jasmine.js" />
/// <reference path="../lib/jasmine-2.0.0/boot.js" />
/// <reference path="../lib/sinon-1.9.0.js" />
/// <reference path="../../vendor/jquery-2.1.1.min.js" />
/// <reference path="../lib/jasmine-jquery-2.1.0.js" />
/// <reference path="../lib/Jasmine2-teamcityreporter.js" />
/// <reference path="../../vendor/underscore-1.7.0.min.js" />
/// <reference path="../../vendor/slick-1.3.11.min.js" />
/// <reference path="../../lead-tracking.js" />
/// <reference path="../../vendor/modernizr-2.8.3.min.js" />
/// <reference path="../../error-handler.js" />
/// <reference path="../../nmcom.js" />
/// <reference path="../../nmcom.js" />

describe('nmcom.js', function () {

    beforeEach(function() {
        //loadFixtures('myfixture.html');
    });

    afterEach(function() {
        
    });

    it('should expose an nmcom object to the global namespace', function() {

        expect(nmcom).toBeDefined();
    });

    it('should have an init function', function() {

        expect(nmcom.init).toBeDefined();

        expect(typeof nmcom.init).toBe('function');
    });

    it('should have a setup events function', function() {

        expect(nmcom.setupEvents).toBeDefined();

        expect(typeof nmcom.setupEvents).toEqual('function');
    });

    describe('Override default error handler', function() {

        it('should have a function to handle errors', function() {

            expect(nmcom.errorHandler.windowErrorHandler).toBeDefined();

            expect(typeof nmcom.errorHandler.windowErrorHandler).toEqual('function');
        });

        it('should set the default error handler', function() {

            nmcom.init();

            expect(window.onerror).toBe(nmcom.errorHandler.windowErrorHandler);
        });

        it('should send the error to the web service', function() {

            var xhr = sinon.useFakeXMLHttpRequest(),
                requests = [];

            xhr.onCreate = function(req) { requests.push(req); };

            nmcom.errorHandler.windowErrorHandler('test', 'www.nm.com', 200);

            expect(requests.length).toEqual(1);

            expect(requests[0].url).toEqual('/nmcomMvc/JavaScriptLogging/Log');

            var requestBody = JSON.parse(requests[0].requestBody);

            expect(requestBody.scriptName).toMatch('www.nm.com');

            expect(requestBody.message).toMatch('test');

            expect(requestBody.lineNumber).toMatch('200');

            xhr.restore();

        });
    });


});