﻿;
(function ($, nmcom) {
    'use strict';
    nmcom.officeLocator = nmcom.officeLocator || {};
    nmcom.recruiting = nmcom.recruiting || {};

    $.extend(nmcom.officeLocator, {
        settings: {
            defaultSearchDistance: 35, //miles
            defaultZoomLevel: 3, //USA
            maxAutoZoomLevel: 12, //stop map bounding from zooming in too far for only one result
            entityServiceUrl: '/nmcomApi/entity?type=NO,DNO,Detached',
            numPerPage: 10,
            pinIconStart: '/NMCOM/assets/img/views/office-locator/pindrop-',
            pinIconEnd: '.png',
            noPhotoImage: '/NMCOM/assets/img/views/advisor-locator/no-photo.jpg',
            doProgressiveScrolling: true,
            animationSpeed: 500,
            alpha: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
			
            //the following are used for bounding (google.maps.LatLngBounds) the visible map
            minLat: 90, //north pole
            maxLat: 0, //equator
            minLng: 0, //prime meridian
            maxLng: -180 //international date line
        },

        mapObjects: {
            //will be instantiated in googleMapsLoaded() once maps API is available
            centerOfUsa: null,
            mapInstance: null,
            geocoderInstance: null,
            markers: []
        },

        searchParameters: {
            distance: 0,
            address: '',
            lat: 0,
            lng: 0,
            lastName: ''
        },

        mapDragTimeout: null,

        init: function() {

            var geolocationSupport = ('geolocation' in navigator);			

			this.loadGoogleMaps();
			
			// If geolocation is not supported use MaxMind and hide find my location button
			if (!geolocationSupport) {
				this.maxMindSearch();
				$('#geolocation-search').hide();
			}
			
            this.setupPageEvents();

            // add placeholders for fields without labels in older browsers (IE9)
            if (!Modernizr.input.placeholder) {
                nmcom.setupLegacyPlaceholders($('#location'));
            }

        },

        setupPageEvents: function() {

            var self = this;

            $(document).ajaxStart(function() {
                $('#loading').show();
            }).ajaxStop(function() {
                $('#loading').hide();
            });

            //wire up search
            $('.map-options input[type="submit"]').click(function (e) {
                e.preventDefault();
                self.search();
            });

            $('#geolocation-search').click( function (e) {
				e.preventDefault();
				self.gpsSearch();
			});

            $('.map-options').keydown(function(e) {
                if (e.keyCode === 13) { // enter key
                    e.preventDefault();
                    self.search();
                }
            });

            $(window).scroll(function() {
                /*
                 * Disable mag dragging and zooming when the window is scrolling
                 */
                if (self.mapDragTimeout == null && self.mapObjects.mapInstance != null) {
                    self.mapObjects.mapInstance.setOptions({ draggable: false, scrollwheel: false });
                    self.mapDragTimeout = setTimeout(function() {
                        self.mapObjects.mapInstance.setOptions({ draggable: true, scrollwheel: true });
                        self.mapDragTimeout = null;
                    }, 2000);
                }
            });

        },

        /*
         FUNCTIONS FOR MAPPING
         */
        loadGoogleMaps: function() { //inject Google Maps script
            if (typeof google === 'object' && typeof google.maps === 'object') {
                this.googleMapsLoaded();
            } else {
                var mapsUrl = "https://maps.google.com/maps/api/js?v=3&client=gme-northwesternmutual&sensor=false&callback=nmcom.officeLocator.googleMapsLoaded";
                nmcom.Services.Http.SCRIPT(mapsUrl);
            }
        },

        googleMapsLoaded: function() { //Google Maps has loaded, go forth and do stuff
            this.mapObjects.centerOfUsa = new google.maps.LatLng(38, -98);
			
            var mapOptions = {
                zoom: this.settings.defaultZoomLevel,
                center: this.mapObjects.centerOfUsa,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scaleControl: true,
                scaleControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                streetViewControl: false,
                panControl: false
            };

            var mapDiv = document.getElementById('map-holder');

            this.mapObjects.mapInstance = new google.maps.Map(mapDiv, mapOptions);

            this.mapObjects.geocoderInstance = new google.maps.Geocoder();

            // Try geolocation before trying MaxMind
            this.gpsSearch();
        },

        mapEntities: function(entityArray) {

            var self = this;

            this.removeMarkers();
            this.resetMapBounds();

            $.each(entityArray, function(i, entity) {
                var address = entity.Address;
                var lat = parseFloat(address.Latitude);
                var lng = parseFloat(address.Longitude);

                //keep track of the northern, eastern, southern and western-most locations, this will be used to bound the map
                if (lat > self.settings.maxLat) self.settings.maxLat = lat;
                if (lat < self.settings.minLat) self.settings.minLat = lat;
                if (lng > self.settings.maxLng) self.settings.maxLng = lng;
                if (lng < self.settings.minLng) self.settings.minLng = lng;

                var pinLatLng = new google.maps.LatLng(lat, lng);

                var pinLetter = (i > 25) ? self.settings.alpha[25] : self.settings.alpha[i]; //don't let pin letters go above Z
                var pinIconUrl = self.settings.pinIconStart + pinLetter + self.settings.pinIconEnd;

                var markerOptions = {
                    position: pinLatLng,
                    icon: pinIconUrl
                };

                var entityMarker = new google.maps.Marker(markerOptions);
                entityMarker.setMap(self.mapObjects.mapInstance);
                self.mapObjects.markers.push(entityMarker);

                google.maps.event.addListener(entityMarker, 'click', function() {
                    self.focusMapOnAddress(this.getPosition());
                    self.focusOnOfficeAtIndex(i);
                });
            });
            this.setMapBounds();
        },

        focusMapOnAddress: function(latLngPoint) {
            this.mapObjects.mapInstance.setCenter(latLngPoint);
            if (this.mapObjects.mapInstance.getZoom() < this.settings.maxAutoZoomLevel) {
                this.mapObjects.mapInstance.setZoom(this.settings.maxAutoZoomLevel);
            }
        },

        removeMarkers: function() {
            $.each(this.mapObjects.markers, function(i, marker) {
                marker.setMap(null);
            });
            this.mapObjects.markers = [];
        },

        setMapBounds: function() {
            var swPoint = new google.maps.LatLng(this.settings.minLat, this.settings.minLng);
            var nePoint = new google.maps.LatLng(this.settings.maxLat, this.settings.maxLng);
            var resultsBounds = new google.maps.LatLngBounds(swPoint, nePoint);
            this.mapObjects.mapInstance.fitBounds(resultsBounds);
            if (this.mapObjects.mapInstance.getZoom() > this.settings.maxAutoZoomLevel) {
                this.mapObjects.mapInstance.setZoom(this.settings.maxAutoZoomLevel);
            }
        },

        resetMapBounds: function() {
            this.settings.minLat = 90, //north pole
                this.settings.maxLat = 0, //equator
                this.settings.minLng = 0, //prime meridian
                this.settings.maxLng = -180; //international date line
        },

        /*
         FUNCTIONS FOR RESULTS
         */
        showMessage: function(msg) {
            $('#office-locator-section h2').html(msg);
        },
        showError: function(msg) {
            $('#office-locator-section h2').html(msg).addClass('error');
        },
        clearMessage: function() {
            $('#office-locator-section h2').html('').removeClass('error');
        },
        showSearchResultsMessage: function(numResults) {
            var params = this.searchParameters;
            var msg;
            var result = (numResults > 1) ? " Results" : " Result";
            if (params.address.length > 0) {
                msg = numResults + result + ' for: <span>' + params.address + '</span>';
            } else {
                msg = numResults + result;
            }
            this.showMessage(msg);
        },
		showYourLocationMessage: function () {
			var params = this.searchParameters;
			var msg = '';
			var address = params.address.split(" ");
			if (params.address.length > 0) {
				msg = '<span>Your Location: </span><span class="location">' +  address[0] + " " + address[1] + '</span>';
			}
			this.showYourLocation(msg);
		},
		showYourLocation: function (msg) {
			$('.map-options .details').html(msg);
		},
		
        clearYourLocation: function () {
            $('.map-options .details').html("");
        },

        focusOnOfficeAtIndex: function(i) {
            var self = nmcom.officeLocator;
            $('.office-list > ul > li.active').removeClass('active'); //unhighlight any existing highlighted office
            var $officeCard = $('.office-list > ul > li').eq(i);
            $officeCard.addClass('active');

            $('html, body').animate({
                scrollTop: $officeCard.offset().top
            }, self.settings.animationSpeed);
        },

        buildAgentCards: function(agentEntities) {

            var formattedAgents = this.formatAgents(agentEntities);

            return this.bindAgentCards(formattedAgents);

        },

        clearAgentCards: function() {
            $('.office-list > ul').children().remove();
        },

        bindAgentCards: function(agents) {
            var agentTemplate = _.template($('#office-template').html());
            var agentHtml = agentTemplate({ agents: agents });

            //convert string to jQuery object and remove text nodes
            //TODO why is underscore inserting text nodes?
            var $agentCardsWrapped = $(agentHtml).filter(function() {
                return this.nodeType === Node.ELEMENT_NODE; //1
            });

            return $agentCardsWrapped;
        },

        showAgentCards: function($agentCards) {
            var $list = $('.office-list > ul');
            $list.append($agentCards);
        },

        formatAgents: function(sortedAgents) {

            var self = this;

            var formattedAgents = [];

            $.each(sortedAgents, function(i, entity) {

                var agent = {
                    displayName: self.formatAgentName(entity),
                    displayTitle: self.formatAgentTitle(entity),
                    displayAddress: self.formatAgentAddress(entity),
                    directionsLink: self.formatDirectionsLink(entity),
                    displayPhone: self.formatAgentPhoneNumbers(entity),
                    socialNetworks: (typeof entity.SocialNetwork[0] == "undefined") ? "" : entity.SocialNetwork[0].Sites,
                    displayBio: entity.BriefBio != null ? entity.BriefBio : "",
                    displayUrl: self.formatAgentWebsite(entity),
                    displayPhoto: self.formatAgentPhoto(entity),
                    dnoNum: entity.DnoNum != null ? entity.DnoNum : "",
                    noNum: entity.NoNum != null ? entity.NoNum : "",
                    pinLetter: (i > 25) ? self.settings.alpha[25] : self.settings.alpha[i] //don't let pin letters go above Z
                };


                formattedAgents.push(agent);

            });

            return formattedAgents;

        },

        formatAgentPhoto: function(entity) {
            var photoUrl;
            if (entity.Type === "Detached") {
                photoUrl = entity.ManagingPartnerPhotoUrl != null ? entity.ManagingPartnerPhotoUrl : "";
            } else {
                photoUrl = entity.Photo != null ? entity.Photo : "";
            }


            return photoUrl;
        },

        formatAgentName: function(entity) {

            //if is detached office, use the Managing Partner Name
            var name = (entity.Type === "Detached") ? entity.ManagingPartnerName : entity.AgentName;

            var displayName = "";

            if (name != null) {
                displayName = name.FirstName;


                if (name.MiddleName != null && name.MiddleName.length > 0) {
                    displayName += ' ' + name.MiddleName;
                }

                displayName += ' ' + name.LastName;

                if (name.Lineage != null && name.Lineage.length > 1) {
                    displayName += ' ' + name.Lineage;
                }
            }

            return displayName;

        },

        formatAgentAddress: function(entity) {

            var addr = entity.Address;
            var displayAddress = addr.Street + ' ' + addr.Building + '<br>' + addr.City + ', ' + addr.State + ' ' + addr.Zip;

            if (addr.ZipLastFour != null && addr.ZipLastFour.length > 0) {
                displayAddress += '-' + addr.ZipLastFour;
            }

            return displayAddress;
        },

        formatDirectionsLink: function(entity) {
            //build link to Google Maps  http://mapki.com/wiki/Google_Map_Parameters
            var myLat = parseFloat(entity.Address.Latitude);
            var myLng = parseFloat(entity.Address.Longitude);
            var street = entity.Address.Street;
            var building = entity.Address.Building;
            var city = entity.Address.City;
            var state = entity.Address.State;
            var zip = entity.Address.Zip;

            var googleAddress = "";
            if (street != null && street.length > 0) googleAddress += street;
            if (building != null && building.length > 0) googleAddress += ' ' + building;
            if (city != null && city.length > 0) googleAddress += ' ' + city;
            if (state != null && state.length > 0) googleAddress += ' ' + state;
            if (zip != null && zip.length > 0) googleAddress += ' ' + zip;

            var mapsHref;
            //check if device type is iPhone or iPad 6.0 or higher --> then display native apple maps otherwise display google diretions
            if (/iphone|ipod|ipad[^)]*OS (\d)/i.test(navigator.userAgent)) {
                if (/(iphone|ipod|ipad)[^)]*OS (\d)/i.exec(navigator.userAgent)[2] >= 6) {
                    //use apple maps
                    mapsHref = "http://maps.apple.com?ll=" + myLat + "," + myLng + "&daddr=" + encodeURIComponent(googleAddress);
                } else {
                    //IOS less than 6 use google maps
                    mapsHref = "http://maps.google.com?ll=" + myLat + "," + myLng + "&daddr=" + encodeURIComponent(googleAddress);
                }
            } else {
                //use google maps
                mapsHref = "http://maps.google.com?ll=" + myLat + "," + myLng + "&daddr=" + encodeURIComponent(googleAddress);
            }

            return mapsHref;
        },

        formatAgentPhoneNumbers: function(entity) {

            var self = this;

            var phone = (entity.Type === "Detached") ? entity.ManagingPartnerPhone : entity.Phone;

            var displayPhone = '';

            if ($.isArray(phone)) { //multiple phone numbers in an array
                $.each(phone, function(i, p) {
                    displayPhone += self.formatPhoneNumber(p) + '<br>';
                });
            } else { //a single phone number as an object
                if (phone != null) {
                    displayPhone = this.formatPhoneNumber(phone);
                }
            }

            return displayPhone;
        },

        formatPhoneNumber: function(phone) {
            var formattedPhone = phone.AreaCode + '-' + phone.ControlNum + '-' + phone.UniqueNum;
            if (phone.Extension !== -null && phone.Extension.length > 0) formattedPhone += ' x' + phone.Extension;
            if (phone.Type.slice(-1).toLowerCase() === 'f') formattedPhone = 'FAX ' + formattedPhone;
            return formattedPhone;
        },

        formatAgentWebsite: function(entity) {

            var url;

            if (entity.Type === "Detached") {
                url = entity.RecruitingOfficeURL != null ? entity.RecruitingOfficeURL : "";
            } else {
                url = entity.URL != null ? entity.URL : "";
            }


            var displayUrl = url;

            if (url.indexOf('http://') === -1) { //TODO: should this also check for SSL (https)?
                displayUrl = 'http://' + url;
            }

            return displayUrl;
        },

        formatAgentTitle: function(entity) {

            var displayTitle = entity.Title;

            if (entity.Type === 'MP' || entity.Type === 'NO') {
                displayTitle = 'Managing Partner';
            } else if (entity.Type === 'MD' || entity.Type === 'DNO') {
                displayTitle = 'Managing Director';
            }

            return displayTitle;

        },

        /*
         FUNCTIONS FOR SEARCH
         */

        maxMindSearch: function () {
            this.clearMessage();
            this.clearYourLocation();
			
            var urlParams = window.location.search;
            var addressPattern = /address=([^&]+)/i;
            var latlngPattern = /latlng=([0-9-.]+),([0-9-.]+)/i;
            var distancePattern = /distance=([0-9]+)/i;
            var lastNamePattern = /lastname=([^&]+)/i;

            var queryStringSearch = false;
            if (addressPattern.test(urlParams)) {
                this.searchParameters.address = decodeURIComponent(addressPattern.exec(urlParams)[1]);
                queryStringSearch = true;
            }
            if (latlngPattern.test(urlParams)) {
                this.searchParameters.lat = latlngPattern.exec(urlParams)[1];
                this.searchParameters.lng = latlngPattern.exec(urlParams)[2];
                queryStringSearch = true;
            }
            if (distancePattern.test(urlParams)) {
                this.searchParameters.distance = distancePattern.exec(urlParams)[1];
                queryStringSearch = true;
            }
            if (lastNamePattern.test(urlParams)) {
                this.searchParameters.lastName = decodeURIComponent(lastNamePattern.exec(urlParams)[1]);
                queryStringSearch = true;
            }

            if (queryStringSearch) {
                this.doSearch();
            } else { //do user location search
                var userLoc = this.userIpLocation;
                if (userLoc.lat.length > 0 && userLoc.lng.length > 0) {
                    this.searchParameters.lat = userLoc.lat;
                    this.searchParameters.lng = userLoc.lng;

                    var address = '';
                    if (userLoc.city.length > 0) {
                        address = userLoc.city + ', ';
                    }
                    if (userLoc.state.length > 0) {
                        address += userLoc.state + ' ';
                    }
                    if (userLoc.zip.length > 0) {
                        address += userLoc.zip;
                    }
                    this.searchParameters.address = address;
		    this.showYourLocationMessage();
                    this.doSearch();
                }
            }

        },

        entityResultsReturned: function(entityJson) {
		    this.clearAgentCards();
			
            if (entityJson != null && entityJson.length > 0) {
                this.mapEntities(entityJson);
                var $agentCards = this.buildAgentCards(entityJson);
                this.showAgentCards($agentCards);
                this.showSearchResultsMessage(entityJson.length);
				var $mapOptionsInputs = $('.map-options input[type="text"]');
                $mapOptionsInputs.val('').blur(); //clear search inputs & trigger blur so placeholders come back IE9
            } else {
                if (this.searchParameters.distance < 400) {
                    this.bumpSearchDistance(); //set a larger radius
                    this.doSearch(); //repeat the search
                } else {
                    this.showError("No results found");
                }
            }

        },

        search: function() {

            var self = nmcom.officeLocator;

            self.clearSearchParameters();
            self.clearMessage();
            self.clearYourLocation();

            self.searchParameters.address = $('input[name="address"]').val();

            if (self.searchParameters.address.length > 0) {
                self.doSearch();
            }
        },

        addressGeocoded: function(geocodeResults, geocodeStatus) {
            var lat;
            var lng;
            if (geocodeStatus === google.maps.GeocoderStatus.OK) { //geocoded request successfully, call backend for offices at this location
                var firstResult = geocodeResults[0];
                var location = firstResult['geometry']['location'];
                lat = location.lat();
                lng = location.lng();

                this.searchParameters.lat = lat;
                this.searchParameters.lng = lng;

                this.doSearch();

            } else {
                this.showError('Address not found');
                // alert('Geocode failed - do stuff'); //TODO: handle geocode error, reset map and inform user
            }

        },

        doSearch: function() {

            var self = this;

            var queryString = '';
            var params = this.searchParameters;

            if ((params.address.length > 0) && !(params.lat && params.lng)) { //address is present but hasn't been geocoded yet
                var geocodeRequest = {
                    region: 'US',
                    address: params.address
                };
                this.mapObjects.geocoderInstance.geocode(geocodeRequest, function(geocodeResults, geocodeStatus) {
                    self.addressGeocoded.call(self, geocodeResults, geocodeStatus);
                });
            } else {
                if (params.distance === 0) {
                    this.searchParameters.distance = this.settings.defaultSearchDistance;
                    params.distance = this.settings.defaultSearchDistance;
                }
                if (params.lat && params.lng) {
                    queryString = '&lat=' + params.lat + '&lng=' + params.lng + '&distance=' + params.distance;
                }
                if (params.lastName.length > 0) {
                    queryString += '&lastname=' + params.lastName;
                }

                $.getJSON(this.settings.entityServiceUrl + queryString, function(data) {
                    self.entityResultsReturned.call(self, data);
                });
            }
        },

        gpsSearch: function () {
            var self = nmcom.officeLocator;

            self.clearSearchParameters();
            self.clearMessage();
            self.clearYourLocation();
			
            navigator.geolocation.getCurrentPosition(self.gpsLocateCallbackSuccess, self.gpsLocateCallbackError);
        },


        gpsLocateCallbackSuccess: function (position) {
            var self = nmcom.officeLocator;
			
            self.clearSearchParameters();
            self.searchParameters.lat = position.coords.latitude;
            self.searchParameters.lng = position.coords.longitude;
			
            $("#advisor-location-section").append('<input type="hidden" name=lng value=' + self.searchParameters.lng + '>');
            $("#advisor-location-section").append('<input type="hidden" name=lat value=' + self.searchParameters.lat + '>');
			self.codeLatLng(self.searchParameters.lat, self.searchParameters.lng);        
        },

        gpsLocateCallbackError: function (error) {
			var self = nmcom.officeLocator;
			
			self.clearAgentCards();
			self.showError("Unable to determine your exact location. <br> <br> Please use the search fields.");
			self.removeMarkers();

            // if permission denied
            if (error.code === 1) {
                $('#geolocation-search').hide();
            }

			self.resetMap();
        },
		
		resetMap: function() {
            var self = nmcom.officeLocator;
            this.mapObjects.centerOfUsa = new google.maps.LatLng(38, -98);

            self.mapObjects.mapInstance.setOptions({
                zoom: this.settings.minZoomLevel,
                minZoom: self.settings.minZoomLevel,
                center: this.mapObjects.centerOfUsa,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scaleControl: true,
                scaleControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                streetViewControl: false,
                panControl: false
            });
        },
		
		codeLatLng: function(lat, lng) {
			var self = nmcom.officeLocator;
			var latlng = new google.maps.LatLng(lat, lng);
			this.mapObjects.geocoderInstance.geocode({'latLng': latlng}, function(results, status) {
			  if (status === google.maps.GeocoderStatus.OK) {
				//Check result 0
				var result = results[0];
				//look for locality tag, administrative_area_level_1, and postal_code
				var city = "";
				var state = "";
				var zip = "";
				for(var i=0, len=result.address_components.length; i<len; i++) {
					var ac = result.address_components[i];
					if (ac.types.indexOf("locality") >= 0) {
						city = ac.long_name;
					}
					if (ac.types.indexOf("administrative_area_level_1") >= 0) {
						state = ac.short_name;
					}
					if (ac.types.indexOf("postal_code") >= 0) {
						zip = ac.short_name;
					}
				}
				//only report if we got Good Stuff
				if(city !== '' && state !== '' && zip !== '') {
					self.searchParameters.address = city + ", " + state + " " + zip;
			        self.showYourLocationMessage();
					self.doSearch();					
				}
			  }
			});
		},

		clearSearchParameters: function () {
            var params = this.searchParameters;
            params.distance = 0;
            params.address = '';
            params.lastName = '';
            params.lat = false;
            params.lng = false;
        },

        /*If no results are returned in the default search radius,
         * try gradually larger radiuses, up to max of 400 miles
         */
        bumpSearchDistance: function() {
            switch (this.searchParameters.distance) {
            case this.settings.defaultSearchDistance:
                this.searchParameters.distance = 50;
                break;
            case 50:
                this.searchParameters.distance = 100;
                break;
            case 100:
                this.searchParameters.distance = 400;
                break;
            }
        }
    }); //extend


})(window.jQuery, window.nmcom || (window.nmcom = {}));

jQuery(function() {
    nmcom.officeLocator.init();
});