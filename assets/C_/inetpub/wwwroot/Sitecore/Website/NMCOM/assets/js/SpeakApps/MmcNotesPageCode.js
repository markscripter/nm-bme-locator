﻿define(["sitecore", "jquery"], function (Sitecore, jQuery) {
    var mmcNotesPageCode = Sitecore.Definitions.App.extend({
        initialized: function () {
            var app = this;
        },
        getQueryStringParms: function () {
            var id = Sitecore.Helpers.url.getQueryParameters(window.location.href)['id'];
            var lang = Sitecore.Helpers.url.getQueryParameters(window.location.href)['lang'];
            var ver = Sitecore.Helpers.url.getQueryParameters(window.location.href)['ver'];
            var processorItem = Sitecore.Helpers.url.getQueryParameters(window.location.href)['processorItem'];
            if (Sitecore.Helpers.id.isId(id)) {
                var data = { "id": id, "lang": lang, "ver": ver, "processorItem": processorItem };
                return data;
            }
        },
        formOk: function (form) {
            var test = true;
            if (form.pageOwner == "") { alert("Page Owner is required."); test = false; }
            else if (form.marketingOwner == "") { alert("Marketing Owner is required."); test = false; }
            else if (form.filingNumber == "") { alert("Filing Number is required."); test = false; }
            else if (form.pageExpiration == "" || form.pageExpiration === undefined || form.pageExpiration === null) { alert("Page Expiration is required."); test = false; }
            return test;
        },
        saveButton_Clicked: function () {
            var app = this;
            var queryString = app.getQueryStringParms();

            var formData = {
                id: queryString.id,
                lang: queryString.lang,
                ver: queryString.ver,
                processorItem: queryString.processorItem,
                pageOwner: this.PageOwner.get("text"),
                marketingOwner: this.MarketingOwner.get("text"),
                filingNumber: this.MmcFilingNumber.get("text"),
                comments: this.Comments.get("text"),
                pageExpiration: this.PageExpiration.get("formattedDate")
            };

            if (this.formOk(formData)) {
                this.Save.set("isEnabled", false);
                this.Save.set("text", "Saving...");
                jQuery.ajax({
                    type: "POST",
                    url: "/nmglobal/SpeakApps/UpdateNotesData",
                    data: formData,
                    success: function (response) {
                        if (response == "success") {
                            var url = "/sitecore/shell/sitecore/content/Applications/Content Editor.aspx?id=" + formData.id + "&amp;fo=" + formData.id + "&amp;la=" + formData.lang + "&amp;ver=" + formData.ver;
                            window.top.location.href = url;
                        }
                    },
                    error: function () {
                        console.log("There was an error. Try again please!");
                    }
                });
            }
        }
    });
    return mmcNotesPageCode;
});