﻿;
(function ($, aws, undefined) {
    'use strict';
    nmcom.asw = nmcom.asw || {};

    nmcom.asw = (function () {
        var aswModel = function (parentElement) {
            var model = {};
            var me = {};

            me.Parent = $(parentElement);

            me.settings = {
                cache: {},
                agentList: null,
                moreResultsClicked: false,
                countOfDisplayedResults: 5,
                scrollPosition: 0,
                searchTimer: null,
                searchTimeDelay: 300, //Search Delay
                reqStr: null,
                reqInProgress: false,
                menuClick: false,
                webServiceEnvironment: 'www.northwesternmutual.com',
                submitURL: '/nmcomApi/advisorsuggest?type=FR,MD',
                secureSite: false,
                activeItemIndex: -1,
                aswRowHeight: 60,
                aswScrollPosOffset: 3,
                queryLengthBeforeSuggest: 2
            };

            model.initModel = function() {

                if (me.Parent.length > 0) {
                    var $aswField = me.Parent.find(".aswInputField");
                    var $aswList = me.Parent.find("#aswList");

                    /*Turn off default browser autocomplete for ASW input field*/
                    $aswField.attr('autocomplete', 'off');

                    /***********************************************/
                    /*          Bind Events                */
                    /***********************************************/
                    //Prevent menu from closing when scrollbar selected
                    $aswList.on("mousedown", function() {
                        me.settings.menuClick = true;
                    });

                    //Bind event to move menu when window resize occurs
                    $(window).resize(function() {
                        model.positionASWContainer();
                        model.positionLoaderImage();
                    });

                    //Bind click event to results list
                    $aswList.on("click", "li", function() {
                        model.resultListClickEvent(this);
                    });

                    //Bind key up event to initiate searches
                    $(".aswInputField").on("keyup", function(e) {
                        model.inputFieldKeyUpEvent(e);
                    });

                    //Bind an event to clear the dropdown if the auto complete field loses focus
                    $aswField.blur(function() {
                        model.inputFieldBlurEvent();
                    });

                    //Clear active list item class when user hovers on menu
                    $aswList.on("hover", "li", function() {
                        me.Parent.find("#aswList li").removeClass("activeListItem");
                    });
                }

            };

            model.inputFieldKeyUpEvent = function(e) {
                var $aswField = me.Parent.find(".aswInputField");
                var $loaderImage = me.Parent.find("#aswLoaderImage");
                var keyPressed = e.which;
                var containerVisible = me.Parent.find("#aswContainer").is(":visible");

                //Handle menu navigation with keyboard if menu is visible
                if (containerVisible && keyPressed === 38) { //If up arrow pressed, navigate up
                    if (me.settings.activeItemIndex > 0) {
                        model.decreaseScrollPosition();
                        me.Parent.find("#aswList li").eq(me.settings.activeItemIndex).removeClass("activeListItem");
                        me.settings.activeItemIndex--; //Decrement active index
                        me.Parent.find("#aswList li").eq(me.settings.activeItemIndex).addClass("activeListItem");
                    }
                } else if (containerVisible && keyPressed === 40) { //If down arrow pressed, navigate down
                    if (me.settings.activeItemIndex < (me.Parent.find("#aswList li").size() - 1)) {
                        model.increaseScrollPosition();
                        me.Parent.find("#aswList li").eq(me.settings.activeItemIndex).removeClass("activeListItem");
                        me.settings.activeItemIndex++; //Increment active index
                        me.Parent.find("#aswList li").eq(me.settings.activeItemIndex).addClass("activeListItem");
                    }
                } else if (containerVisible && keyPressed === 13) { //If enter, select the current active item
                    model.resultListClickEvent("#aswList li.activeListItem");
                    me.settings.activeItemIndex--; //Set new index to previous row
                    me.Parent.find("#aswList li").eq(me.settings.activeItemIndex).addClass("activeListItem");
                } else { //If not up, down or enter, do normal key handling
                    me.settings.activeItemIndex = -1; //Reset arrow key index
                    if ($aswField.val().length > 1) { //Only return results after 2 or more characters entered
                        $loaderImage.css('display', 'inline'); //use this instead of .show() as there was an issue with .show() applying display: block to the element
                        model.positionLoaderImage(); // set position of image
                    }
                    //Delay the search call until period of time (declared in searchTimeDelay) since the last key press event to help reduce server requests
                    //if (me.settings.searchTimer && e.which != 8) { //Don't delay if keypress is backspace, will retrieve from cache
                    //    clearTimeout(me.settings.searchTimer);
                    //    me.settings.searchTimer = setTimeout(model.initiateSearch, me.settings.searchTimeDelay);
                    //} else if (e.which == 8) {
                    model.initiateSearch();
                    //} else {
                    //    me.settings.searchTimer = setTimeout(model.initiateSearch, me.settings.searchTimeDelay);
                    //}
                }
            };

            model.resultListClickEvent = function(selectedObject) {
                if ($(selectedObject).is("#listItem_MoreResults")) { //If the "show more results.." button is pressed
                    me.settings.menuClick = true;
                    model.showMoreResultsClicked();
                    model.drawAgentList();

                    // Auto scroll up to show additional results on the mobile form to make apparent user can scroll in window
                    if (me.Parent.find("#txtMobileFRName").length > 0) {
                        var mobileScrollPosition = me.Parent.find("#aswList").scrollTop();
                        setTimeout(function() {
                            me.Parent.find("#aswList").animate({ scrollTop: mobileScrollPosition + 133 }, 600);
                        }, 300); //Wait 300ms to auto scroll
                    }
                } else { //The user has clicked on an agent
                    me.settings.menuClick = false;

                    me.Parent.find(".aswInputField").val($(selectedObject).find("h4.listItem_Name").text());
                    me.Parent.find(".aswNum").val($(selectedObject).find("div.listItem_Num").text());

                    model.clearAgentList();

                    if (me.Parent.find("#txtAccordionFRName").length > 0) { //If this is the accordion widget
                        me.Parent.find(".tableSlide").slideDown();
                        me.Parent.find(".tableRowFade").fadeIn();

                        if ($.trim(me.Parent.find('#txtAccordionFRName').val()) === "") {
                            $(this).val("Representative's Name").addClass('accordionLabelColor');
                        }
                    }
                }
            };

            model.inputFieldBlurEvent = function() {
                var frNameLabel = "Representative's Name";

                setTimeout(function() { //Use a timeout to prevent blur from preventing menu click events
                    if (!me.settings.menuClick) { //Only blur if not a menu click
                        model.clearAgentList();

                        if (me.Parent.find("#txtAccordionFRName").length > 0) {
                            me.Parent.find(".tableSlide").slideDown();
                            me.Parent.find(".tableRowFade").fadeIn();

                            if ($.trim(me.Parent.find('#txtAccordionFRName').val()) === "") {
                                $(this).val(frNameLabel).addClass('accordionLabelColor');
                            }
                        }
                    } else {
                        me.Parent.find(".aswInputField").focus(); //Refocus the input field if click was on menu
                        me.settings.menuClick = false;
                    }
                }, 400);
            };

            model.initiateSearch = function() {
                var $aswField = me.Parent.find(".aswInputField");
                var $loaderImage = me.Parent.find("#aswLoaderImage");
                var query = $aswField.val();

                if (!me.settings.reqInProgress) {
                    me.settings.reqInProgress = true;
                    me.settings.reqStr = query; //Keep a copy of the query for comparison later
                    //This allows you to only run after a certain   of characters are entered.
                    if (query.length > me.settings.queryLengthBeforeSuggest) {
                        //Check the cache for this query
                        if (query in me.settings.cache) {
                            //Clear old results and draw new ones
                            me.Parent.find("#results").empty();
                            me.settings.agentList = me.settings.cache[query];
                            model.showMoreResultsReset();
                            model.drawAgentList();
                            me.settings.reqInProgress = false;
                        } else {
                            //Get Results
                            $.ajax({
                                type: "GET",
                                dataType: "json",
                                url: me.settings.submitURL + '&comboname=' + query,
                                success: function(results) {
                                    if (me.settings.reqStr === $aswField.val()) {
                                        var dataResult = results;
                                        if (results.Data !== undefined)
                                        {
                                            dataResult = results.Data;
                                        }
                                        if (dataResult.length > 0) { //If there are results
                                            me.settings.agentList = dataResult;
                                            //Add results to cache
                                            me.settings.cache[query] = me.settings.agentList;
                                            model.showMoreResultsReset();
                                            model.drawAgentList(query);
                                            me.settings.reqInProgress = false;
                                        } else { //If no results
                                            model.clearAgentList();
                                            $loaderImage.hide();
                                            me.settings.reqInProgress = false;
                                        }
                                    } else { //Do new search if results don't match latest request
                                        me.settings.reqInProgress = false;
                                        model.initiateSearch();
                                    }
                                },
                                error: function() {
                                    $loaderImage.hide();
                                    model.clearAgentList();
                                    me.settings.reqInProgress = false;
                                },
                                timeout: 8000 // Set timeout to 8 seconds
                            });
                        }
                    } else { //If query is less than 2 characters
                        $loaderImage.hide();
                        model.clearAgentList();
                        me.settings.reqInProgress = false;
                    }
                }
            };

            model.drawAgentList = function() {
                var $aswContainer = me.Parent.find("#aswContainer");
                var $aswList = me.Parent.find("#aswList");
                var $loaderImage = me.Parent.find("#aswLoaderImage");

                var $ul = $aswList;
                $ul.empty();
                var tempElementString = "";
                var length = me.settings.agentList.length;
                var accumulator;

                if (length > 5 && me.settings.moreResultsClicked === false) { //Show first 5 results
                    accumulator = 5;
                } else if (length > (me.settings.countOfDisplayedResults + 20) && me.settings.moreResultsClicked === true) { //If show more results is clicked show next 20 results
                    accumulator = me.settings.countOfDisplayedResults + 20;
                    me.settings.countOfDisplayedResults += 20;
                    me.settings.moreResultsClicked = false;
                } else if (length === 1) {
                    $(".aswNum").val(me.settings.agentList[0].AgentNum);
                    accumulator = 1;
                } else {
                    accumulator = length; //If show more results is clicked and less than 20 more results remain
                }

                for (var i = 0; i < accumulator; ++i) {
                    tempElementString = "";

                    tempElementString += "<li><div><img src='" + me.settings.agentList[i].Photo + "'</img></div>";
                    tempElementString += "<h4 class='listItem_Name'>" + me.settings.agentList[i].FirstName + " " + me.settings.agentList[i].LastName + "</h4>";
                    tempElementString += "<p>" + me.settings.agentList[i].City + ", " + me.settings.agentList[i].State + "</p>";
                    tempElementString += "<div class='listItem_Num' style='display: none;'>" + me.settings.agentList[i].AgentNum + "</div></li>";

                    $ul.append(tempElementString);
                }

                if (length > 5 && me.settings.moreResultsClicked === false) { //If more results available, add more results button
                    $ul.append("<li id='listItem_MoreResults'><span>See more results...</span></li>");
                }
                $loaderImage.hide();
                $aswContainer.append($ul); // add the list to the DOM

                // If previous scroll position set then scroll to that location
                if (me.settings.scrollPosition !== 0) {
                    $aswList.scrollTop(me.settings.scrollPosition);
                }

                //Slide up affect for accordion functionality
                if (me.Parent.find("#txtAccordionFRName").length > 0) {
                    me.Parent.find(".tableSlide").slideUp(400, function () { $aswContainer.show() });
                    me.Parent.find(".tableRowFade").hide();
                } else {
                    $aswContainer.show();
                    model.positionASWContainer();
                }
            };

            model.clearAgentList = function() {
                me.Parent.find("#aswContainer").hide();
                me.Parent.find("#aswList").empty();
            };

            model.showMoreResultsClicked = function() {
                me.Parent.find("#aswList").addClass("showMoreResults");
                me.settings.scrollPosition = me.Parent.find("#aswList").scrollTop();
                me.settings.moreResultsClicked = true;
            };

            model.showMoreResultsReset = function() {
                me.Parent.find("#aswList").removeClass("showMoreResults");
                me.settings.moreResultsClicked = false;
                me.settings.countOfDisplayedResults = 5;
                me.settings.scrollPosition = 0;
            };

            model.positionLoaderImage = function() {
                var imageLeft;
                var imageTop;
                var $tempField = me.Parent.find(".aswInputField");
                var $loaderImage = me.Parent.find("#aswLoaderImage");

                //Only set positions if page is not accordion or mobile*/
                if (me.Parent.find("#txtAccordionFRName").length <= 0 && me.Parent.find("#txtMobileFRName").length <= 0) {

                    //Only set positions if both fields are visible to avoid buggy behavior
                    if ($loaderImage.is(":visible") && $tempField.is(":visible")) {
                        imageLeft = ($tempField.offset().left + $tempField.outerWidth());
                        imageTop = $tempField.offset().top;
                        $loaderImage.offset({ top: imageTop, left: imageLeft });
                    }
                }
            };

            model.positionASWContainer = function() { //Function used to position the asw container under the input field
                var menuTop;
                var menuLeft;
                var $tempField = me.Parent.find(".aswInputField");
                var $aswContainer = me.Parent.find("#aswContainer");

                //Only set positions if page is not accordion or mobile
                if (me.Parent.find("#txtAccordionFRName").length <= 0 && me.Parent.find("#txtMobileFRName").length <= 0) {

                    //Only set positions if both fields are visible to avoid buggy behavior
                    if ($aswContainer.is(":visible") && $tempField.is(":visible")) {
                        menuLeft = $tempField.offset().left;
                        menuTop = $tempField.offset().top + $tempField.outerHeight();
                        $aswContainer.offset({ top: menuTop, left: menuLeft });
                    }
                }
            };

            model.decreaseScrollPosition = function() { //Used to scroll up in the agent list
                var $aswList = me.Parent.find("#aswList");
                var tempScrollMin = $aswList.scrollTop(); //Find the lowest index visible
                var tempItemPosition = aswRowHeight * me.settings.activeItemIndex;

                if (tempItemPosition <= tempScrollMin) {
                    var tempNewScrollPosition = aswRowHeight * (me.settings.activeItemIndex - 1);
                    $aswList.scrollTop(tempNewScrollPosition);
                }
            };

            model.increaseScrollPosition = function () { //Used to scroll down in the agent list
                var $aswList = me.Parent.find("#aswList");
                var tempScrollMax = $aswList.scrollTop() + (me.settings.aswRowHeight * 4); //Highest index visible = row height * 4 row items
                var tempItemPosition = me.settings.aswRowHeight * me.settings.activeItemIndex;
                if (tempItemPosition >= tempScrollMax) {
                    var tempNewScrollPosition = me.settings.aswRowHeight * (me.settings.activeItemIndex - me.settings.aswScrollPosOffset);
                    $aswList.scrollTop(tempNewScrollPosition);
                }
            };

            return model;
        };
        var init = function (parentElement) {
            var model = new aswModel(parentElement);
            model.initModel();
            return model;
        };
        return {
            Init: init
        };
    })();

})(window.jQuery, window.nmcom || (window.nmcom = {}));
