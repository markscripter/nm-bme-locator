﻿;
(function ($, nmcom) {
    'use strict';
    nmcom.StateSelector = nmcom.StateSelector || {};

    $.extend(nmcom.StateSelector, {
        settings: {
            cookieName: "selected-state",
            cookieDurationDays: 365
        },

        verbText: '',
        verbTextStateToken: '{state}',

        mapObjects: {
            geocoderInstance: null
        },

        init: function() {
            var self = this;

            //hide the "use my location" button if the browser doesn't support it
            var geolocationSupport = ('geolocation' in navigator);
            if (!geolocationSupport) {
                $('#geolocation-search').hide();
            }

            self.loadGoogleMaps();

            self.setupPageEvents();

            self.checkForCookie();

        },

        setupPageEvents: function() {
            var self = this;

            $('#state').change(self.handleStateSelection);

            $('#state-map').click(function() {
                window.location = $('#state-link').attr('href');
            });

            $('#geolocation-search').click(self.sensorSearch);

        },

        loadGoogleMaps: function() { //inject Google Maps script
            var mapsUrl = "https://maps.google.com/maps/api/js?v=3&client=gme-northwesternmutual&sensor=false&callback=nmcom.StateSelector.googleMapsLoaded";
            $.getScript(mapsUrl);
        },

        googleMapsLoaded: function() { //Google Maps has loaded, go forth and do stuff
            this.mapObjects.geocoderInstance = new google.maps.Geocoder();
        },

        geocodeLatLng: function(lat, lng) {
            var self = nmcom.StateSelector;
            var latlng = new google.maps.LatLng(lat, lng);
            var geocodeRequest = {
                'location': latlng
            };
            self.mapObjects.geocoderInstance.geocode(geocodeRequest, self.addressGeocoded);
        },

        addressGeocoded: function(geocodeResults, geocodeStatus) {
            var self = nmcom.StateSelector;
            var stateAbbr;
            var state;
            self.hideSpinner();
            if (geocodeStatus === google.maps.GeocoderStatus.OK) { //geocoded request successfully
                var firstResult = geocodeResults[0];
                var addressComponents = firstResult.address_components;
                for (var i = 0; i < addressComponents.length; i++) {
                    var typesArray = addressComponents[i].types;
                    if (typesArray.indexOf("administrative_area_level_1") > -1) {
                        stateAbbr = addressComponents[i].short_name;
                        state = addressComponents[i].long_name;
                    }
                }

                if (stateAbbr != null && state != null && self.isValidState(stateAbbr)) {

                    var stateUrl = $('#state').find('option[abbr="' + stateAbbr + '"]').val();
                    self.goToStatePage(state, stateAbbr, stateUrl);

                } else {
                    ///TODO: are alerts OK for this error?
                    alert("Unable to determine your location, please select your state.");
                    self.showNoState();
                }

            } else {
                ///TODO: are alerts OK for this error?
                alert("Unable to determine your location, please select your state.");
                self.showNoState();
            }
        },

        sensorSearch: function(e) {
            var self = nmcom.StateSelector;
            e.preventDefault();
            self.showSpinner();
            navigator.geolocation.getCurrentPosition(self.sensorLocateCallbackSuccess, self.sensorLocateCallbackError);
        },

        sensorLocateCallbackSuccess: function(position) {
            var self = nmcom.StateSelector;
            self.geocodeLatLng(position.coords.latitude, position.coords.longitude);
        },

        sensorLocateCallbackError: function(error) {
            window.console && console.log('error determining location with sensor. error:');
            window.console && console.log(JSON.stringify(error, null, "\t"));
            ///TODO: are alerts OK for this error?
            alert("Unable to determine your location, please select your state.");
            self.hideSpinner();
        },

        checkForCookie: function() {
            var self = this;
            var cookieValue = nmcom.getCookie(self.settings.cookieName);
            if (cookieValue != null) {
                var state = cookieValue.split('|')[0];
                var stateAbbr = cookieValue.split('|')[1];
                self.showState(stateAbbr, state);
            }
        },

        handleStateSelection: function() {
            var self = nmcom.StateSelector;
            var selector = $(this);
            if (this.selectedIndex > 0) { //user has chosen something other than the "state..." first entry
                var selectedOption = selector.children('option:selected');
                self.goToStatePage(selectedOption.text(), selectedOption.attr('abbr'), selectedOption.val());
            }
        },

        goToStatePage: function(state, stateAbbr, url) {
            var self = nmcom.StateSelector;

            //set cookie for return visit
            var cookieValue = state + "|" + stateAbbr;
            nmcom.setCookie(self.settings.cookieName, cookieValue, self.settings.cookieDurationDays);

            //redirect to state page
            var http = 'http';
            if (url.slice(0, http.length) !== http) {
                url = window.location.pathname + "/" + url;
            }
            window.location = url;
        },

        makeStateNameUrlFriendly: function(state) {
            var urlFriendlyState;
            if (state.toLowerCase() === "dist. of columbia") {
                urlFriendlyState = "district-of-columbia";
            } else {
                urlFriendlyState = state.toLowerCase().replace(/\s/g, "-");
            }
            return urlFriendlyState;
        },

        showState: function(stateAbbr, state) {
            var self = nmcom.StateSelector;

            //show things that were hidden when there wasn't a valid state
            $('.state-not-found').removeClass('state-not-found');

            //update the link
            var url = window.location.pathname + "/" + self.makeStateNameUrlFriendly(state);
            $('#state-link').attr('href', url).text(state);

            //update the image
            var imgSrc = '/NMCOM/assets/img/states/' + stateAbbr.toUpperCase() + '.png';
            var imgAlt = self.verbText.replace(self.verbTextStateToken, state);
            $('#state-map').attr({
                'src': imgSrc,
                'alt': imgAlt
            });

            self.hideSpinner();
        },

        /*
         * if MaxMind fails to find the state, or a zip search returns a bad response
         * hide everything but the zip and state inputs
         */
        showNoState: function() {
            $('#state-filter-component').find('p:first-child').addClass('state-not-found');
            $('#zip-change-text').addClass('state-not-found');
            $('#state-map').addClass('state-not-found');
        },

        showSpinner: function() {
            $('#loading').css('display', 'inline-block');
        },

        hideSpinner: function() {
            $('#loading').hide();
        },

        isValidState: function(stateAbbr) {
            var stateList = new Array("AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY");
            return (stateList.indexOf(stateAbbr.toUpperCase()) > -1);
        }

    }); //extend

})(window.jQuery, window.nmcom || (window.nmcom = {}));

jQuery(function() {
    nmcom.StateSelector.init();
});