﻿;
(function ($, nmcom) {
    'use strict';
    nmcom.nameOrAddressChangeForm = nmcom.nameOrAddressChangeForm || {};

    $.extend(nmcom.nameOrAddressChangeForm, {
        settings: {
        },

        init: function () {
            var self = this;

            self.setupPageEvents();

            self.setupValidation();
        },

        setupPageEvents: function () {

            $(document).ajaxStart(function () {
                $('#loading').css('display', 'inline-block');
                $('#change-name-or-address-form input[type="submit"]').prop('disabled', true);
            }).ajaxStop(function () {
                $('#loading').hide();
                $('#change-name-or-address-form input[type="submit"]').prop('disabled', false);
            });

            $('#change-name-or-address-form').submit(nmcom.nameOrAddressChangeForm.handleSubmit);

        },

        setupValidation: function () {
            nmcom.validation.setupValidate('#change-name-or-address-form', {
                rules: {
                    'ReqFirstName': {
                        required: true
                    },
                    'ReqLastName': {
                        required: true
                    },
                    'ReqBirthDate': {
                        required: true
                    }
                },
                messages: {
                    'ReqFirstName': {
                        required: "Please enter your first name."
                    },
                    'ReqLastName': {
                        required: "Please enter your last name."
                    },
                    'ReqBirthDate': {
                        required: "Please enter your birth date."
                    }
                }
            });
        },

        handleSubmit: function (e) {
            var self = nmcom.nameOrAddressChangeForm;
            e.preventDefault();

            var $form = $(e.currentTarget);

            if (!$form.valid()) {
                return;
            }

            self.submitForm();

        },

        submitForm: function () {
            var self = nmcom.nameOrAddressChangeForm;
            var $form = $('#change-name-or-address-form');

            var xhr2 = !!(window.FormData && ("upload" in ($.ajaxSettings.xhr())));
            if (xhr2) { //this browser supports AJAX file uploading, do it!
                var actionUrl = $form.attr('action');
                var data = new FormData($form[0]);

                $.ajax({
                    url: actionUrl,
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    dataType: 'text',
                    success: self.formSubmitSuccess,
                    error: self.formSubmitError
                });
            } else { //this is an old browser that doesn't support ajax file uploading, so do the post to hidden iframe trick
                $('iframe').attr({
                    "name": "postResultsFrame"
                }).appendTo($form).hide();

                /*
                 * Tell the form to post to the hidden iframe
                 * remove the submit event handler so that a traditional form POST may occur
                 * submit the form
                 */
                $form.attr("target", "postResultsFrame").unbind('submit').submit();

                self.formSubmitSuccess();
                $('#loading').hide();
            }

            //track analytics
            //nmcom.adobeDtmDirectCall("");
        },

        formSubmitSuccess: function () {
            $('#change-name-or-address-thankyou').show();
            $('#change-name-or-address-form').hide();
        },

        formSubmitError: function (jqXhr, status, error) {
            var pageUrl = window.location.href;
            var errorMsg = {
                scriptName: 'change-name-or-address-form-view.js',
                message: 'Change Name or Address form failed to submit.  URL=' + pageUrl + ' STATUS=' + status + ' ERROR=' + error,
                originUrl: pageUrl,
                browser: window.navigator.userAgent,
                lineNumber: 0,
                level: 3
            };

            nmcom.errorHandler.logError(errorMsg);
            $('#change-name-or-address-error').show();
            $('#change-name-or-address-form input[type="submit"]').prop('disabled', false);
        }

    }); //extend

})(window.jQuery, window.nmcom || (window.nmcom = {}));

jQuery(function () {
    nmcom.nameOrAddressChangeForm.init();
});