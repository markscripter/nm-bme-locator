﻿;
(function ($, nmcom, undefined) {
    'use strict';
    nmcom.search = nmcom.search || {};

    nmcom.search.Initialize = function () {
        var searchToggleButton = $('.search button');
        if (searchToggleButton.length) {
            searchToggleButton.click(function (e) {
                var parent = $(this).parent('.search');
                parent.toggleClass('active');
                if (parent.hasClass('active')) {
                    var searchBox = parent.find('input[type="search"]');
                    var phText = searchBox.attr('placeholder');
                    if (searchBox.val() === phText) {
                        searchBox.val('').blur();
                    }
                    searchBox.focus();
                }
                e.preventDefault();
                return false;
            });
        }

        var searchMore = $('.search-more');
        if (searchMore.length) {
            searchMore.click(function (e) {
                e.preventDefault();
                var form = $('.search-form');
                var searchIndex = $('.search-index');
                var searchSize = $('.search-size');
                var action = form.attr('action');
                var data = form.serialize();
                $.post(action, data, function (result) {
                    $('.search-list').append(result);
                    searchIndex.val(parseInt(searchIndex.val()) + 1);
                    var count = $(result).filter('li').length;
                    if (count < parseInt(searchSize.val())) {
                        form.hide();
                    }
                });
            });
        }

        // add placeholders for fields without labels in older browsers (IE9)
        var searchInputs = $('#address, #last-name');
        if (!Modernizr.input.placeholder && searchInputs.length) {
            nmcom.setupLegacyPlaceholders(searchInputs);
        }
    };

    nmcom.search.Search = (function () {
        var searchModel = function(config) {
            var vm = {};
            var me = {}; // for private members

            me.defaults = {
                ItemId: '',
                searchApi: 'nmcomApi/search',
                enableHash: true,
                SettingID: '',
                PageSize: 10,
                Input: {
                    SearchInput: $(),
                    SearchButton: $()
                },
                Containers: {
                    ResultsContainer: $(),
                    NoResultsContainer: $(),
                    SelectedFacetContainer: $(),
                    FacetContainer: $(),
                    PaginationContainer: $()
                },
                Labels: {
                    ResultsTotalLabel: $(),
                    ResultsTermLabel: $(),
                    ResultsInfo: $()
                },
                Selectors: {
                    resultsTemplate: '',
                    selectedFacetTemplate: '',
                    facetTemplate: '',
                    pagingTemplate: '',
                    pageLink: '',
                    pageLinkPrev: '',
                    pageLinkNext: '',
                    resetFacetsLink: '',
                    removeFacetLink: '',
                    facetCheckbox: ''
                }
            };

            me.adobeTracker = nmcom.AdobeTracker != undefined ? nmcom.AdobeTracker : undefined;

            var opts = $.extend(true, {}, me.defaults, config);

            //#region settings
            me.TrackAnalytics = true;
            me.ItemId = opts.ItemId;
            vm.items = [];
            vm.pages = [];
            vm.currentPageIndex = 0;
            vm.searchResultsTotal = 0;
            vm.searchTotalPages = 0;
            //#endregion

            //#region methods
            vm.setup = function () {
                vm.showNoResultMessage(false);

                if (opts.Input.SearchButton.length) {
                    opts.Input.SearchButton.on('click', _.debounce(function (e) {
                        e.preventDefault();
                        me.TrackAnalytics = true;
                        vm.facetModel.resetFacets(); // this will reset the search model and perform the search
                        return false;
                    }, 200));
                }

                if (!Modernizr.input.placeholder && opts.Input.SearchInput.length) {
                    nmcom.setupLegacyPlaceholders(opts.Input.SearchInput);
                }
            };

            vm.prepareTemplates = function() {
                if (opts.Selectors.resultsTemplate.length) {
                    me.resultsTemplate = _.template($(opts.Selectors.resultsTemplate).html());
                }
                if (opts.Selectors.selectedFacetTemplate.length) {
                    me.selectedFacetTemplate = _.template($(opts.Selectors.selectedFacetTemplate).html());
                }
                if (opts.Selectors.facetTemplate.length) {
                    me.facetTemplate = _.template($(opts.Selectors.facetTemplate).html());
                }
                if (opts.Selectors.pagingTemplate.length) {
                    me.pagingTemplate = _.template($(opts.Selectors.pagingTemplate).html());
                }
            };

            vm.loadSettings = function (responseData) {
                vm.currentPageIndex = responseData.PageIndex;
                vm.pages = responseData.Pages;
                vm.searchResultsTotal = responseData.NumberOfResults;
                vm.searchTotalPages = responseData.NumberOfPages;
                vm.showNoResultMessage(responseData.NumberOfResults === 0);

                if (opts.Labels.ResultsTermLabel.length && opts.Input.SearchInput.length) {
                    opts.Labels.ResultsTermLabel.html(opts.Input.SearchInput.val());
                }
                if (opts.Labels.ResultsTotalLabel.length) {
                    opts.Labels.ResultsTotalLabel.html(responseData.NumberOfResults);
                }
                vm.showSearchResultsInfo(opts.Input.SearchInput.length && opts.Input.SearchInput.val().length);

                return responseData;
            };

            vm.render = function () {

                vm.prepareTemplates();

                if (opts.Containers.ResultsContainer.length && me.resultsTemplate.length) {
                    opts.Containers.ResultsContainer.html(me.resultsTemplate({
                        results: vm.items
                    }));
                    var ellipsisContainer = opts.Containers.ResultsContainer.find('.ellipsis');
                    if (ellipsisContainer.length) {
                        window.nmcom.ellipsis(ellipsisContainer);
                    }
                }

                if (opts.Containers.SelectedFacetContainer.length) {
                    if (opts.Selectors.selectedFacetTemplate.length && vm.facetModel.hasSelectedFacets()) {
                        opts.Containers.SelectedFacetContainer.html(me.selectedFacetTemplate({
                            facetGroups: vm.facetModel.facetGroups
                        })).show();
                    } else {
                        opts.Containers.SelectedFacetContainer.hide();
                    }
                }

                if (opts.Containers.FacetContainer.length && me.facetTemplate.length) {
                    opts.Containers.FacetContainer.html(me.facetTemplate({
                        facetGroups: vm.facetModel.facetGroups
                    }));
                }

                if (opts.Containers.PaginationContainer.length && me.pagingTemplate.length) {
                    opts.Containers.PaginationContainer.html(me.pagingTemplate({
                        showPreviousButton: vm.currentPageIndex > 0,
                        pages: vm.pages,
                        pageIndex: vm.currentPageIndex,
                        showNextButton: vm.currentPageIndex < (vm.searchTotalPages - 1)
                    }));
                }
            };

            vm.clickOnce = function(e, elem, func) {
                elem.off('click');
                func(elem);
                e.preventDefault();
            }

            vm.setupDom = function() {
                window.nmcom.accordion();
                $(opts.Selectors.pageLink).off('click').one('click', function (e) { vm.clickOnce(e, $(this), vm.getPageItems); });
                $(opts.Selectors.pageLinkPrev).off('click').one('click', function (e) { vm.clickOnce(e, $(this), vm.getPreviousPage); });
                $(opts.Selectors.pageLinkNext).off('click').one('click', function (e) { vm.clickOnce(e, $(this), vm.getNextPage); });
                $(opts.Selectors.resetFacetsLink).off('click').one('click', function (e) { vm.clickOnce(e, $(this), vm.facetModel.resetFacets); });
                $(opts.Selectors.removeFacetLink).off('click').one('click', function (e) { vm.clickOnce(e, $(this), vm.facetModel.facetCheckChanged); });
                $(opts.Selectors.facetCheckbox).off('click').one('click', function (e) { vm.clickOnce(e, $(this), vm.facetModel.facetCheckChanged); });
            }

            vm.reset = function () {
                vm.currentPageIndex = 0;
                vm.items.length = 0;
            };

            vm.querystringSearch = function () {
                var urlParams = window.location.search;
                var termPattern = /q=([^&]+)/i;

                if (termPattern.test(urlParams) && opts.Input.SearchInput.length) {
                    opts.Input.SearchInput.val(decodeURIComponent(termPattern.exec(urlParams)[1]).replace(/\+/g, " "));
                }

                if (window.location.hash.length > 0) {
                    var hash = window.location.hash;
                    hash = hash.substr(1, hash.length - 1);
                    var hashParams = decodeURIComponent(hash);
                    var params = $.parseJSON(hashParams);
                    if (params.SearchTerm !== undefined && opts.Input.SearchInput.length) {
                        opts.Input.SearchInput.val(params.SearchTerm);
                    }
                    if (params.PageIndex !== undefined) {
                        vm.currentPageIndex = params.PageIndex;
                    }
                    if (params.SelectedFacets !== undefined) {
                        vm.facetModel.setSelectedFacets(params.SelectedFacets);
                    }
                }

                return vm.getSearchResults();
            };

            vm.setQuery = function(query) {
                vm.query = query;
                return vm.querystringSearch();
            }

            vm.getSearchResults = function () {

                // interact with any search hint models on the page
                if (window.searchBoxModels !== undefined && window.searchBoxModels.length) {
                    $.each(window.searchBoxModels, function (index, searchBoxModel) {
                        searchBoxModel.hideHints();
                        searchBoxModel.cancelHints();
                        $('.search button').parent('.search').removeClass('active');
                    });
                }

                if (opts.enableHash) {
                    window.location.hash = vm.getSearchHash();
                }

                return vm.doSearch().then(vm.loadSettings).then(vm.render).then(vm.setupDom).then(vm.recordAnalytics);
            };

            vm.getSearchHash = function () {
                return encodeURIComponent(JSON.stringify(vm.getSearchQuery(true))); // to do: make it no call twice
            }

            // the query can be provided with the vm config object.
            // if no provided query exists, construct one
            vm.getSearchQuery = function (excludeSettings) {
                var query;
                if (vm.query != undefined) {
                    query = vm.query;
                    query.PageIndex = vm.currentPageIndex;
                } else {
                    query = {
                        SearchTerm: opts.Input.SearchInput.length ? opts.Input.SearchInput.val() : '',
                        ItemId: me.ItemId,
                        PageIndex: vm.currentPageIndex,
                        PageSize: opts.PageSize,
                        SelectedFacets: vm.facetModel.selectedFacets(),
                        TrackAnalytics: me.TrackAnalytics
                    };
                    if (!excludeSettings && opts.SettingID.length) {
                        query.SettingID = opts.SettingID;
                    }
                }
                return query;
            };

            vm.scrollToTop = function () {

                var offsetTop = 0;

                // search results scroll so the results label is visible
                if (opts.Labels.ResultsInfo.length && opts.Containers.ResultsContainer.length) {
                    if (opts.Labels.ResultsInfo.offset().top < opts.Containers.ResultsContainer.offset().top) {
                        offsetTop = opts.Labels.ResultsInfo.offset().top;
                    } else {
                        offsetTop = opts.Containers.ResultsContainer.offset().top;
                    }
                } else {
                    if (opts.Labels.ResultsInfo.length) {
                        offsetTop = opts.Labels.ResultsInfo.offset().top;
                    }

                    if (opts.Containers.ResultsContainer.length) {
                        offsetTop = opts.Containers.ResultsContainer.offset().top;
                    }
                }

                if (offsetTop < $(window).scrollTop()) {
                    $('html, body').animate({
                        scrollTop: offsetTop - 20
                    }, 500);
                }
            };

            vm.getPageItems = function (elem) {
                var pageIndex = elem.data('pageindex');
                vm.currentPageIndex = pageIndex;
                return vm.getSearchResults().then(vm.scrollToTop);
            };

            vm.getPreviousPage = function () {
                vm.currentPageIndex = (vm.currentPageIndex - 1);
                return vm.getSearchResults().then(vm.scrollToTop);
            };

            vm.getNextPage = function () {
                vm.currentPageIndex = (vm.currentPageIndex + 1);
                return vm.getSearchResults().then(vm.scrollToTop);
            };

            vm.showNoResultMessage = function (show) {
                if (opts.Containers.NoResultsContainer.length) {
                    if (show) {
                        opts.Containers.NoResultsContainer.show();
                    } else {
                        opts.Containers.NoResultsContainer.hide();
                    }
                }
            }

            vm.showSearchResultsInfo = function (show) {
                if (opts.Labels.ResultsInfo.length) {
                    if (show) {
                        opts.Labels.ResultsInfo.show();
                    } else {
                        opts.Labels.ResultsInfo.hide();
                    }
                }
            }

            vm.onDoSearchDone = function (data) {
                vm.items.length = 0;
                vm.items.push.apply(vm.items, data.Results);
                vm.facetModel.updateFacetCounts(data.FacetCounts);
                me.TrackAnalytics = false;
            };

            vm.onDoSearchError = function (jqXhr, status, error) {
                window.console && console.log('error requesting search results. error:');
                window.console && console.log(JSON.stringify(error, null, "\t"));

                var pageUrl = window.location.href;
                var errorMsg = {
                    scriptName: 'search.js',
                    message: 'error requesting search results.  URL=' + pageUrl + ' STATUS=' + status + ' ERROR=' + error,
                    originUrl: pageUrl,
                    browser: window.navigator.userAgent,
                    lineNumber: 0,
                    level: 5
                };

                nmcom.errorHandler.logError(errorMsg);
            };

            vm.doSearch = function () {
                // this function exists only as an external dependency that can be replaced in unit tests.

                var searchParams = vm.getSearchQuery();

                var request = nmcom.Services.Http.POST(
                        opts.searchApi,
                        {},
                        JSON.stringify(searchParams)
                    );

                request.done(vm.onDoSearchDone);
                request.error(vm.onDoSearchError);

                return request;
            };

            vm.recordAnalytics = function () {

                if (me.adobeTracker !== undefined && me.adobeTracker.setVar !== undefined) {
                    try {
                        if (opts.Labels.ResultsTotalLabel.length) {
                            //adobe dtm call to record results count
                            me.adobeTracker.setVar('InteralSearchNumberOfResults', opts.Labels.ResultsTotalLabel.text());

                            nmcom.adobeDtmDirectCall("nmcomInternalSearch");
                        }
                    } catch (e) {
                        var errorMsg = {
                            scriptName: 'search.js',
                            message: '_satellite: Failed to call .track() method with this rule: nmcomInternalSearch',
                            originUrl: window.location.href,
                            browser: window.navigator.userAgent,
                            lineNumber: 361,
                            level: 2
                        };

                        nmcom.errorHandler.logError(errorMsg);
                    }
                }
            };

            //#endregion

            vm.facetModel = nmcom.search.Facets.Init(config, vm.reset, vm.getSearchResults);

            return vm;
        };

        var createSearchModel = function (config) {
            var model = new searchModel(config);
            model.setup();
            return model;
        };

        return {
            Init: createSearchModel
        };
    })();

    nmcom.search.Facets = (function () {
        var facetModel = function(config, resetList, search) {
            var model = {};

            model.setFacetGroups = function(groups) {
                if (groups !== undefined && groups !== null && groups.length) {
                    model.facetGroups = groups;

                    $.each(model.facetGroups, function(index, facetGroup) {

                        $.each(facetGroup.Facets, function(innerindex, facet) {
                            facet.Selected = false;
                            facet.visible = function() {
                                return facet.Count > 0;
                            };
                        });

                        facetGroup.open = false;
                        facetGroup.visible = function() {
                            var visible = false;
                            $.each(facetGroup.Facets, function(innerindex, facet) {
                                if (facet.visible()) {
                                    visible = true;
                                }
                            });
                            return visible;
                        };
                        facetGroup.hasSelectedFacets = function() {
                            var hasSelected = false;
                            $.each(facetGroup.Facets, function(innerindex, facet) {
                                if (facet.Selected) {
                                    hasSelected = true;
                                }
                            });
                            return hasSelected;
                        };

                    });
                } else {
                    model.facetGroups = {};
                }
            };

            model.facetCheckChanged = function (elem) {
                var facetValue = elem.data('facetvalue');
                // close all of the groups
                $.each(model.facetGroups, function (index, facetGroup) {
                    facetGroup.open = false;
                });
                // toggle the selected property of the clicked facet, if it's checked open the group that it's in
                $.each(model.facetGroups, function (index, facetGroup) {
                    var currentFacet = $.grep(facetGroup.Facets, function (f) { return f.Value === facetValue; });

                    if (currentFacet.length !== 0) {
                        currentFacet[0].Selected = !currentFacet[0].Selected;
                        facetGroup.open = currentFacet[0].Selected;
                        return false;
                    }
                    return true;
                });

                model.applySelectedFacets();
            };

            model.selectedFacets = function () {

                var selected = [];
                $.each(model.facetGroups, function (index, facetGroup) {
                    var selectedFacets = $.grep(facetGroup.Facets, function (e) { return e.Selected; });

                    var selectedFacetValues = $.map(selectedFacets, function (facet) {
                        return facet.Group + '|' + facet.Value;
                    });

                    selected.push.apply(selected, selectedFacetValues);
                });

                return selected.join(' ');
            };

            model.hasSelectedFacets = function() {
                return model.selectedFacets().length === 0 ? false : true;
            };

            model.setSelectedFacets = function (facets) {
                if (facets !== undefined && facets !== null) {
                    var ids = $.map(facets.split(' '), function (selection) {
                        var facetparts = selection.split('|');
                        return facetparts.length > 1 ? facetparts[1] : '';
                    });

                    $.each(model.facetGroups, function(index, facetGroup) {
                        $.each(facetGroup.Facets, function(innerindex, facet) {
                            var selectedFacetId = $.grep(ids, function(e) { return e === facet.Value; });
                            if (selectedFacetId.length > 0) {
                                facet.Selected = true;
                            }
                        });
                    });
                }
            };

            model.resetFacets = function () {
                $.each(model.facetGroups, function (index, facetGroup) {
                    facetGroup.open = false;
                    $.each(facetGroup.Facets, function (innerindex, facet) {
                        facet.Selected = false;
                    });
                });
                model.applySelectedFacets();
            };

            model.updateFacetCounts = function (searchResultFacetGroups) {
                $.each(model.facetGroups, function (index, facetGroup) {
                    $.each(facetGroup.Facets, function (innerindex, facet) {
                        facet.Count = searchResultFacetGroups[facet.Value] | 0;
                    });
                });
            };

            model.applySelectedFacets = function () {
                resetList();
                search();
            };

            return model;
        };

        // init (constructor) takes a config object and functions from the search model to reset the list and perform the search.
        var createFacetModel = function (config, resetList, search) {
            var model = new facetModel(config, resetList, search);
            model.setFacetGroups(config.AllowedFacets);
            return model;
        };

        return {
            Init: createFacetModel,
            FacetModel: facetModel
        };
    })();

    nmcom.search.SearchBox = (function () {
        var searchBoxModel = function(config) {
            var vm = {};
            var me = {}; // for private members

            me.defaults = {
                SearchHintApi: '/nmcomMvc/hint',
                DefaultText: 'Search',
                ButtonLabel: 'Search',
                MaxSearchHints: 7,
                MinSearchHintCharacters: 3,
                Delay: 200,
                Input: {
                    SearchInput: $(),
                    SearchButton: $()
                },
                Containers: {
                    SearchHintsContainer: $(),
                    SearchHintsResultsContainer: $(),
                    SearchContainer: $()
                },
                Selectors: {
                    resultsTemplate: ''
                }
            };

            me.adobeTracker = nmcom.AdobeTracker != undefined ? nmcom.AdobeTracker : undefined;

            me.Autocomplete = {
                term: '',
                request: null,
                cache: {}
            };

            var opts = $.extend(true, {}, me.defaults, config);
            vm.items = [];

            vm.keys = {
                UP: 38,
                DOWN: 40,
                ENTER: 13,
                TAB: 9
            };

            vm.searchBoxText = function (value) {
                if (value === undefined) {
                    return opts.Input.SearchInput.val();
                }

                return opts.Input.SearchInput.val(value);
            }

            vm.setup = function () {
                vm.hideHints();
                if (opts.Selectors.resultsTemplate.length) {
                    me.resultsTemplate = _.template($(opts.Selectors.resultsTemplate).html());
                }
            }

            vm.setupEvents = function () {
                if (opts.Input.SearchInput.on === undefined) {
                    opts.Input.SearchInput = $(opts.Selectors.searchInput);
                }
		
                vm.setupHintAutocomplete();
                var containerClass = '.' + opts.Containers.SearchContainer.attr('class');
                var clickEvent = 'click' + containerClass;
                // handle clicking out to close the hints
                $('html').on(clickEvent, function (e) {
                    var target = $(e.target);
                    if (target.parents(containerClass).length) {
                        return;
                    } else if (!vm.searchBoxText().length) {
                        vm.searchBoxText(opts.DefaultText);
                    }
                    vm.hideHints();
                });

                opts.Input.SearchInput.on('focus, click', function (e) {
                    me.removeWaterMarkText();
                });

                // arrow keys
                opts.Containers.SearchContainer.keyup(function (e) {

                    if (vm.searchBoxText().length < opts.MinSearchHintCharacters) {
                        vm.hideHints();
                        return;
                    }

                    var key = e.which;
                    if (key !== vm.keys.TAB && key !== vm.keys.DOWN && key !== vm.keys.UP) {
                        return;
                    }

                    var items = opts.Containers.SearchHintsResultsContainer.find('li');

                    if (!items.length) {
                        return;
                    }

                    var selected = items.filter('.selected:first');

                    var current;

                    if (key === vm.keys.TAB) {
                        // get the title of the selected item and set the value of the search box
                        if (selected.length) {
                            vm.disableAutocomplete();
                            vm.searchBoxText(selected.data('term'));
                            opts.Input.SearchInput.focus();
                            selected.removeClass('selected');
                            opts.Input.SearchButton.trigger('click');
                            vm.enableAutocomplete();
                        }
                    } else if (key === vm.keys.DOWN) {
                        if (!selected.length) { // arrow down from the textbox
                            current = items.eq(0);
                        } else if (items.index(selected) === (items.length - 1)) { // arrow down from the last item
                            current = null; // move focus to the textbox
                            opts.Input.SearchInput.focus();
                        } else {
                            current = $(items[items.index(selected) + 1]);
                        }
                    } else if (key === vm.keys.UP) {
                        if (!selected.length) // arrow up from the textbox
                        {
                            current = items.last();
                        } else if (items.index(selected) === 0) { // up arrow from the first item
                            current = null; // move focus to the textbox
                            opts.Input.SearchInput.focus();
                        } else {
                            current = $(items[items.index(selected) - 1]);
                        }
                    }

                    items.removeClass('selected');
                    if (current) {
                        current.addClass('selected');
                    }
                    e.preventDefault();
                });
                // enter key               
                opts.Input.SearchInput.keypress(function (e) {

                    if (e.which !== vm.keys.ENTER) {
                        return;
                    }

                    e.preventDefault();

                    var items = $('.search-hints ul li');
                    var selected = $();
                    if (items.length) {
                        selected = items.filter('.selected:first');
                    }

                    if (selected.length) {
                        vm.disableAutocomplete();
                        vm.searchBoxText(selected.data('term'));
                        opts.Input.SearchInput.focus();
                        selected.removeClass('selected');
                        opts.Input.SearchButton.trigger('click');
                        vm.enableAutocomplete();
                    } else {
                        opts.Input.SearchButton.trigger('click');
                    }
                });
            };

            vm.disableAutocomplete = function() {
                opts.Input.SearchInput.autocomplete({ disabled: true });
            };

            vm.enableAutocomplete = function() {
                opts.Input.SearchInput.autocomplete({ disabled: false });
            };

            vm.setupHintAutocomplete = function () {
                me.removeWaterMarkText();

                opts.Input.SearchInput.autocomplete({
                    source: function(request, response) {

                        var term = $.trim(request.term).toLowerCase();

                        if (!term.length) return;

                        vm.items.length = 0;
                        vm.cancelHints();
                        me.Autocomplete.term = term;

                        var termSuggestionCache = me.Autocomplete.cache[term];
                        if (termSuggestionCache && termSuggestionCache.length) {
                            vm.items.push.apply(vm.items, termSuggestionCache);
                            vm.render();
                            vm.hintClick();
                            return;
                        }

                        vm.getSearchHints(term);
                    },
                    minLength: opts.MinSearchHintCharacters,
                    delay: opts.Delay,
                    search: function(event, ui) {
                        var key = event.which;
                        if (key === vm.keys.DOWN || key === vm.keys.UP) {
                            return false;
                        }
                        return true;
                    }
                });
            };

            vm.prepareTemplates = function() {
                if (opts.Selectors.resultsTemplate.length) {
                    me.resultsTemplate = _.template($(opts.Selectors.resultsTemplate).html());
                }
            };
            vm.hintClick = function () {
                if (opts.Containers.SearchHintsContainer.length && opts.Containers.SearchHintsResultsContainer.length) {
                    var terms = opts.Containers.SearchHintsResultsContainer.find('li');
                    terms.on('click', vm.hintItemClicked);
                }
            };

            vm.hintItemClicked = function() {
                var selected = opts.Containers.SearchHintsResultsContainer.find('li').filter('.selected:first');

                vm.searchBoxText($(this).data('term'));
                vm.cancelHints();
                opts.Input.SearchInput.focus();
                selected.removeClass('selected');
                opts.Input.SearchButton.trigger('click');
            }

            vm.showHints = function () {
                if (opts.Containers.SearchHintsContainer.length) {
                    opts.Containers.SearchHintsContainer.show();
                }
            };

            vm.hideHints = function () {
                if (opts.Containers.SearchHintsContainer.length) {
                    opts.Containers.SearchHintsContainer.hide();

                    if (opts.Containers.SearchHintsResultsContainer.length) {
                        opts.Containers.SearchHintsResultsContainer.html('');
                    }
                }
            };

            vm.cancelHints = function () {
                if (me.Autocomplete.request && me.Autocomplete.request.readystate !== 4) {
                    me.Autocomplete.request.abort();
                }
            };

            vm.hintCache = function () {
                me.Autocomplete.cache[me.Autocomplete.term] = [];
                me.Autocomplete.cache[me.Autocomplete.term].push.apply(me.Autocomplete.cache[me.Autocomplete.term], vm.items);
            };

            vm.render = function () {
                vm.prepareTemplates();

                if (opts.Containers.SearchHintsContainer.length && opts.Containers.SearchHintsResultsContainer.length && me.resultsTemplate.length) {
                    if (vm.items.length) {
                        var resultContent = me.resultsTemplate({
                            suggestions: vm.items
                        });
                        opts.Containers.SearchHintsResultsContainer.html(resultContent);
                        vm.showHints();
                    } else {
                        vm.hideHints();
                    }
                }
            };

            vm.getSearchHints = function (term) {
                return vm.doSearch(term).then(vm.hintCache).then(vm.render).then(vm.hintClick);
            };

            vm.searchHintResult = function (data) {
                me.Autocomplete.request = null;
                vm.items.length = 0;
                vm.items.push.apply(vm.items, data.suggestions);
            };

            vm.searchHintError = function (jqXhr, status, error) {
                me.Autocomplete.request = null;
                if (status === 'abort') {
                    return;
                }

                window.console && console.log('error requesting search hints. error:');
                window.console && console.log(JSON.stringify(error, null, "\t"));

                var pageUrl = window.location.href;
                var errorMsg = {
                    scriptName: 'search.js',
                    message: 'error requesting search hints.  URL=' + pageUrl + ' STATUS=' + status + ' ERROR=' + error,
                    originUrl: pageUrl,
                    browser: window.navigator.userAgent,
                    lineNumber: 0,
                    level: 5
                };

                nmcom.errorHandler.logError(errorMsg);
            };

            vm.getSearchQuery = function (term) {
                var query = {
                    SearchTerm: term,
                    PageSize: opts.MaxSearchHints,
                    PageIndex: 0 // always the first page for hints
                };
                return query;
            };

            vm.doSearch = function (term) {
                var searchParams = vm.getSearchQuery(term);

                var request = nmcom.Services.Http.POST(
                        opts.SearchHintApi,
                        {},
                        JSON.stringify(searchParams)
                    );

                request.done(vm.searchHintResult);
                request.error(vm.searchHintError);

                me.Autocomplete.request = request;

                return request;
            };

            me.removeWaterMarkText = function () {
                if (vm.searchBoxText() === opts.DefaultText) {
                    vm.searchBoxText('');
                }
            };

            return vm;
        };

        var createSearchBoxModel = function (config) {
            var model = new searchBoxModel(config);
            model.setup();
            model.setupEvents();

            window.searchBoxModels = window.searchBoxModels || [];
            window.searchBoxModels.push(model);

            return model;
        };

        return {
            Init: createSearchBoxModel
        };    
    })();

})(window.jQuery, window.nmcom || (window.nmcom = {}));

jQuery(function() {
    nmcom.search.Initialize();
});