﻿;
(function ($, nmcom) {
    'use strict';
    nmcom.genCommentsForm = nmcom.genCommentsForm || {};

    $.extend(nmcom.genCommentsForm, {
        settings: {
        },

        init: function () {
            var self = this;

            self.setupPageEvents();

            self.setupValidation();
        },

        setupPageEvents: function () {

            $(document).ajaxStart(function () {
                $('#loading').css('display', 'inline-block');
                $('#general-comments-form input[type="submit"]').prop('disabled', true);
            }).ajaxStop(function () {
                $('#loading').hide();
                $('#general-comments-form input[type="submit"]').prop('disabled', false);
            });

            $('#general-comments-form').submit(nmcom.genCommentsForm.handleSubmit);

        },

        setupValidation: function () {
            nmcom.validation.setupValidate('#general-comments-form', {
                rules: {
                    'a1_name': {
                        required: true
                    },
                    'a2_address': {
                        required: false
                    },
                    'a3_city_state_zip': {
                        required: false
                    },
                    'a4_phone': {
                        required: false,
                        phoneWithArea: true
                    },
                    'a5_email': {
                        required: false,
                        validEmail: true
                    },
                    'a7_comments': {
                        required: true
                    }
                },
                messages: {
                    'a1_name': {
                        required: "Please enter your name."
                    },
                    'a7_comments': {
                        required: "Please enter a comment."
                    }
                }
            });
        },

        handleSubmit: function (e) {
            var self = nmcom.genCommentsForm;
            e.preventDefault();

            var $form = $(e.currentTarget);

            if (!$form.valid()) {
                return;
            }

            if (navigator != null) {
                $("input[name=a6_browser]").val(navigator.userAgent);
            }

            self.submitForm();

        },

        submitForm: function () {
            var self = nmcom.genCommentsForm;
            var $form = $('#general-comments-form');

            var xhr2 = !!(window.FormData && ("upload" in ($.ajaxSettings.xhr())));
            if (xhr2) { //this browser supports AJAX file uploading, do it!
                var actionUrl = $form.attr('action');
                var data = new FormData($form[0]);

                $.ajax({
                    url: actionUrl,
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    dataType: 'text',
                    success: self.formSubmitSuccess,
                    error: self.formSubmitError
                });
            } else { //this is an old browser that doesn't support ajax file uploading, so do the post to hidden iframe trick
                $('iframe').attr({
                    "name": "postResultsFrame"
                }).appendTo($form).hide();

                /*
                 * Tell the form to post to the hidden iframe
                 * remove the submit event handler so that a traditional form POST may occur
                 * submit the form
                 */
                $form.attr("target", "postResultsFrame").unbind('submit').submit();

                self.formSubmitSuccess();
                $('#loading').hide();
            }

            //track analytics
            nmcom.adobeDtmDirectCall("QuestionstoWebmaster");
        },

        formSubmitSuccess: function () {
            $('#general-comments-thankyou').show();
            $('#general-comments-form').hide();
        },

        formSubmitError: function (jqXhr, status, error) {
            var pageUrl = window.location.href;
            var errorMsg = {
                scriptName: 'general-comments-form-view.js',
                message: 'General comments form failed to submit.  URL=' + pageUrl + ' STATUS=' + status + ' ERROR=' + error,
                originUrl: pageUrl,
                browser: window.navigator.userAgent,
                lineNumber: 0,
                level: 3
            };

            nmcom.errorHandler.logError(errorMsg);
            $('#general-comments-error').show();
            $('#general-comments-form input[type="submit"]').prop('disabled', false);
        }

    }); //extend

})(window.jQuery, window.nmcom || (window.nmcom = {}));

jQuery(function () {
    nmcom.genCommentsForm.init();
});