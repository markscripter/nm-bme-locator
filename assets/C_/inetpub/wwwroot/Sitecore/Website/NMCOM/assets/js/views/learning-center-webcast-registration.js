﻿;
(function ($, nmcom) {
    'use strict';
    nmcom.learningcenter = nmcom.learningcenter || {};

    $.extend(nmcom.learningcenter, {
        initWebcast: function() {
            this.setupWebcastPageEvents();
            this.setupWebcastValidation();
            this.getPartnerRefParam();

            var model = new nmcom.asw.Init($('#webcast-registration-form'));
        },

        setupWebcastPageEvents: function() {
            var self = this;
            $('#webcast-registration-form').submit(self.handleWebcastRegistrationForm);
            $('#already-registered').click(self.alreadyRegisteredForWebcast);
            $('input[name="std1"]').click(self.toggleAgentName);

        },

        toggleAgentName: function() {
            if ($('input[name="std1"]:checked').val() === "No") {
                $('input[name="std2"]').parents('.form-item').hide();
            } else if ($('input[name="std1"]:checked').val() === "Yes") {
                $('input[name="std2"]').parents('.form-item').css('display', 'inline-block');
            }
        },

        getPartnerRefParam: function () {
            var partnerRefParam = nmcom.getQueryStringParam("partnerref");
            if (partnerRefParam.length) {
                $('input[name="partnerref"]').val(partnerRefParam);
            }
        },

        setupWebcastValidation: function() {
            nmcom.validation.setupValidate('#webcast-registration-form', {
                rules: {
                    'firstname': {
                        required: true
                    },
                    'lastname': {
                        required: true
                    },
                    'zip': {
                        required: true,
                        minlength: 5,
                        digits: true
                    },
                    'email': {
                        required: true,
                        email: true
                    },
                    'std1': {
                        required: true
                    },
                    'std2': {
                        required: {
                            depends: function() {
                                return $('input[name="std1"]:checked').val() === 'Yes';
                            }
                        }
                    }
                },
                messages: {
                    'firstname': {
                        required: "Please enter your first name."
                    },
                    'lastname': {
                        required: "Please enter your last name."
                    },
                    'zip': {
                        required: "Please enter a zip code.",
                        minlength: 'Zip code must be at least 5 digits long.',
                        digits: 'Zip code can only be digits.'
                    },
                    'email': {
                        required: "Please enter your email address.",
                        email: "Please enter a valid email address."
                    },
                    'std1': {
                        required: "Please indicate if you are currently working with a Northwestern Mutual financial representative."
                    },
                    'std2': {
                        required: "Please enter your financial professional's name."
                    }
                }
            });
        },

        handleWebcastRegistrationForm: function(e) {
            var $form = $(e.currentTarget);

            e.preventDefault();

            if (!$form.valid()) {
                return;
            }

            if (nmcom.learningcenter.upcoming === "true") { //register the user and say thanks
                /* 
                   * On24 doesn't support CORS, so we have to do a classic form submission
                   * but we can hide the response in a hidden iframe
                   * assume everything went according to plan
                   */
                $('iframe').attr({
                    "name": "postResultsFrame"
                }).appendTo($form).hide();

                $form.attr("target", "postResultsFrame");

                $('#upcoming-registration-thankyou').show();
                $form.hide();
            } else { //open the on-demand webcast for the user in a new window
                $form.attr("target", "_blank");
                $('#upcoming-registration-thankyou').show();
                $form.hide();
            }

            //track analytics
            nmcom.adobeDtmDirectCall("LearningCenterWebcastFormComplete");

            /*Let the traditional form post happen without further intervention*/
            $form.unbind('submit').submit();

        },

        alreadyRegisteredForWebcast: function() {
            var eventId = $('input[name="eventid"]').val();
            var sessionId = $('input[name="sessionid"]').val();
            var key = $('input[name="key"]').val();
            var url = "https://event.on24.com/eventRegistration/EventLobbyServlet?target=registration.jsp&eventid=" + eventId + "&sessionid=" + sessionId + "&key=" + key + "&sourcepage=register";
            window.open(url);
        }

    });

})(window.jQuery, window.nmcom || (window.nmcom = {}));

jQuery(function() {
    nmcom.learningcenter.initWebcast();
});