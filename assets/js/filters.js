﻿;

'use strict';

//allows filters to be seen
var menu_open = false;//dropdowns closed (false) or open(true), default is closed
var mobile_menu_open = false;//mobile menu closed (false) or open(true), default is closed
var mobile_fr_list_show = false;//mobile results list closed (false) or open (true), default is closed
var large = 900;//900px width
var width;
var list = $("#list");
$(document).ready(function () {
    width = $(window).width();//gets the width of the device to determine layout
    titleSize(width);
    
    //continuously watch the width of device to determine the layout
    $(window).resize(function () {
        width = $(window).width();
        titleSize(width);
        //reset mobile variables to their defaults
        mobile_menu_open = false;
        mobile_fr_list_show = false;
        $("#map").css('height', '100%');
        if (width < large) {
            $("#mobile-menu-container").show();
            list.hide();
            $("#search").hide();
            $("#show-fr").text("Open Expert List");
        }
        else {
            $(".mobile-hidden").show();
            $(".agent-image").show();
            $("#mobile-menu-container").hide();
            list.show();
            $("#search").show();
            $("#rank-joint-work-wrapper").show();
            $("#show-fr").text("Open Expert List");
        }
    });
    //Slider Bar, display the percentage while sliding
    var range = $('#slider-percentage');
    var value = $('#percentage');
    //Necesarry for IE
    range.on('change',function(){
        value.html("Top " + range.attr('value') + "%");
        value.html("Top " + this.value + "%");
        
    });
    //Necessary for most browsers and mobile
    range.on('input',function () {
        value.html("Top " + range.attr('value') + "%");
        value.html("Top " + this.value + "%");
    });
    //hides the mobile menu if the map is touched in mobile
    $("#map").on("touch click", function () {
        if (width < large) {
            $("#mobile-menu-container").show();
            $("#search").hide();
        }
    });
    //print results of the list in a new window
    $("#print").on("touch click", function () {
        var newWindow = window.open('', '');
        $(".agent-image").css('display', 'none');
        $(".agent-information").css('line-height', '0');
        var printReport = document.getElementById('list').innerHTML;
        newWindow.document.body.innerHTML = printReport.fontsize(.5);
        newWindow.print();
        newWindow.close();
        $(".agent-image").css('display', 'block');
        return false;
    });
    //decides whether to show results in mobile
    $("#show-fr").on('touch click', function () {
        if (mobile_fr_list_show == false) {
            $("#map").css('height', '70%');
            $("#list").css('display', 'block');
            $("#show-fr").text("Close Expert List");
            mobile_fr_list_show = true;
        }
        else {
            $("#map").css('height', '100%');
            $("#list").css('display', 'none');
            $("#show-fr").text("Open Expert List");
            mobile_fr_list_show = false;
        }
    });
    //when touch on the results list, mobile menu will disappear
    $("#list ul").on("touch click", function () {
        var width = $(window).width();
        if (width < large) {
            mobile_menu_open = false;
            $("#mobile-menu-container").css('margin-left', 0);
            $("#mobile-menu-container").show();
            $("#search").hide();
        }
    });
    //mobile menu
    $('#mobile-menu-container').on('touch click', function () {
        $("#mobile-menu-container").hide();

        $("#search").css('visibility', 'visible');
        $("#search").show("slow");

        mobile_menu_open = true;
    });

    $('.menu > li').bind('mouseover', openSubMenu);
    $('.menu > li').bind('mouseout', closeSubMenu);

    function openSubMenu() {
        $(this).find('ul').css('display', 'inline');
    };
    function closeSubMenu() {
        $(this).find('ul').css('display', 'none');
    };
    //Refreshes the page
    $("#clear").on("touch click",function(){
        location.reload();
    });
    //for clicking/touching for menues in mobile
    $('.title').on("touch click", function () {
        if (menu_open == true) {
            $('.menu').bind('touch click', closeSubMenu);
            menu_open = false;
        }
        else {
            $('.menu').bind('touch click', openSubMenu);
            menu_open = true;
        }
    });
    //close mobile menu
    $('#close-mobile').on('touch click', function () {
        mobile_menu_open = false;
        $("#mobile-menu-container").css('margin-left', 0);
        $("#mobile-menu-container").show();
        $("#search").hide();
    });
    //display more information on the specialist when selected in mobile
    $("ul").on("click", "li", function () {
        if (width < large) {
            $("#list ul li").each(function () {
                if ($(this).hasClass("active")) {
                    $(".agent-image", this).toggle();
                    $(".mobile-hidden", this).toggle();
                }
                else {
                     $(".mobile-hidden",this).hide();
                     $(".agent-image",this).hide();
                }
            });
        }
    });
    //adjust the height of the filter titles depending on device width
    function titleSize(width) {
        if (width < large) {
            $("#business-market").text("Bus. Market Experts");
            $(".title").css('height', '30px');
        }
        else {
            $("#business-market").text("Business Market Experts");
            $(".title").css('height', '35px');
        }
    }

});
