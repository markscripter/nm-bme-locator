;

'use strict';

var FilterModel = Backbone.Model.extend({

    defaults: {
        id: '',
        name: '',
        labelFor: '',
        label: '',
        status: 'unchecked'
    },

    toggleStatus: function () {
        if (this.get('name') != 'Coli Boli') {
            if (this.get('status') == 'unchecked') {
                this.set({ 'status': 'checked' });
                this.trigger('filter:checked', this);
            } else {
                this.set({ 'status': 'unchecked' });
                this.trigger('filter:unchecked', this);
            }
        }
    }

});
