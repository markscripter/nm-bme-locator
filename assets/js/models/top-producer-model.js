;

'use strict';

var TopProducerModel = Backbone.Model.extend({

   defaults: {
      percent: ''
   },

   toggleStatus: function (percent) {
         this.set({ 'percent':  percent});
   }


});
