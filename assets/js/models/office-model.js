;

'use strict';

var OfficeModel = Backbone.Model.extend({

    // Gets called when you click "Show Office"
    select: function () {

           this.set({ selected: true });
           this.trigger('selected', this);

       }


});
