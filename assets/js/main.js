;
// https://stackoverflow.com/questions/5020479/what-advantages-does-using-functionwindow-document-undefined-windo
(function ($, window, document, interactivemap, undefined) {

    'use strict';

    interactivemap.Services = interactivemap.Services || {};

    //#region Http Service
    interactivemap.Services.Http = (function () {
        var post = function (url, header, data) {
            /// <summary>Perform a generic AJAX POST</summary>
            /// <param name="url" type="Object"></param>
            /// <param name="header" type="Object"></param>
            /// <param name="data" type="Object"></param>
            return $.ajax({
                type: 'POST',
                dataType: 'json',
                headers: header,
                url: url,
                contentType: 'application/json; charset=utf-8',
                data: data
            });
        };
        var get = function (url, header, data) {
            /// <summary>Perform a generic AJAX GET</summary>
            /// <param name="url" type="Object"></param>
            /// <param name="header" type="Object"></param>
            /// <param name="data" type="Object"></param>
            return $.ajax({
                type: 'GET',
                dataType: 'json',
                headers: header,
                url: url,
                contentType: 'application/json; charset=utf-8',
                data: data
            });
        };
        var getTextWithCredentials = function (url) {
            /// <summary>Perform a generic AJAX GET with xjrFields withCredentials = true</summary>
            /// <param name="url" type="Object"></param>
            return $.ajax({
                type: 'GET',
                url: url,
                cache: false,
                dataType: 'text',
                xhrFields: {
                    withCredentials: true
                }
            });
        };
        var html = function (url) {
            /// <summary>Get an html response</summary>
            /// <param name="url" type="Object"></param>
            return $.ajax({
                type: 'GET',
                url: url,
                contentType: 'html'
            });
        };
        var script = function (url) {
            /// <summary>Perform a AJAX GET for javascript</summary>
            /// <param name="url" type="Object"></param>
            return $.getScript(url);
        };
        return {
            POST: post,
            GET: get,
            HTML: html,
            SCRIPT: script,
            GETTEXTWITHCREDENTIALS: getTextWithCredentials
        };
    })();
   //#endregion
   $.extend(interactivemap, {
      settings: {
         adobeTracker: window._satellite !== undefined ? window._satellite: undefined
      },
      Spinner: {
         opts: {
            lines: 11, // The number of lines to draw

            length: 24, // The length of each line

            width: 14, // The line thickness

            radius: 42, // The radius of the inner circle

            scale: 0.50, // Scales overall size of the spinner

            corners: 1, // Corner roundness (0..1)

            color: '#000', // #rgb or #rrggbb or array of colors

            opacity: 0, // Opacity of the lines

            rotate: 0, // The rotation offset

            direction: 1, // 1: clockwise, -1: counterclockwise

            speed: 1, // Rounds per second

            trail: 60, // Afterglow percentage

            fps: 20, // Frames per second when using setTimeout() as a fallback for CSS

            zIndex: 2e9, // The z-index (defaults to 2000000000)

            className: 'spinner', // The CSS class to assign to the spinner

            top: '50%', // Top position relative to parent

            left: '50%', // Left position relative to parent

            shadow: false, // Whether to render a shadow

            hwaccel: false, // Whether to use hardware acceleration

            position: 'absolute' // Element positioning
         }
      },

      adobeDtmDirectCall: function (directCallRule) {
         if (interactivemap.settings.adobeTracker !== undefined && interactivemap.settings.adobeTracker.track !== undefined) {
            try {
               interactivemap.settings.adobeTracker.track(directCallRule);
            } catch (e) {
               var errorMsg = {
                  scriptName: 'main.js',
                  message: '_satellite: Failed to call .track() method with this rule: ' + directCallRule,
                  originUrl: window.location.href,
                  browser: window.navigator.userAgent,
                  lineNumber: 479,
                  level: 2
               };
            }
         }
      },
   });



   var mediator = new Backbone.Controller(),
      officeCollection = new OfficeCollection(),
      productFilterCollection = new ProductFilterCollection(),
      marketFilterCollection = new MarketFilterCollection(),
      searchView = new SearchView({
         mediator: mediator,
         collection: officeCollection
      }),
      officeListView = new OfficeListView({
         mediator: mediator,
         collection: officeCollection
      }),
      productFilterListView = new ProductFilterListView({
         mediator: mediator,
         collection: productFilterCollection
      }),
      marketFilterListView = new MarketFilterListView({
         mediator: mediator,
         collection: marketFilterCollection
      }),
      resultsPanelView = new ResultsPanelView({
         mediator: mediator,
         collection: officeCollection
      }),
      topProducerView = new TopProducerView({
         mediator: mediator,
         model: new TopProducerModel()
      });

   $('#main-container').find('#list').html(officeListView.$el);
   $('#main-container').find('#product-filter-list').html(productFilterListView.$el);
   $('#main-container').find('#market-filter-list').html(marketFilterListView.$el);

   productFilterCollection.fetch();
   marketFilterCollection.fetch();

   interactivemap.MapController = new MapController(
   {
      mediator: mediator,
      collection: officeCollection
   });

   interactivemap.Spinner = new Spinner(interactivemap.Spinner.opts).spin(document.getElementById('loading'));

})(window.jQuery, window, document, window.interactivemap || (window.interactivemap = {}));
