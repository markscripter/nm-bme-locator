;

'use strict';

var OfficeCollection = Backbone.Collection.extend({

    model: OfficeModel,

    initialize:function(){
        console.log(1234);
    },

    // url: 'api/Entity/GetQuiltEntities',
    url: 'assets/js/api-office.json',

    selected: null,

       initialize: function () {

           this.on('selected', function (model) { //propogated from model

               if (this.selected === model) {
                   return;
               }

               if (this.selected) {
                   this.selected.set({ selected: false });
               }

               this.selected = model;


           });

       },

       select: function (model) {

           if (model) {

               model.select();

           }

           return this;

       }

});
