/**
 * backbone-controller 1.0.0
 * 
 * Copyright (c) 2014 Ezekiel Chentnik
 * Licensed under the MIT @license.
 * 
 * A multi-purpose object to use as a controller 
 * and as a mediator for coordination of other objects, views, etc.
 * 
 * @requires Backbone
 * @requires underscore
 * 
 */

;

'use strict';

(function (factory) {

    if (typeof define === 'function' && define.amd) {
        define(['backbone', 'underscore'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('backbone'), require('underscore'));
    } else {
        factory(window.Backbone, window._);
    }

})(function(Backbone, _) {

    /**
     * @class Backbone.Controller
     * @constructor
     * @extends Backbone
     */

    Backbone.Controller = function (options) {
        options = options || {};
        this.initialize.apply(this, arguments);
    };

    _.extend(Backbone.Controller.prototype, Backbone.Events, {
        initialize: function () { },
        destroy: function () {

            this.stopListening();
            this.off();
            return this;

        }
    });

    Backbone.Controller.extend = Backbone.Router.extend;

});
