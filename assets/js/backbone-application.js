
/**
 * backbone-application 1.0.0
 * 
 * Copyright (c) 2014 Ezekiel Chentnik
 * Licensed under the MIT @license.
 * 
 * a nice object to kick off an application and all of its processes using initializers,
 * helping to organize and avoid bloated init functions
 * 
 * @requires jQuery
 * @requires Backbone
 * @requires underscore
 * 
 */

;

'use strict';

(function (factory) {

    if (typeof define === 'function' && define.amd) {
        define(['backbone', 'underscore', 'jquery'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('backbone'), require('underscore'), require('jquery'));
    } else {
        factory(window.Backbone, window._, window.jQuery);
    }

})(function (Backbone, _, $) {


    /**
     * @class Backbone.Application
     * @constructor
     * @extends Backbone
     */

    Backbone.Application = function (options) {

        options = options || {};
        this.initialize.apply(this, arguments);

    };

    _.extend(Backbone.Application.prototype, Backbone.Events, {

        initialize: function () { },

        start: function (options) {

            this.trigger('start', options);

        }

    });

    Backbone.Application.extend = Backbone.Model.extend;

});
