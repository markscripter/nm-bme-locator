﻿;
(function ($, nmcom) {
    nmcom.validation = nmcom.validation || {};

    $.extend(nmcom.validation, {
        messages: {
            validEmail: 'Please enter a valid email.',
            zipCodeDigits: 'Please enter a zip code only containing digits',
            zipWithCode: 'Please enter extended zip code correctly. Ex. 12345-1234',
            phoneWithExt: 'Please enter phone number with area code. Ex: 414-555-1234',
            fileSize: 'Please choose a smaller file.'
        },

        rules: {

        },

        restrictionPatterns: {

        },

        methods: {
            validEmail: function(value, element) { //checks for a valid email.  Framework OOTB doesn't require top-level domain, this pattern does.
                return this.optional(element) || /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)\b/i.test(value);
            },

            zipCodeDigits: function(value, element) {
                return this.optional(element) || /^\d{5}/.test(value);
            },

            zipWithCode: function(value, element) { //adds support for extensions
                return this.optional(element) || /^\d{5}(?:[-\s]\d{4})?$/.test(value);
            },

            phoneWithExt: function(value, element) { //adds support for extensions
                return this.optional(element) || /^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})\s*-?[2-9]\d{2}-?\d{4}(\s*x\s*\d{1,4})?$/.test(value);
            },

            fileSize: function(value, element, param) {
                var fileApiSupport = !!(window.File && window.FileReader && window.FileList && window.Blob);
                return (fileApiSupport) ? (this.optional(element) || (element.files[0].size <= param)) : true;
            }
        },

        setupValidate: function(selector, options) {

            // setup app defaults here if different than base framework
            var defaults = {

                /* allow for cascading style from a form element container (.form-item) */
                highlight: function(element) {
                    $(element).parent().addClass('error');
                },
                unhighlight: function(element) {
                    $(element).parent().removeClass('error');
                },
                /* moving where radio errors are displayed so don't insert in between labels and throw off layout */
                errorPlacement: function(error, element) {
                    if (element.attr('type') === 'radio') {
                        //error.insertBefore(element);
                        error.appendTo($(element).parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            };

            var optionsOverride = $.extend(defaults, options);
            $(selector).validate(optionsOverride);

        },

        addCustomValidationMethods: function() {
            if ($.validator !== undefined)
            {
                $.validator.addMethod('validEmail', nmcom.validation.methods.validEmail, nmcom.validation.messages.validEmail);
                $.validator.addMethod('zipCodeDigits', nmcom.validation.methods.zipCodeDigits, nmcom.validation.messages.zipCodeDigits);
                $.validator.addMethod('zipWithCode', nmcom.validation.methods.zipWithCode, nmcom.validation.messages.zipWithCode);
                $.validator.addMethod('phoneWithExt', nmcom.validation.methods.phoneWithExt, nmcom.validation.messages.phoneWithExt);
                $.validator.addMethod('fileSize', nmcom.validation.methods.fileSize, nmcom.validation.messages.fileSize);
            }
        }
    });

    nmcom.validation.addCustomValidationMethods();

})(window.jQuery, window.nmcom || (window.nmcom = {}));