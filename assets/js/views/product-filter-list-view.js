;

'use strict';
var products_count = 0;
var ProductFilterListView = Backbone.View.extend({
   tagName: 'ul',
   className: 'sub-menu',

   itemView: FilterItemView,

   settings: {
      marketFilterCount: 0,
      productFilterCount: 0
   },

   initialize: function(options) {
      var self = this;

      self.mediator = options.mediator;

      self.listenTo(this.collection, 'sync', self.render);

      self.mediator.on('market-filter-list:checked', self.incrementMarketFilterCount, self);
      self.mediator.on('market-filter-list:unchecked', self.decrementMarketFilterCount, self);

      // triggered when an input is checked
      self.listenTo(this.collection, 'filter:checked', function (model) {
         self.settings.productFilterCount++;

          self.mediator.trigger('product-filter-list:checked', model);
          self.mediator.trigger('product-filter-list:hideOrShowTopProduction', self.settings.productFilterCount, self.settings.marketFilterCount);
      });

      // triggered when an input is unchecked
      self.listenTo(this.collection, 'filter:unchecked', function (model) {
         self.settings.productFilterCount--;

          self.mediator.trigger('product-filter-list:unchecked', model);
          self.mediator.trigger('product-filter-list:hideOrShowTopProduction', self.settings.productFilterCount, self.settings.marketFilterCount);
      });

   },

   incrementMarketFilterCount: function() {
      var self = this;

      self.settings.marketFilterCount++;
   },

   decrementMarketFilterCount: function() {
      var self = this;

      self.settings.marketFilterCount--;
   },

   render: function() {

      var self = this,
         fragment = document.createDocumentFragment();

      self.collection.each(function(model) {


         var item = new self.itemView({ model: model, mediator: self.mediator });

         fragment.appendChild(item.el);

      });

      self.$el.html(fragment);
   },

});

