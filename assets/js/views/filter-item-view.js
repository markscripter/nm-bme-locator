;

'use strict';

var FilterItemView = BaseItemView.extend({
      tagName: 'li',

      events: {
         'change input': 'toggleStatus'
      },

    initialize: function(options){

        var self = this;
        self.mediator = options.mediator;//todo: buble up event to parent, let parent communicate with mediator
        self.template = $('#filter-item-view').html(); // gets used by base-item-view.js
        this.render();
    },

    serialize: function(){
        return this.model.toJSON();
    },

   toggleStatus: function() {
      this.model.toggleStatus();
   }

});
