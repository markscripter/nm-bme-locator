;

'use strict';

var SearchView = Backbone.View.extend({
   el: '#search',

   events: {
      'click #btnSearch': 'submit'
   },

   searchParameters: {
      filters: new Array(),
      topProducerValue: '0',
      points: new Array(),
      showApproxMiles: true
   },


   initialize: function(options) {

      var self = this;
      self.mediator = options.mediator;
      self.collection = options.collection;

      self.mediator.on('product-filter-list:checked', self.addToFilters, self);
      self.mediator.on('product-filter-list:unchecked', self.removeFromFilters, self);

      self.mediator.on('market-filter-list:checked', self.addToFilters, self);
      self.mediator.on('market-filter-list:unchecked', self.removeFromFilters, self);

      self.mediator.on('top-producer:slider', self.setTopProducer, self);

      self.mediator.on('map:setAndSearchMapPoints', self.setPoints, self);
      self.mediator.on('list:stopSpinner', self.stopSpinner, self);
   },


   setTopProducer: function (topProducerValue) {
      var self = this;
      self.searchParameters.topProducerValue = topProducerValue;
      self.submit();
   },

   addToFilters: function(filter) {
      var self = this;
      self.searchParameters.filters.push(filter);
      self.submit();
   },

   setPoints:function(points, showApproxMiles) {
      var self = this;

      self.searchParameters.showApproxMiles = showApproxMiles;
      self.searchParameters.points = points;
      self.setFetchParameters();
   },

   removeFromFilters: function(filter) {
      var self = this;
      self.searchParameters.filters = _.without(self.searchParameters.filters, _.findWhere(self.searchParameters.filters, { id: filter.id }));
      self.submit();
   },

   setFetchParameters: function () {
      var self = this;

      var specialtiesList = self.getFilterNames();

      var nameNOAgentNumber = $('#fr-no-other-search').val();

      var points = self.searchParameters.points;
      var topProducerValue;

      topProducerValue = self.searchParameters.topProducerValue;

      self.startSpinner();      

      self.fetchData(nameNOAgentNumber, specialtiesList, topProducerValue, points, self.searchParameters.showApproxMiles);
   },

   submit: function(e) {
      var self = this;

      if (e) {
         e.preventDefault();
      }

      var address = $('#address-search').val();

      if (address) {
         self.mediator.trigger('search:codeAddress', address);
         return; // exit early to get lat lng from address entered
      } else {
         self.mediator.trigger('search:tryToSearchByGeolocation');
         return; // exit early to geolocate search if nothing entered in the search boxes
      }

   },

   fetchData: function(nameNOAgentNumber, specialtiesList, topProducerValue, points, showApproxMiles) {
      this.collection.fetch({
         data: {
            'nameNOAgentNumber': nameNOAgentNumber,
            'specialties': specialtiesList,
            'topProducerValue': topProducerValue,
            'points': points,
            'showApproxMiles': showApproxMiles
         },
         success: function (data) {
            
         },
         error: function (error) {
            self.stopSpinner();
         }
      });

      //track analytics
      interactivemap.adobeDtmDirectCall("ExpertLocatorSearchByClickingEnter");

   },

   getFilterNames: function() {
      var specialties = '';

      _.each(this.searchParameters.filters, function (model) {
         specialties = specialties + model.get('name') + ',';
      });

      return specialties;
   },

   startSpinner: function() {
      $('#loading').show();
   },

   stopSpinner: function () {
      $('#loading').hide();
   }


});
