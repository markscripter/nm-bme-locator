;

'use strict';

MapController = Backbone.Controller.extend({
      officeInfoWindow: null,

      settings: {
         //the following are used for bounding (google.maps.LatLngBounds) the visible map
         minLat: 90, //north pole
         maxLat: 0, //equator
         minLng: 0, //prime meridian
         maxLng: -180, //international date line
         geoLocationAccuracyThreshold: 80467, // 50 miles
         pinRepIconURL: window.location.pathname.replace(/^\/([^\/]*).*$/, '$1') + 'assets/img/icons/pinrep.png',
         pinRepIconHighlightedURL: window.location.pathname.replace(/^\/([^\/]*).*$/, '$1') + 'assets/img/icons/pinreph.png',
         pinRepsIconURL: window.location.pathname.replace(/^\/([^\/]*).*$/, '$1') + 'assets/img/icons/pinreps.png',
         pinRepshIconHighlightedURL: window.location.pathname.replace(/^\/([^\/]*).*$/, '$1') + 'assets/img/icons/pinrepsh.png'
      },

      locationTypes: {
// these are what googles API return when you geocode an Address
         streetAddress: 'street_address', // Zoom: 16-18 | Street Address
         neighborhood: 'neighborhood', // Zoom: 15    | "Downtown"
         adminAreaLevelThree: 'administrative_area_level_3', // Zoom: 15    | Township
         postalCode: 'postal_code', // Zoom: 12-14 | City, State, Postal Code
         locality: 'locality', // Zoom: 10-11 | City, State
         adminAreaLevelTwo: 'administrative_area_level_2', // Zoom: 8-9   | County
         adminAreaLevelOne: 'administrative_area_level_1', // Zoom: 6-7   | State
         political: 'political', // Zoom: 1-5   | City/County, State (only type)
         country: 'country' // Zoom: 1-5   | Country
      },

      mapObjects: {
         mapInstance: null,
         oms: null, // Overlapping Marker Spiderfier object
         markers: [], // list of google markers
         markersCurrentlySpiderfied: [], // markers that are currently spiderfied on the map
         // List of markers when using clustering.
         // Needed because the markers array uses the ID of a model as its index and doesn't work with clustering.
         markerClustererArray: [],
         markerClusterer: null, // marker clusterer object
         geocoderInstance: null,
         youAreHerePin: null,
         activeMarker: null // active marker used for marker highlighting
      },

      searchParameters: {
         bounds: null, // bounds of viewport
         lat: 0, // lat for youAreHerePin
         lng: 0, // lng for youAreHerePin
         showApproxMiles: true
      },

      getMapStyles: function() {

         //https://snazzymaps.com/style/15/subtle-grayscale
         var styles = [
            {
               "featureType": "landscape",
               "stylers": [
                  {
                     "saturation": -100
                  }, {
                     "lightness": 65
                  }, {
                     "visibility": "on"
                  }
               ]
            }, {
               "featureType": "poi",
               "stylers": [
                  {
                     "saturation": -100
                  }, {
                     "lightness": 51
                  }, {
                     "visibility": "simplified"
                  }
               ]
            }, {
               "featureType": "road.highway",
               "stylers": [
                  {
                     "saturation": -100
                  }, {
                     "visibility": "simplified"
                  }
               ]
            }, {
               "featureType": "road.arterial",
               "stylers": [
                  {
                     "saturation": -100
                  }, {
                     "lightness": 30
                  }, {
                     "visibility": "on"
                  }
               ]
            }, {
               "featureType": "road.local",
               "stylers": [
                  {
                     "saturation": -100
                  }, {
                     "lightness": 40
                  }, {
                     "visibility": "on"
                  }
               ]
            }, {
               "featureType": "transit",
               "stylers": [
                  {
                     "saturation": -100
                  }, {
                     "visibility": "simplified"
                  }
               ]
            }, {
               "featureType": "administrative.province",
               "stylers": [
                  {
                     "visibility": "off"
                  }
               ]
            }, {
               "featureType": "water",
               "elementType": "labels",
               "stylers": [
                  {
                     "visibility": "on"
                  }, {
                     "lightness": -25
                  }, {
                     "saturation": -100
                  }
               ]
            }, {
               "featureType": "water",
               "elementType": "geometry",
               "stylers": [
                  {
                     "hue": "#ffff00"
                  }, {
                     "lightness": -25
                  }, {
                     "saturation": -97
                  }
               ]
            }
         ];

         return styles;
      },


      initialize: function(options) {

         var self = this;
         self.mediator = options.mediator;
         self.collection = options.collection;

         var mapsUrl = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDkFNUY_U6aWKoRYs9S9Typp7iueFMuHsU&v=3.20&sensor=false&callback=interactivemap.MapController.googleApiLoaded";
         interactivemap.Services.Http.SCRIPT(mapsUrl);

         self.mediator.on('list:addMarker', self.addOfficeMarker, self)
            .on('list:deleteMarkers', self.deleteMarkers, self)
            .on('search:clearMarkers', self.clearMarkers, self)
            .on('office:officeItemClick', self.highlightListItemAndMarker, self)
            .on('list:showMarkers', self.showMarkers, self)
            .on('list:centerMapOverMarkers', self.centerMapOverMarkers, self)
            .on('search:codeAddress', self.codeAddress, self)
            .on('search:tryToSearchByGeolocation', self.geolocationSearch, self)
            .on('list:cluster', self.cluster, self)
            .on('list:changeIconsForMarkersNearAnyOtherMarker', self.changeIconsForMarkersNearAnyOtherMarker, self)
      },

      googleApiLoaded: function() {
         var self = this;

         self.mapObjects.geocoderInstance = new google.maps.Geocoder(); // create geocoder instance
         var geolocationPosition;

         // initialize google maps with geolocation
         //https://stackoverflow.com/questions/25964700/how-can-i-use-a-deferred-object-to-retrieve-a-longitude-and-latitude-with-the-ht
         self.getGeolocation().then(function (position) {
            geolocationPosition = { 'lat': position.coords.latitude, 'lng': position.coords.longitude}
            self.createGoogleMapInstance(geolocationPosition, 11) // if geolocaton successful send lat lng to new google map instance
         }).fail(function (err) {
          //  self.getMaxMindLocation().then(function (position) {
            //   geolocationPosition = { 'lat': position.location.latitude, 'lng': position.location.longitude }
            //   self.createGoogleMapInstance(geolocationPosition, 11) // if geolocaton failed, but maxmind successful send lat lng to new google map instance
          //  }).fail(function (err) {
               geolocationPosition = { 'lat': 37.6, 'lng': -95.665 }
               self.createGoogleMapInstance(geolocationPosition, 3) // if geolocation and maxmind failed, send center of USA to new google map instance
           // });
         });
      },

      // After we get the users geolocation we can initialize google maps with their location
      createGoogleMapInstance: function (geolocationPosition, zoomLevel) {
         var self = this;

         if (!geolocationPosition) {
            geolocationPosition = { 'lat': 37.6, 'lng': -95.665 };
         }
         var mapOptions = {
            center: {
               lat: geolocationPosition.lat,
               lng: geolocationPosition.lng
            },
            zoom: zoomLevel,
            //styles: self.GETMAPSTYLES()
         };

         self.mapObjects.mapInstance = new google.maps.Map(document.getElementById('map'), mapOptions); // create map instance

         self.setGoogleMapEventListeners();
      },

   // set events listeners for google maps after we initialize google maps
   setGoogleMapEventListeners: function () {
      var self = this;

      // when the map gets zoomed in or out we need to change the icons for markers that may or may not be spiderfied
      google.maps.event.addListener(self.mapObjects.mapInstance, 'idle', function(event) {
         self.resetMarkerHighlighting();
      });

      // Some of spiderfies methods rely on the google maps Projection object being available, and thus cannot be called until the map�s first idle event fires.
      // Therefore, we need to wait to load spiderfy until tilesloaded is called.
      google.maps.event.addListenerOnce(self.mapObjects.mapInstance, 'tilesloaded', function (event) {

         jQuery.getScript("assets/js/vendor/overlapping-marker-spiderfier.min.js", function(data, status, jqxhr) {

            //https://github.com/jawj/OverlappingMarkerSpiderfier
            // If you know that you won�t be moving and/or hiding any of the markers you add to this instance,
            // you can save memory (a closure per marker in each case) by setting the options named
            // markersWontMove and/or markersWontHide to true.
            self.mapObjects.oms = new OverlappingMarkerSpiderfier(self.mapObjects.mapInstance, { markersWontMove: true, markersWontHide: true, keepSpiderfied: true, circleSpiralSwitchover: Infinity, nearbyDistance: 10 });
            self.setSpiderfyEventListeners();

            // Try to search using geolocation
            self.geolocationSearch();
         });
      });
   },

   setSpiderfyEventListeners: function () {
      var self = this;
         // event triggered if you click on a marker that has been spiderfied
      self.mapObjects.oms.addListener('click', function(marker, event) {
         self.spiderfyMarkerClick(marker, event)
      });

         // event triggered if you click on a marker that spiderfies
         self.mapObjects.oms.addListener('spiderfy', function(markers) {
            self.spiderfyChangePinIcon(markers);
         });

      // event triggered if you unspiderfy markers
      self.mapObjects.oms.addListener('unspiderfy', function (markers) {
         self.unspiderfyChangePinIcon(markers);
      });
   },

   // search the map using your geolocation position.
   // we want to focus the map around the users location to show the agents closest to them.
   geolocationSearch: function () {
      var self = this;

      self.getGeolocation().then(function (position) {
         var geolocationPosition = { 'lat': position.coords.latitude, 'lng': position.coords.longitude }
         self.searchParameters.showApproxMiles = true;
         self.setGeolocationSearchParameters(geolocationPosition);
         self.distanceFromYouAreHerePinSearch();

      }).fail(function (err) {
       //  self.getMaxMindLocation().then(function(position) {
       //     var geolocationPosition = { 'lat': position.location.latitude, 'lng': position.location.longitude }
      //      self.searchParameters.showApproxMiles = true;
      //      self.setGeolocationSearchParameters(geolocationPosition);
      //      self.distanceFromYouAreHerePinSearch();
      //   }).fail(function(err) {
            self.searchParameters.showApproxMiles = false;
            self.mapObjects.mapInstance.setCenter(new google.maps.LatLng(37.6, -95.665)); // center of the USA
            self.mapObjects.mapInstance.setZoom(3);
            self.viewportSearch();
    //     });
      });
   },

   getGeolocation: function () {
      var self = this;
      var deferred = $.Deferred();

      if (navigator.geolocation) {
         var geolocationOptions = {
            timeout: 10 * 1000,
            enableHighAccuracy: true
         }
         // geo location is supported. Call navigator.geolocation.getCurrentPosition and :
         // - resolve the promise with the returned Position object, or
         // - reject the promise with the returned PositionError object, or
         navigator.geolocation.getCurrentPosition(deferred.resolve, deferred.reject, geolocationOptions);
      }
      return deferred.promise();
   },

   getMaxMindLocation: function () {
      var self = this;
      var deferred = $.Deferred();
      // - resolve the promise with the returned Position object, or
      // - reject the promise with the returned PositionError object, or
      geoip2.city(deferred.resolve, deferred.reject);

      return deferred.promise();
   },
  
   setGeolocationSearchParameters: function (position) {
      var self = this;

      self.setYouAreHerePin(position.lat, position.lng); // add geolocation pin to map
      var address = $('#address-search').val();
      var nameOrNO = $('#fr-no-other-search').val();

      if (!nameOrNO && !address) {
         self.mapObjects.mapInstance.setZoom(11);
         self.mapObjects.mapInstance.panTo(new google.maps.LatLng(position.lat, position.lng));
      }
   },


   // finds lat/lng for an address passed as a parameter
   codeAddress: function (address) {
      var self = this;
      self.mapObjects.geocoderInstance.geocode({ 'address': address }, function (results, status) {
         if (status == google.maps.GeocoderStatus.OK) {
            var result = results[0];

            if (result.types.indexOf(self.locationTypes.streetAddress) >= 0) {
               self.mapObjects.mapInstance.setZoom(16); // street level
            }
            else if (result.types.indexOf(self.locationTypes.locality) >= 0) {
               self.mapObjects.mapInstance.setZoom(10); // city level
            }
            else if (result.types.indexOf(self.locationTypes.adminAreaLevelOne) >= 0) {
               self.mapObjects.mapInstance.setZoom(6); // state level
            }
            else if (result.types.indexOf(self.locationTypes.country) >= 0) {
               self.mapObjects.mapInstance.setZoom(1); // country level
            } else {
               self.handleNoGeocode('Address type is not of city, state, or country');
               return;
            }

            self.mapObjects.mapInstance.setCenter(new google.maps.LatLng(result.geometry.location.lat(), result.geometry.location.lng()));

            self.viewportSearch();
         } else {
            self.handleNoGeocode(status);
         }
      });
   },

   handleNoGeocode: function(status) {
      var self = this;
      var content = 'Error: Geocode was not successful for the following reason: ' + status;

      self.collection.reset();
   },


   // Gets Lat/Lng from geolocated pin to figure out the distance of each marker from the pin.
   distanceFromYouAreHerePinSearch: function() {
      var self = this;

      self.searchParameters.bounds = null;

      var youAreHerePin = self.mapObjects.youAreHerePin;
      self.searchParameters.lat = youAreHerePin.position.lat();
      self.searchParameters.lng = youAreHerePin.position.lng();

      var mapPoints = {};

      if (self.searchParameters.lat !== null && self.searchParameters.lng !== null) {
         mapPoints.Center = { Lat: self.searchParameters.lat, Lng: self.searchParameters.lng };
      }

      self.mediator.trigger('map:setAndSearchMapPoints', mapPoints, self.searchParameters.showApproxMiles);
   },

   // Gather map points to search on.
   // Need to get lat/lng from the youAreHerePin and the lng/lng for NE and SW corners of the map for the bounds.
   // Searching by viewport return all markers within the bounds of the visible map and figures out the distance of each marker from the youAreHerePin.
   viewportSearch: function () {
      var self = this;

      self.searchParameters.bounds = self.mapObjects.mapInstance.getBounds();

      if (self.mapObjects.youAreHerePin) {
         self.searchParameters.lat = self.mapObjects.youAreHerePin.position.lat();
         self.searchParameters.lng = self.mapObjects.youAreHerePin.position.lng();
      } else {
         var centerOfUSA = new google.maps.LatLng(37.6, -95.665) // geolocation and maxmind failed, put user in the cetner of the USA
         self.searchParameters.lat = centerOfUSA.lat();
         self.searchParameters.lng = centerOfUSA.lng();
         self.searchParameters.showApproxMiles = false;
      }

      var mapPoints = {};

      if (self.searchParameters.lat !== null && self.searchParameters.lat !== undefined && self.searchParameters.lng !== null && self.searchParameters.lng !== undefined) {
         mapPoints.Center = { Lat: self.searchParameters.lat, Lng: self.searchParameters.lng };
      }

      if (self.searchParameters.bounds !== null && self.searchParameters.bounds !== undefined) { // get bounds
         var ne = self.searchParameters.bounds.getNorthEast();
         var maxLat = ne.lat();
         var maxLong = ne.lng();

         var sw = self.searchParameters.bounds.getSouthWest();
         var minLat = sw.lat();
         var minLong = sw.lng();

         mapPoints.Bounds = { NE: { Lat: maxLat, Lng: maxLong }, SW: { Lat: minLat, Lng: minLong } };
      }

      self.mediator.trigger('map:setAndSearchMapPoints', mapPoints, self.searchParameters.showApproxMiles);
   },

   // Set the geolocation pin for where the user is currently located
   setYouAreHerePin: function (lat, lng) {
      var self = this;

      self.removeGeolocationPin(); // remove old pin

      var pinLatLng = new google.maps.LatLng(lat, lng);

      var markerOptions = {
         position: pinLatLng,
         icon: {
            url: window.location.pathname.replace(/^\/([^\/]*).*$/, '$1') + '/SpecialistLocator/assets/img/icons/pincenter.png',
            size: new google.maps.Size(25, 25),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(13, 13)
         },
         cursor: 'pointer',
         zIndex: -1
      }

      self.mapObjects.youAreHerePin = new google.maps.Marker(markerOptions);

      google.maps.event.addListener(self.mapObjects.youAreHerePin, 'click', function (event) {
         $('#address-search').val('');
         $('#fr-no-other-search').val('');

         self.geolocationSearch();
      });

      self.mapObjects.youAreHerePin.setMap(self.mapObjects.mapInstance);
   },

   // remove old center pin
   removeGeolocationPin: function () {
      var self = this;

      if (self.mapObjects.youAreHerePin !== null && self.mapObjects.youAreHerePin !== undefined) {
         self.mapObjects.youAreHerePin.setMap(null); // remove from map
         self.mapObjects.youAreHerePin = null; // remove from memory
      }
   },

   // center the map over all markers on map
    centerMapOverMarkers: function () {
       var self = this;
       var address = $('#address-search').val();
       var nameOrNO = $('#fr-no-other-search').val();

       // only center the map over all markers if we have markers or using any of the search boxes
       if (self.mapObjects.markers.length > 0 && (address || nameOrNO)) {
          var bounds = new google.maps.LatLngBounds();

          self.mapObjects.markers.forEach(function(marker) { // loop through all markers and extend the bounds to be able to show all markers
             bounds.extend(marker.getPosition());
          });

          //center the map to the geometric center of all markers
          self.mapObjects.mapInstance.setCenter(bounds.getCenter());

          self.mapObjects.mapInstance.fitBounds(bounds);
       } 

       // set a minimum zoom 
       // if you got only 1 marker or all markers are on the same address map will be zoomed too much.
       if (self.mapObjects.mapInstance.getZoom() > 15) {
          self.mapObjects.mapInstance.setZoom(15);
       }
    },

    cluster: function() {
      var self = this;
   //   var mcOptions = { gridSize: 50, maxZoom: 15 };

     // self.mapObjects.markerClusterer = new MarkerClusterer(self.mapObjects.mapInstance, self.mapObjects.markerClustererArray, mcOptions);   
    },

   setAllMap: function (map) {
      var self = this;

      self.mapObjects.markers.forEach(function (marker) {
         marker.setMap(map);
      });
   },

   showMarkers: function() {
      this.setAllMap(self.mapObjects.mapInstance);
   },

    clearMarkers: function() {
       this.setAllMap(null);
    },

    deleteMarkers: function() {
       var self = this;

       if (self.mapObjects.oms) {
          self.mapObjects.oms.clearMarkers(); // clear all spiderfy markers
       }

       if (self.mapObjects.markerClusterer) {
          self.mapObjects.markerClusterer.clearMarkers(); // clear all cluster markers
       }

       self.mapObjects.markersCurrentlySpiderfied = [];
       self.mapObjects.activeMarker = null; // clear the active marker
       self.mapObjects.markerClustererArray = []; // clear cluster marker array
       self.clearMarkers(); // clear all normal markers on map
       self.mapObjects.markers = []; // clear out normal marker array
    },


    // Add a marker to the map and push to the array.
    addOfficeMarker : function(model) {
       var self = this;

        //build a LatLng for the office position
        var address = model.get('Address');
        var position = new google.maps.LatLng(address.Latitude, address.Longitude);

        //add the marker
        var marker = new google.maps.Marker({
           position: position,
           icon: {
              url: window.location.pathname.replace(/^\/([^\/]*).*$/, '$1') + '/SpecialistLocator/assets/img/icons/pinrep.png',
              size: new google.maps.Size(32, 40),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 40)
           },
           map: self.mapObjects.mapInstance
        });

        self.mapObjects.markers[model.get('id')] = marker;
       self.mapObjects.markerClustererArray.push(marker);

       self.mapObjects.oms.addMarker(marker);
    },

   // Gets called when an item in the list gets clicked. We need to set the highlighting on the correct marker on the map for the new marker.
    highlightListItemAndMarker: function (model) {
       var self = this;

       // hide previously selected list item
       self.mediator.trigger('map:hidePrevSelected');

       // set model item attribute selected to true so we can highlight item in the list
       model.set({ selected: true });

       var newMarker = self.mapObjects.markers[model.get('id')];
       var newMarkerPosition = newMarker.getPosition();

       if (self.mapObjects.activeMarker) {
          var markersNearActiveMarker = self.mapObjects.oms.markersNearMarker(self.mapObjects.activeMarker, false);
       }

       // if zoomed out too far the first click will call spiderfyChangePinIcon which will zoom in the map and not click any markers,
       // a second click will spiderfy the new markers, and third click will trigger the spiderfymarker click event for the new markers.
       if (self.mapObjects.mapInstance.getZoom() < 10) {
          google.maps.event.trigger(newMarker, 'click');
          self.resetMarkerHighlighting(); // reset the highlighting after map zooms in for the case of more than one 'cluster' of pins is spiderfied when zoomed out too far
          google.maps.event.trigger(newMarker, 'click');
          google.maps.event.trigger(newMarker, 'click');
       } else if (self.mapObjects.oms.markersNearMarker(newMarker, true).length > 0) {// one click to spiderfy new markers, another click to trigger spiderfymarker click event for the new markers.
          google.maps.event.trigger(newMarker, 'click')
          google.maps.event.trigger(newMarker, 'click')
       } else {
          google.maps.event.trigger(newMarker, 'click')
       }

       if (!self.mapObjects.mapInstance.getBounds().contains(newMarkerPosition)) { // check if marker is outside bounds of map
          self.mapObjects.mapInstance.panTo(newMarkerPosition);
       }

       newMarker.setAnimation(google.maps.Animation.BOUNCE);
       setTimeout(function () { newMarker.setAnimation(null); }, 1400);
    },

   // When the map gets zoomed in or out we need to change the icons for markers that
   // may or may not be spiderfied because spiderfiable markers change upon zoom changes
    resetMarkerHighlighting: function () {
       var self = this;

       // Dont change any markers if we have any markers currently spiderfied.
       // Markers will only be spiderfied after a idle event if the size of the windows gets changed or
       // when you move around on the map, otherwise markers will be unspiderfied on a zoom or map click.
       if (self.mapObjects.markersCurrentlySpiderfied.length <= 0) {
          if (self.mapObjects.markers && self.mapObjects.markers.length > 0) { // only do stuff if markers are on map
             self.mapObjects.markers.forEach(function (marker) {
                self.setMarkerIcon(marker, self.settings.pinRepIconURL); // set all markers to pinrep.png
             });
             self.changeIconsForMarkersNearAnyOtherMarker(); // set all spiderfiable markers to pinreps.png
          }
          if (self.mapObjects.activeMarker) { // if we have an active marker we have to highlight the marker
             if (self.mapObjects.oms.markersNearMarker(self.mapObjects.activeMarker, true).length > 0) { // if active marker is spiderfiable, change icons for all markers near it to pinrepsh.png
                var markersNearActiveMarker = self.mapObjects.oms.markersNearMarker(self.mapObjects.activeMarker, false);

                markersNearActiveMarker.forEach(function (marker) {
                   self.setMarkerIcon(marker, self.settings.pinRepshIconHighlightedURL);
                });
                self.setMarkerIcon(self.mapObjects.activeMarker, self.settings.pinRepshIconHighlightedURL); // Lastly, change icon for active marker to pinrepsh.png
             } else {
                self.setMarkerIcon(self.mapObjects.activeMarker, self.settings.pinRepIconHighlightedURL); // change icon for non spiderfiable marker to pinreph.png
             }
          }
       }
    },

   // Change icons for all pins on top of each other to pinreps.png
    changeIconsForMarkersNearAnyOtherMarker: function () {
       var self = this;
       if (self.mapObjects.oms) {
          var markersNearAnyOtherMarker = self.mapObjects.oms.markersNearAnyOtherMarker();
          markersNearAnyOtherMarker.forEach(function(marker) {
             self.setMarkerIcon(marker, self.settings.pinRepsIconURL);
          });
       }
    },

   // Called when markers are spiderfied.
   // When markers are spiderfied, change all their icons to pinrep.png.
   // If one of the spiderfied markers are active change its icon to pinreph.png
    spiderfyChangePinIcon: function (markers) {
       var self = this;
       // set zoom if they are zoomed out too far
       if (self.mapObjects.mapInstance.getZoom() < 10) {
          self.mapObjects.mapInstance.setZoom(11);
          self.mapObjects.mapInstance.panTo(markers[0].getPosition());
       } else {
          self.mapObjects.markersCurrentlySpiderfied = markers;

          markers.forEach(function(marker) {
             self.setMarkerIcon(marker, self.settings.pinRepIconURL); // change all spiderfied markers to pinrep.png
          });

          // if there is an active marker that is spiderfiable and the user unspiderfies and respiderfies we need to rehighlight that old active marker
          if (self.mapObjects.activeMarker && _.contains(markers, self.mapObjects.activeMarker)) {
             self.setMarkerIcon(self.mapObjects.activeMarker, self.settings.pinRepIconHighlightedURL);
          }
       }
    },

   // Called when markers are unspiderfied.
   // When markers are unspiderfied, change all their icons to pinreps.png.
   // If the active marker is unspiderfied, change all their icons to pinrepsh.png
    unspiderfyChangePinIcon: function (markers) {
       var self = this;

       self.mapObjects.markersCurrentlySpiderfied = [];

       // If the active marker is unspiderfied, change all their icons to pinrepsh.png
       if (self.mapObjects.activeMarker && _.contains(markers, self.mapObjects.activeMarker)) {
          markers.forEach(function (marker) {
             self.setMarkerIcon(marker, self.settings.pinRepshIconHighlightedURL);
          });
       } else {
          markers.forEach(function (marker) {
             self.setMarkerIcon(marker, self.settings.pinRepsIconURL);
          });
       }
    },

   // Called when a marker on the map is clicked.
    spiderfyMarkerClick: function (marker, event) {
       var self = this;

       self.mediator.trigger('map:hidePrevSelected'); // unhighlight old office item
       // set zoom if they are zoomed out too far
       if (self.mapObjects.mapInstance.getZoom() < 10) {
          self.mapObjects.mapInstance.setZoom(11);
          self.mapObjects.mapInstance.panTo(marker.getPosition());
       }
       self.highlightNewActiveMarker(marker);

       //set model item attribute to selected so we can highlight it
       var index = self.mapObjects.markers.indexOf(marker);
       var model = self.collection.find(function (model) { return model.get('id') == index })
       model.set({ selected: true });

       self.mediator.trigger('map:scrollToOffice', model); // scroll to item in collection
    },

   // Highlight newMarker parameter on the map when a office item or marker on the map is clicked
    highlightNewActiveMarker: function (newMarker) {
       var self = this;

       self.unhighlightOldActiveMarker(newMarker);

       self.setMarkerIcon(newMarker, self.settings.pinRepIconHighlightedURL);

       self.mapObjects.activeMarker = newMarker;
    },

   // Unhighlight previous active marker on the map when a office item or marker on the map is clicked
    unhighlightOldActiveMarker: function (newMarker) {
       var self = this;

       if (self.mapObjects.activeMarker) {

          // if old active marker has markers near it(spiderfiable)
          // we can check this because unspiderfy gets called before all other functions
          if (self.mapObjects.oms.markersNearMarker(self.mapObjects.activeMarker, true).length > 0 ) {
             var markersNearActiveMarker = self.mapObjects.oms.markersNearMarker(self.mapObjects.activeMarker, false);

             // loop through all prevously spiderfied markers that are near the old active marker 
             // and change their icon to pinreps.png
             markersNearActiveMarker.forEach(function (marker) {
                self.setMarkerIcon(marker, self.settings.pinRepsIconURL);
             });
             // lastly change the old active marker to pinreps.png
             self.setMarkerIcon(self.mapObjects.activeMarker, self.settings.pinRepsIconURL);
          } else {
             self.setMarkerIcon(self.mapObjects.activeMarker, self.settings.pinRepIconURL); // if old active marker isn't spiderfiable change icon to pinrep.png
          }
       }
    },


    setMarkerIcon: function (marker, iconURL) {
       marker.setIcon({
          url: iconURL,
          size: new google.maps.Size(32, 40),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 40)
       });
    }

});
