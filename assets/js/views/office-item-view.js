;

'use strict';

var OfficeItemView = BaseItemView.extend({

    tagName: 'li',


    events: {
        'click .office': 'officeItemClick',
    },

    initialize: function(options){

        var self = this;
        self.mediator = options.mediator;//todo: buble up event to parent, let parent communicate with mediator
        self.template = $('#office-item-view').html();

        self.listenTo(this.model, 'change:selected', self.toggleActiveItem); //listen if models 'selected' attribute is changed

        this.render();

    },

    serialize: function(){
        return this.model.toJSON();
    },

    // Toggle the class active when the items 'selected' variable gets changed
    toggleActiveItem: function () {
        this.$el.toggleClass('active');
    },

    officeItemClick: function () {
       this.mediator.trigger('office:officeItemClick', this.model);
    }

});
