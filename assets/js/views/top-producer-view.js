;

'use strict';

var TopProducerView = Backbone.View.extend({
   el: '#li_slider',

   events: {
      'change input': 'toggleStatus'
   },


   initialize: function(options) {

      var self = this;
      self.mediator = options.mediator;
      self.model = options.model;

      self.mediator.on('product-filter-list:hideOrShowTopProduction', self.hideOrShowTopProduction, self);
      self.mediator.on('market-filter-list:hideOrShowTopProduction', self.hideOrShowTopProduction, self);

   },

   hideOrShowTopProduction: function(productFilterCount, marketFilterCount) {
      if (productFilterCount > 1 || marketFilterCount > 0) {
         $(".search-filter-production").css('background-color', '#565a5c');
         $(".search-filter-production").css('cursor', 'no-drop');
         $("#production-right").css('visibility', 'hidden');
      }
      else {
         $(".search-filter-production").css('background-color', '#002b49');
         $(".search-filter-production").css('cursor', 'default');
         $("#production-right").css('visibility', 'visible');
      }
   },

   toggleStatus: function () {
      var self = this;
      self.mediator.trigger('top-producer:slider', (self.$el).find('#slider-percentage').val());
      if ((self.$el).find('#slider-percentage').val() != '0') {
          $("#business-market").css('background-color', '#565a5c');
          $("#business-market").css('cursor', 'no-drop');
          $("#market-filter-list").slideUp();
      }
      else {
          $("#business-market").css('background-color', '#002b49');
          $("#business-market").css('cursor', 'default');
          $("#market-filter-list").slideDown();
      }
   }


});
