;

'use strict';

var ResultsPanelView = Backbone.View.extend({

        el:'#results-panel',

        itemView: OfficeItemView,

        initialize: function (options) {
            var self = this;

            self.mediator = options.mediator;

            self.listenTo(this.collection, 'sync', self.render);
            self.listenTo(this.collection, 'reset', self.render);
            self.mediator.on('map:clearResultsCount', self.clearResultsCount, self);

        },

        render: function () {

            var self = this;

            self.$('.results-count-item').html(self.collection.length+' Results');

        }

});

var OfficeListView = Backbone.View.extend({
   tagName: 'ul',

   itemView: OfficeItemView,

   initialize: function(options) {
      var self = this;

      self.mediator = options.mediator;

      self.mediator.on('map:hidePrevSelected', self.hidePrevSelected, self);

      self.mediator.on('map:scrollToOffice', self.scrollToOffice, self);

      self.listenTo(this.collection, 'selected', function(model) {

         self.mediator.trigger('list:selected-item', model);
      })

      self.listenTo(this.collection, 'sync', self.render);
      self.listenTo(this.collection, 'reset', self.render);
   },

   beforeRender: function() {
   
   },

   render: function() {
      var self = this;

      fragment = document.createDocumentFragment();

      self.mediator.trigger('list:deleteMarkers');

      self.collection.each(function (model) {


         var item = new self.itemView({ model: model, mediator: self.mediator });

         fragment.appendChild(item.el);

         model.set({ id: _.uniqueId() });
         self.mediator.trigger('list:addMarker', model);

      });

      self.$el.html(fragment);

      self.mediator.trigger('list:centerMapOverMarkers');
      self.scrollToTop();
      self.mediator.trigger('list:centerMap');
      self.mediator.trigger('list:changeIconsForMarkersNearAnyOtherMarker');
      self.mediator.trigger('list:cluster');
      self.mediator.trigger('list:stopSpinner'); 

   },


   // set previous models 'selected' attribute to false to trigger that items remove style function
   hidePrevSelected: function() {
      this.collection.each(function(model) {
         if (model.get('selected') === true) {
            model.set({ selected: false });
         }
      });
   },

   scrollToOffice: function(model) {
      var modelIndex = this.collection.indexOf(model) + 1;

      $('#list').animate({
         scrollTop: $('#list>ul>li:nth-child(' + modelIndex + ')').position().top - $('#list>ul>li:first').position().top
      }, 200);
   },

   scrollToTop: function () {
      $('#list').animate({
         scrollTop: '0'
      }, 200);
   }

});

