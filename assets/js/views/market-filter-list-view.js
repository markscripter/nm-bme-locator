;

'use strict';
var market_count = 0;
var MarketFilterListView = Backbone.View.extend({
   tagName: 'ul',
   className: 'sub-menu',

   itemView: FilterItemView,

   settings: {
      marketFilterCount: 0,
      productFilterCount: 0
   },

   initialize: function(options) {
      var self = this;

      self.mediator = options.mediator;

      self.listenTo(this.collection, 'sync', self.render);

      self.mediator.on('product-filter-list:checked', self.incrementProductFilterCount, self);
      self.mediator.on('product-filter-list:unchecked', self.decrementProductFilterCount, self);

      // triggered when an input is checked
      self.listenTo(this.collection, 'filter:checked', function (model) {
         self.settings.marketFilterCount++;

         self.mediator.trigger('market-filter-list:checked', model);
         self.mediator.trigger('market-filter-list:hideOrShowTopProduction', self.settings.productFilterCount, self.settings.marketFilterCount);
      });

      // triggered when an input is unchecked
      self.listenTo(this.collection, 'filter:unchecked', function (model) {
         self.settings.marketFilterCount--;

         self.mediator.trigger('market-filter-list:unchecked', model);
         self.mediator.trigger('market-filter-list:hideOrShowTopProduction', self.settings.productFilterCount, self.settings.marketFilterCount);       
      });
   },

   incrementProductFilterCount: function () {
      var self = this;

      self.settings.productFilterCount++;
   },

   decrementProductFilterCount: function () {
      var self = this;

      self.settings.productFilterCount--;
   },

   render: function() {

       var self = this,
         fragment = document.createDocumentFragment();

      self.collection.each(function(model) {


         var item = new self.itemView({ model: model, mediator: self.mediator });

         fragment.appendChild(item.el);

      });

      self.$el.html(fragment);
      //self.$el.append("<li id='coli_boli'>COLI&#47BOLI</li>");
   },

   events: {
       'click #coli_boli': 'showColiBoliAlert',
   },
    //alert box to inform the user that if they want information on coli/boli to call the specified number
   showColiBoliAlert: function () {
      new Messi('Please call Specialty Markets at 414.661.5300 to discuss COLI/BOLI joint work opportunities.', { title: 'COLI/BOLI Information', titleClass: 'info', buttons: [{ id: 0, label: 'OK', val: 'X' }] });
       $("#coli_boli").prop("checked", false);
       
   },

 
   

});

