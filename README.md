# NM BME Locator
This project contains the files associated to the NorthWestern Mutual Business Market Expert (BME) Locator.

## Installation
Before you begin, make sure you have [grunt](http://gruntjs.com/) installed:

    sudo npm i -g grunt-cli

To install the project & its dependencies:

    git clone <repo>
    cd <repo>/assets
    npm i

To validate everything is working, run the following command:

    grunt watch


## Viewing The project
To run a local server without configuring or setting anything up, install:

    sudo npm i -g http-server

This will install a local node server. To use it, run the following command from the root of this repo (where Index.html is located):

    http-server .


This will provide you with a URL to visit, something like:

    Starting up http-server, serving .
    Available on:
      http:127.0.0.1:8080
      http:192.168.1.109:8080
    Hit CTRL-C to stop the server
